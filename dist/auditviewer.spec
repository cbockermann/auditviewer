Name:		auditviewer
Version:	%_version
Release:	%_revision
Summary:	The AuditViewer is a standalone application to inspect ModSecurity audit-log files
Group:		admin
URL:		http://www.jwall.org/AuditViewer
Source0:	http://download.jwall.org/AuditViewer/current/
BuildRoot:	/home/chris/Uni/Projekte/AuditViewer/.build_tmp
BuildArch:	noarch
License:	None

Group: Applications/System


%description
Brief description of software package.

%prep

%build

%files -f ../tmp/rpmfiles.list
%defattr(-,root,root)
%doc

%changelog
