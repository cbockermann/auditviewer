package org.jwall.tools;

import java.io.FileWriter;
import java.io.PrintStream;
import java.util.TreeSet;


/**
 * 
 * This class simply checks for version information provided by the classes
 * currently loaded by the VM.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class VersionTool
{
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // By default we write to stdout
        //
        PrintStream out = System.out;
        
        // The version information we need to print out in the end
        TreeSet<String> versions = new TreeSet<String>();
        
        //
        // Usually this class is called to determine a single version, e.g. from within
        // a build-file to determine an applications software version.
        // In this case we print out the single version string in one line
        //
        for( String arg : args ){
            try {
                Class.forName( arg );
                if( System.getProperty( arg + ".version") != null )
                    versions.add( arg );
                else
                    System.out.println( "Class " + arg + " does not provide version information!" );
            } catch (Exception e) {
                e.printStackTrace();
                System.exit( -1 );
            }
        }
        
        //
        // in case we have been called with no particular component which's version
        // is to be determined, we dump out all version information available:
        //
        if( args.length == 0 ){
            for( Object key : System.getProperties().keySet() ){
                if( key.toString().endsWith( ".version" ) )
                    versions.add( key.toString() );
            }
        }

        // print out the collected version information to stdout
        //
        for( String vk : versions ){
            out.println( vk + "="  + System.getProperty( vk + ".version" ) );
        }

        try {
            FileWriter fout = new FileWriter( ".version" );
            for( String vk : versions )
                fout.write( vk + ".version=" + System.getProperty( vk + ".version" ) + "\n" );
            fout.close();
        } catch (Exception e) {
            
        }
    }
}