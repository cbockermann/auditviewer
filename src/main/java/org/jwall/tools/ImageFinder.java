package org.jwall.tools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.TreeSet;

import org.jwall.app.Resource;
import org.jwall.app.Resources;

/**
 * 
 * This class extracts a list of required icons from the
 * icons.properties file.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public class ImageFinder {

    /**
     * @param args
     */
    public static void main(String[] args) {

        try {

            List<String> files = new LinkedList<String>();
            files.add( "/org/jwall/app/resources.xml");

            for( String s : args )
                files.add( s );


            TreeSet<String> images = new TreeSet<String>();

            for( String resFile : files ){
                int count = 0;

                if( resFile.endsWith( ".xml" ) ){
                    System.out.println("Reading resource-file " + resFile + " with xstream...");
                    count = 0;
                    Resources rs = new Resources();
                    rs.load( resFile );
                    
                    for( Resource r : rs.getResources() ){
                        String path = r.getId(); //.getIcon();
                        path = r.getIcon();
                        if( path.startsWith( "/" ) ){
                            images.add( path.replaceFirst("/", "") );
                            count ++;
                        } else {
                            images.add( "icons/16x16/" + path );
                            images.add( "icons/24x24/" + path );
                            count += 2;
                        }
                    }
                    
                } else {

                    URL url = ImageFinder.class.getResource( resFile );
                    if( url != null ){
                        Properties p = new Properties();
                        p.load( url.openStream() );


                        for( Object o : p.values() ){
                            String path = o.toString();
                            if( path.startsWith("/") ) {
                                images.add( path.replaceFirst( "/", "") );
                                count++;
                            } else {
                                images.add( "icons/16x16/" + path );
                                images.add( "icons/24x24/" + path );
                                count += 2;
                            }
                        }



                    } else
                        System.out.println("Could not locate the resource \"" + resFile + "\"!");
                }
                
                System.out.println( count + " images referenced in file  "+resFile);
            }

            System.out.println("In total " + images.size() + " (distinct!) images referenced.");

            PrintStream out = new PrintStream( new FileOutputStream( "icons.txt" ) );
            for( String f : images )
                out.println( f );

            out.flush();
            out.close();

        } catch (Exception e){
            e.printStackTrace();
            System.exit(-1);
        }
    }

}
