/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.tools;

import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.jwall.app.Resource;
import org.jwall.app.Resources;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class ResourceMappingConverter
{

    /**
     * @param args
     */
    @SuppressWarnings("unchecked")
    public static void main( String[] args ){
    
        try {
            
            Resources res = new Resources();

            
            
            
            InputStream in = Resources.class.getResourceAsStream( "/org/jwall/web/audit/viewer/icons.properties" );
            Properties icons = new Properties();
            icons.load( in );
            
            in = Resources.class.getResourceAsStream( "/org/jwall/web/audit/viewer/messages.properties" );
            Properties msgs = new Properties();
            msgs.load( in );
            
            
            Enumeration en = icons.keys();
            while( en.hasMoreElements() ){
                String k = (String) en.nextElement();
                
                Resource a = new Resource( k );
                a.setIcon( icons.getProperty( k ) );
                
                if( msgs.get( k ) != null ){
                    String label = (String) msgs.get( k );
                    a.setLabel( label );
                }
                
                res.getResources().add( a );
            }
            
            XStream xs = new XStream( new DomDriver() );
            xs.processAnnotations( Resources.class );
            xs.processAnnotations( Resource.class );
            
            System.out.println( xs.toXML( res ) );
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
