/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.jwall.app.Action;
import org.jwall.app.Application;
import org.jwall.app.BugReportLogger;
import org.jwall.app.BugReporter;
import org.jwall.app.LogHandler;
import org.jwall.app.ui.DisplayAction;
import org.jwall.app.ui.SplashScreen;
import org.jwall.web.audit.AuditEventDispatcher;
import org.jwall.web.audit.viewer.EventModule;
import org.jwall.web.audit.viewer.ui.AuditView;

/**
 * <p>
 * This is the main-class of the AuditViewer application. The class is
 * responsible to set up the basic event-handling, load properties and
 * initialize some data structures. The main functionality is implemented by
 * modules such as the <code>EventModule</code>.
 * </p>
 * <p>
 * The <code>EventModule</code> provides facilities to load, process and display
 * events.
 * </p>
 * <h3>Changes</h3>
 * <ul>
 * <li>Added support for recursively reading from concurrent-data directories
 * </li>
 * </ul>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditViewer extends Application implements Runnable {
    /** The version of the AuditViewer */
    public final static String VERSION = "0.3.6-SNAPSHOT";
    public final static String REVISION = "$Revision$";

    static {
        Logger.getGlobal().addHandler(new LogHandler());
        LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME).addHandler(new LogHandler());
        System.setProperty("org.jwall.AuditViewer.version", VERSION);
        System.setProperty("org.jwall.AuditViewer.revision", REVISION);
    }

    final static Logger log = Logger.getLogger("AuditViewer");

    public static boolean VERBOSE = false;

    /**
     * This is the main module which allows for displaying and processing of
     * audit-events.
     */
    EventModule eventModule;

    /** This module allows receiving audit-events via a network connection */
    // RemoteAuditModule remoteModule;

    /** */
    AuditEventDispatcher eventDispatcher;

    private static AuditViewer auditViewer;

    /**
     * 
     * This constructor create the application, does the basic setup, displays a
     * splash-screen during setup, etc.
     * 
     */
    public AuditViewer() {
        super("AuditViewer");
        auditViewer = this;

        log.info("AuditViewer version " + this.getVersion());
        log.info("web-audit library version " + org.jwall.web.audit.Version.getVersion());

        this.registerExceptionHandler(new BugReporter());

        SplashScreen s = new SplashScreen();
        s.setVisible(true);

        s.setProgress(25);
        s.setMessage("Loading properties");

        try {
            this.loadProperties();
        } catch (Exception e) {
        }
        s.setProgress(50);

        try {
            Thread.sleep(50);
        } catch (Exception e) {
        }

        s.setProgress(75);
        s.setMessage("Registering plugins...");
        try {
            Thread.sleep(150);
        } catch (Exception e) {
        }

        registerAction(new Action(this, Action.QUIT, "File/Quit", 10.0d));
        registerAction(new DisplayAction("org.jwall.app.help.help", "Help/Help", 1.0d,
                Application.getResource("/docs/help.html"), true));
        registerAction(new DisplayAction("org.jwall.app.help.about", "Help/About", 2.0d,
                Application.getResource("/docs/about.html"), true));

        eventDispatcher = new AuditEventDispatcher();
        eventModule = new EventModule(this);

        for (Action a : eventModule.getActions()) {
            registerAction(a);
        }

        getWindow().getToolBar().addSeparator();

        /*
         * remoteModule = RemoteAuditModule.createModule( this ); for( Action a
         * : remoteModule.getActions() ) registerAction( a );
         */

        try {

            // String lafName = UIManager.getSystemLookAndFeelClassName();
            // UIManager.setLookAndFeel( lafName );

        } catch (Exception e) {

            e.printStackTrace();

        }

        win.setJMenuBar(getMenuBar());
        win.setView(eventModule.getAuditView());

        s.setMessage("web-audit library version " + org.jwall.web.audit.Version.getVersion());
        try {
            Thread.sleep(750);
        } catch (Exception e) {
        }

        s.setProgress(90);
        s.setMessage("Starting AuditViewer " + VERSION + " ...");
        try {
            Thread.sleep(250);
        } catch (Exception e) {
        }
        s.setProgress(100);

        s.setVisible(false);
        win.setVisible(true);
    }

    /**
     * @see org.jwall.app.Application#getPreferenceDirectory()
     */
    public String getPreferenceDirectory() {
        return System.getProperty("user.home") + SLASH + ".jwall";
    }

    /**
     * @see org.jwall.app.Application#getPreferenceFileName()
     */
    public String getPreferenceFileName() {
        return "viewer.xml";
    }

    /**
     * @see org.jwall.app.Application#getVersion()
     */
    public String getVersion() {
        return VERSION;
    }

    public void actionPerformed(ActionEvent e) {
        if (Action.QUIT.equals(e.getActionCommand())) {
            this.cleanUp();
            System.exit(0);
        }
    }

    /**
     * 
     * 
     */
    public void cleanUp() {
        try {
            log.info("Cleanup...");
            saveProperties();
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void run() {
        log.info("Stopping the AuditViewer application.");
        try {
            cleanUp();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            log.info("Backlog:");
            log.info(BugReportLogger.backlog());
        }
    }

    public EventModule getEventModule() {
        return eventModule;
    }

    public static void setProperty(String key, String value) {
        System.setProperty(key, value);
    }

    public static String getProperty(String key) {
        return System.getProperty(key);
    }

    /**
     * 
     * @deprecated The AuditView should become part of the event-module to
     *             enforce a separation of modules+view from the application.
     *             (More flexibility).
     * @return
     */
    public AuditView getAuditView() {
        return eventModule.getAuditView();
    }

    public static AuditViewer getAuditViewer() {
        return auditViewer;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.jwall.app.Application#getResourceFiles()
     */
    @Override
    public List<String> getResourceFiles() {
        List<String> files = super.getResourceFiles();
        files.add(new String("/org/jwall/web/audit/viewer/resources.xml"));
        files.add(new String("/org/jwall/web/audit/viewer/modsecurity-resources.xml"));
        files.add(new String("/org/jwall/web/audit/tagging/resources.xml"));
        return files;
    }

    public static void main(String args[]) {

        // Optionally remove existing handlers attached to j.u.l root logger
        // SLF4JBridgeHandler.removeHandlersForRootLogger(); // (since SLF4J
        // 1.6.5)

        // add SLF4JBridgeHandler to j.u.l's root logger, should be done once
        // during
        // the initialization phase of your application
        // SLF4JBridgeHandler.install();

        for (String arg : args) {
            if (arg.equalsIgnoreCase("-v") || arg.equalsIgnoreCase("--version")) {
                System.out.print(VERSION);
                System.exit(0);
            }

            if (arg.equalsIgnoreCase("--verbose"))
                VERBOSE = true;
        }

        if (VERBOSE) {
            TreeSet<String> keys = new TreeSet<String>();
            for (Object k : System.getProperties().keySet())
                keys.add(k.toString());

            for (String key : keys)
                System.out.println(key + " = " + System.getProperty(key));
        }

        Logger log = Logger.getLogger("");
        log.setLevel(java.util.logging.Level.ALL);

        AuditViewer c = new AuditViewer();
        Runtime.getRuntime().addShutdownHook(new Thread(c));
    }
}
