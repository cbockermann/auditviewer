/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.obfuscator;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.File;
import java.io.StringReader;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JFileChooser;

import org.jwall.AuditViewer;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurityAuditEvent;
import org.jwall.web.audit.io.BufferedAuditEventWriter;
import org.jwall.web.audit.io.ModSecurity2AuditWriter;
import org.jwall.web.audit.util.Base64Codec;
import org.jwall.web.audit.viewer.ui.EventProcessor;


/**
 * 
 * This is a simple obfuscator which only provides straight forward substitutions
 * of the audit-event data.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class Obfuscator 
implements EventProcessor
{

    Hashtable<String,String> requestReplacements = new Hashtable<String,String>();
    Hashtable<String,String> responseReplacements = new Hashtable<String,String>();



    public Obfuscator(){

        Base64Codec codec = new Base64Codec();

        requestReplacements.put("Host: .*", "Host: www.example.com");
        requestReplacements.put("Referer: http://.*/", "Referer: http://external.example.com/");
        requestReplacements.put("Authorization: .*", "Authorization: Basic: "+( codec.encode( "Homer:Simpson".getBytes() ) ) );
        requestReplacements.put("Proxy-Via: .*", "Proxy-Via: proxy.example.com");
        requestReplacements.put("Via: .*", "Via: proxy.example.com");
        requestReplacements.put("WWW-Authenticate: .*", "WWW-Authenticat: Basic Form-Authentication");
        requestReplacements.put("Cookie: PHPSESSIONID=[A-Z0-9]*", "Cookie: PHPSESSIONID=00000000000000000000000001");
        requestReplacements.put("Cookie: JSESSIONID=[A-Z0-9]*", "Cookie: JSESSIONID=00000000000000000000000001");
        requestReplacements.put("X-BlueCoat-Via: [A-Z0-9]*", "X-Bluecoat-Via: FFFFFFFFF");

        responseReplacements.put( "JSESSIONID=[A-Z0-9]*", "JSESSIONID=0000000000000000000000000000000000" );
        responseReplacements.put( "PHPSESSIONID=[A-Z0-9]*", "PHPSESSIONID=0000000000000000000000000000000000" );
    }

    public void processEvent(String cmd, Collection<AuditEvent> evt) {


        if( EventProcessor.ACTION_OBFUSCATE_TO_CLIPBOARD.equals( cmd ) ){

            Obfuscator o = new Obfuscator();
            StringBuffer s = new StringBuffer();

            for( AuditEvent e : evt ){
                try{
                    AuditEvent obf = o.obfuscate( e );
                    s.append( obf.toString() );
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            Toolkit tk = Toolkit.getDefaultToolkit();
            Clipboard clip = tk.getSystemClipboard();
            clip.setContents( new StringSelection(s.toString()), null);
        }

        if( EventProcessor.ACTION_OBFUSCATE_TO_DISK.equals( cmd ) ){

            Obfuscator o = new Obfuscator();
            Vector<AuditEvent> obfuscated = new Vector<AuditEvent>();
            for( AuditEvent e : evt ){
                try {
                    obfuscated.add( o.obfuscate( e ) );
                } catch (Exception ex){
                    ex.printStackTrace();
                }
            }

            saveEvents( obfuscated );
        }

    }

    public void saveEvents(Collection<AuditEvent> events){
        try {

            JFileChooser fc = new JFileChooser();
            String fname = "";

            if(events.size() == 1){
                fname = events.iterator().next().getEventId()+"-audit.log";
                fc.setSelectedFile(new File(fname));
            }

            int r = fc.showSaveDialog( AuditViewer.getWindow() );
            if(r == JFileChooser.APPROVE_OPTION){

                File f = fc.getSelectedFile();

                BufferedAuditEventWriter wr = new BufferedAuditEventWriter( new ModSecurity2AuditWriter( f ) );
                wr.addAll( events );
                wr.start();

                // by immediately closing the writer, we are just signaling it to 
                // quit after all events have been written.
                wr.close(); 
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean handles( String cmd ){
        return EventProcessor.ACTION_OBFUSCATE_TO_CLIPBOARD.equals( cmd ) || EventProcessor.ACTION_OBFUSCATE_TO_DISK.equals( cmd );
    }


    public AuditEvent obfuscate( AuditEvent evt ) throws Exception {

        String[] parts = evt.getRawData();
        String[] data = new String[parts.length];
        for( int i = 0; i < parts.length; i++ )
            data[i] = new String(parts[i]);

        String ipPattern = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";

        data[0] = data[0].replaceFirst( ipPattern, "A.B.C.D");
        data[0] = data[0].replaceFirst( ipPattern, "E.F.G.H");

        data[1] = replace( requestReplacements, data[1]);
        data[5] = replace( responseReplacements, data[5] );

        return new ModSecurityAuditEvent( data, AuditEventType.ModSecurity2 );
    }

    public String replace( Hashtable<String,String> rep, String header) throws Exception {
        BufferedReader r = new BufferedReader( new StringReader( header ) );
        String line = null;
        StringBuffer s = new StringBuffer();
        do {
            line = r.readLine();
            if( line != null ){

                line = line.replaceAll("http://[a-z0-9\\.]*/", "http://www.example.com/");

                for( String pat : rep.keySet() )
                    line = line.replaceAll( pat, rep.get(pat) );

                s.append(line + "\n");
            }
        } while( line != null );

        return s.toString();
    }
}
