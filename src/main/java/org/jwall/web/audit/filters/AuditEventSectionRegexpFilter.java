/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.web.audit.AuditEvent;


/**
 * 
 * A simple audit event filter that matches a given section against a regular expression.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditEventSectionRegexpFilter
    extends AuditEventSectionSubstringFilter
{
    
    /** The unique class ID */
    private static final long serialVersionUID = -2522094967073362158L;


    /**
     * This filter checks for an event, if the given string <code>query</code> is contained
     * in the section denoted by <code>section</code>. The boolean parameter handles whether
     * the match will be case-sensitive or case-insensitive.
     * 
     * @param section The ID of the section that is processed @see org.jwall.web.audit.ModSecurity
     *                for a list of predefined section-IDs (generally A=0, B=1, C=2,...).
     * @param query The string that this filter is looking for.
     * @param ignoreCase Denotes whether the search will ignore the case.
     */
    public AuditEventSectionRegexpFilter( int section, String query ){
        super( section, query, true );
    }

    
    /**
     * This is intended for remote-filtering, but currently not used. We can simply
     * return <code>null</code> here.
     * 
     * @see org.jwall.web.audit.AuditEventFilter#filterCommand()
     */
    public String filterCommand()
    {
        return null;
    }
   
    
    /**
     * This method actually does the filtering. It needs to return <code>true</code>, if
     * the filter matches the given audit-event <code>evt</code> and <code>false</code>
     * otherwise.
     * 
     * @see org.jwall.web.audit.AuditEventFilter#matches(org.jwall.web.audit.AuditEvent)
     */
    public boolean matches( AuditEvent evt )
    {
        String sect = evt.getSection( section );
        
        if( sect == null )
            return false;
        
        Pattern p = Pattern.compile( exp );
        Matcher m = p.matcher( sect );
        
        if( negate )
            return !m.find();
        else
            return m.find();
    }
}
