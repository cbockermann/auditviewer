package org.jwall.web.audit.filters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.filter.AuditEventMatch;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class AuditEventDateFilter
    extends AuditEventMatch
{
    /** The unique class ID */
    private static final long serialVersionUID = 1357928312192516533L;

    static DateFormat fmt = new SimpleDateFormat();
    
    @XStreamAsAttribute
    Date fromDate;
    
    @XStreamAsAttribute
    Date toDate;
    
    public AuditEventDateFilter( Date from, Date to ){
        fromDate = from;
        toDate = to;
    }
    

    public String filterCommand()
    {
        StringBuffer s = new StringBuffer();
        
        s.append( "DATE_FORMAT[" );
        
        if( fromDate != null ){
            s.append( "from[" );
            s.append( fmt.format( fromDate ) );
            s.append( "]" );
        }

        if( toDate != null ){
            s.append( "to[" );
            s.append( fmt.format( toDate ) );
            s.append( "]" );
        }

        s.append( "]" );
        
        return null;
    }
    
    public String getVariable(){
        return ModSecurity.DATE;
    }
    
    public Date getFromDate(){
        return fromDate;
    }
    
    public Date getToDate(){
        return toDate;
    }

    public boolean matches(AuditEvent arg0)
    {
        if( fromDate != null && arg0.getDate().before( fromDate ) )
            return false;

        if( toDate != null && arg0.getDate().after( toDate ) )
            return false;
        
        return true;
    }

    
    public String toXML(){
        return AuditEventFilter.getXStream( getClass().getClassLoader() ).toXML( this );
    }
    
    public void addValue( String date ){
    }

    public boolean removeValue( String date ){
        return false;
    }

    public Set<String> getValues()
    {
        TreeSet<String> dates = new TreeSet<String>();
        dates.add( fromDate.getTime() + "" );
        dates.add( toDate.getTime() + "" );
        return dates;
    }


    public String getOperator()
    {
        // TODO Auto-generated method stub
        return null;
    }


    public List<String> getSupportedOperators()
    {
        // TODO Auto-generated method stub
        return null;
    }


    public void setOperator(String op)
    {
        // TODO Auto-generated method stub
        
    }


    public boolean removeValue(Object val)
    {
        // TODO Auto-generated method stub
        if( this.fromDate != null && fromDate.equals( val ) ){
            fromDate = null;
            return true;
        }

        if( this.toDate != null && toDate.equals( val ) ){
            toDate = null;
            return true;
        }

        return false;
    }
}
