/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.ui;

import java.io.File;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jwall.app.ui.FileSelectionPanel;
import org.jwall.app.ui.InputError;
import org.jwall.app.ui.InputPanel;

public class ConcurrentLogReaderPanel extends InputPanel implements
		ChangeListener {
	public final static long serialVersionUID = 453534L;

	FileSelectionPanel indexFile = new FileSelectionPanel("Index File");
	FileSelectionPanel dataDir = new FileSelectionPanel("Data Directory");

	public ConcurrentLogReaderPanel() {
		// setLayout( new GridLayout(2,0) );

		setLayout(null);

		indexFile.setBounds(5, 5, 350, 30);
		indexFile.addChangeListener(this);
		add(indexFile);

		dataDir.setBounds(5, 35, 350, 30);
		add(dataDir);
	}

	@Override
	public boolean checkInput() throws InputError {

		File ddir = dataDir.getFile();
		return ddir != null
				&& ddir.isDirectory()
				&& (indexFile.getFile() == null || indexFile.getFile().isFile());
		/*
		 * 
		 * if( dataDir.getFilename().equals("") && indexFile.getFile() != null
		 * ){ dataDir.setFilename( indexFile.getFile().getPath() ); }
		 * 
		 * return ( indexFile.checkInput() && dataDir.checkInput() );
		 */
	}

	public File getIndexFile() {
		return indexFile.getFile();
	}

	public File getDataDir() {
		return dataDir.getFile();
	}

	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == indexFile && indexFile.getFile() != null
				&& dataDir.getFile() == null) {

			String p = indexFile.getFile().getPath();
			File index = indexFile.getFile();

			dataDir.setFilename(p.substring(0, p.length()
					- index.getName().length()));
		}
	}
}
