/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jwall.AuditViewer;
import org.jwall.app.ui.FileSelectionPanel;
import org.jwall.app.ui.InputError;
import org.jwall.app.ui.InputPanel;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AccessLogAuditReader;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.io.ConcurrentAuditReader;
import org.jwall.web.audit.io.ConcurrentDirectoryReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.io.ModSecurityAuditReader;

public class AuditReaderPanel extends InputPanel implements ItemListener, ChangeListener {
    public final static long serialVersionUID = 546456546L;
    public final static int ACCESS_LOG = 0;
    public final static int SERIAL_MOD_1 = 1;
    public final static int SERIAL_MOD_2 = 2;
    public final static int CONCURRENT = 3;

    JTextField fileNameField, limitField, exp;
    long limit = -1;
    String fileName = "";
    JCheckBox enableFilter = new JCheckBox("Filter On");
    String[] filterVars = {
            // ModSecurity.REQUEST_URI,
            // ModSecurity.REQUEST_METHOD,
            // ModSecurity.QUERY_STRING,
            // ModSecurity.REMOTE_ADDR,
            // ModSecurity.REQUEST_FILENAME,
            ModSecurity.RESPONSE_STATUS // ,
            // ModSecurity.SERVER_NAME
    };

    int cur = 2;
    String[] types = { "Access-Log", "Serial Audit-Log (ModSecurity 1.x)", "Serial Audit-Log (ModSecurity 2.x)",
            "Concurrent Audit-Log" };

    JComboBox<String> logType = new JComboBox<String>(AuditFormat.FORMAT_NAMES);
    JPanel readerPanel;

    FileSelectionPanel logFile = new FileSelectionPanel();
    FileSelectionPanel dataDir = new FileSelectionPanel();
    JLabel dataDirLabel = new JLabel("Data-Directory");

    public AuditReaderPanel() {
        setLayout(new BorderLayout());

        JEditorPane msg = new JEditorPane();
        msg.setBackground(Color.WHITE);
        msg.setFont(msg.getFont().deriveFont(Font.PLAIN));
        JPanel info = new JPanel(new BorderLayout());
        info.add(msg, BorderLayout.CENTER);
        msg.setContentType("text/html");
        msg.setText(
                "<html><body style=\"font-family: Arial,Verdana,Sansserif; font-size:12pt;\">Please choose the audit log file to load events from. When selecting "
                        + "a file, the AuditViewer will try to automatically detect the right format.</body></html>");
        msg.setEditable(false);
        msg.setBorder(new EmptyBorder(25, 15, 15, 15));
        info.add(msg, BorderLayout.CENTER);
        info.setBackground(Color.WHITE);
        this.add(info, BorderLayout.NORTH);

        limitField = new JTextField(8);
        limitField.setText("1000");
        if (System.getProperty(AuditEventReaderDialog.PROPERTY_LIMIT) != null)
            limitField.setText(System.getProperty(AuditEventReaderDialog.PROPERTY_LIMIT));

        logFile = new FileSelectionPanel();
        logFile.addChangeListener(this);
        logFile.setPreferredSize(new Dimension(logFile.getPreferredSize().width, 20));
        dataDir = new FileSelectionPanel();
        dataDir.setVisible(false);
        GridBagLayout g = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        readerPanel = new JPanel();
        readerPanel.setLayout(g);
        readerPanel.setBorder(new EmptyBorder(15, 15, 15, 15));

        Insets defaultInsets = new Insets(2, 0, 2, 0);

        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(2, 0, 2, 10);
        c.weightx = 0.0;
        c.anchor = GridBagConstraints.LINE_START;

        JLabel l = new JLabel("Log-Format ");
        g.setConstraints(l, c);
        readerPanel.add(l);

        c.gridy = 1;
        l = new JLabel("Log-File");
        g.setConstraints(l, c);
        readerPanel.add(l);

        c.gridy = 2;
        dataDirLabel.setVisible(false);
        g.setConstraints(dataDirLabel, c);
        readerPanel.add(dataDirLabel);

        c.gridy = 3;
        l = new JLabel("Limit");
        g.setConstraints(l, c);
        readerPanel.add(l);

        c.weightx = 0.0;
        c.anchor = GridBagConstraints.WEST;
        c.fill = GridBagConstraints.REMAINDER;

        c.gridx = 1;
        c.gridy = 0;

        g.setConstraints(logType, c);
        readerPanel.add(logType);
        c.insets = defaultInsets;

        c.gridy = 1;
        g.setConstraints(logFile, c);
        readerPanel.add(logFile);

        c.gridy = 2;
        g.setConstraints(dataDir, c);
        dataDir.setVisible(false);
        readerPanel.add(dataDir);

        c.fill = GridBagConstraints.NONE;
        c.gridy = 3;
        c.weightx = 30.0d;

        limitField.setMinimumSize(new Dimension(80, 20));
        g.setConstraints(limitField, c);
        readerPanel.add(limitField);

        logType.addItemListener(this);
        add(readerPanel);
    }

    @Override
    public boolean checkInput() throws InputError {
        try {
            limit = Long.parseLong(limitField.getText());
        } catch (NumberFormatException nfe) {
            throw new InputError("Invalid number specified for limit!");
        }

        if (cur == 0 || cur == 1 || cur == 2)
            return logFile.checkInput();

        if (cur == 3) {
            if (dataDir.getFile() == null || !dataDir.getFile().isDirectory())
                throw new InputError("Invalid data-directory specified!");

            if (logFile.getFilename() == null || logFile.getFilename().trim().equals("") || logFile.getFile() == null) {
                System.out.println("No logFile selected, using data-directory only...");
                int ret = JOptionPane.showConfirmDialog(null,
                        "<html>You have not selected an index file. The concurrent reader will recursively <br/>read events from all files found in the selected data-directory!</html>",
                        "", JOptionPane.OK_CANCEL_OPTION);
                if (ret == JOptionPane.CANCEL_OPTION)
                    return false;

                return true;
            }

            return logFile.checkInput();
        }

        return false;
    }

    public void itemStateChanged(ItemEvent e) {
        // System.out.println(e);
        this.select(logType.getSelectedIndex());
    }

    public AuditEventReader getAuditEventReader() {
        AuditEventReader r = null;

        try {
            cur = logType.getSelectedIndex();
            if (cur == 0) {
                r = new AccessLogAuditReader(logFile.getFile());
            }

            if (cur == 1) {
                r = new ModSecurityAuditReader(logFile.getFile());
            }

            if (cur == 2) {
                r = new ModSecurity2AuditReader(logFile.getFile());
            }

            if (cur == 3) {

                File index = logFile.getFile();
                File data = dataDir.getFile();

                if (index == null || !index.isFile()) {

                    r = new ConcurrentDirectoryReader(data);

                } else
                    r = new ConcurrentAuditReader(data, index);

            }

        } catch (IOException ie) {
            JOptionPane.showMessageDialog(null, "An I/O-error occurred: " + ie.getMessage(), "I/O Error!",
                    JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            AuditViewer.handleException(e);
            return null;
        }

        return r;
    }

    public void select(int i) {
        if (i < 0 || i > 3)
            return;

        // readerPanel.removeAll();

        if (i == 3)
            checkDataDir();

        dataDir.setVisible(i == 3);
        dataDirLabel.setVisible(i == 3);

        cur = i;
        readerPanel.revalidate();
        if (getParent() != null) {
            getParent().validate();
            getParent().repaint();
        }
    }

    public long getLimit() {
        return limit;
    }

    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == logFile) {

            try {
                if (logFile.getFile() == null)
                    return;

                int format = AuditFormat.guessFormat(new FileInputStream(logFile.getFile()));
                if (format != AuditFormat.UNKNOWN_FORMAT) {
                    select(format);
                    logType.setSelectedIndex(format);
                    // System.out.println("Format most likely is:
                    // "+AuditFormat.FORMAT_NAMES[format]
                    // );
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (cur != 3)
            return;

        if (e.getSource() == logFile) {
            checkDataDir();
        }
    }

    public void checkDataDir() {
        if ((dataDir.getFilename() == null || dataDir.getFilename().equals(""))) {
            String p = logFile.getFile().getPath();
            File index = logFile.getFile();

            dataDir.setFilename(p.substring(0, p.length() - index.getName().length()));
        }
    }

    public void saveProperties() {
        logFile.saveProperties();
    }
}
