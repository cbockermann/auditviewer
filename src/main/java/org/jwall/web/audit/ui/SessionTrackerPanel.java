/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.URL;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.jwall.app.ui.DisplayDialog;
import org.jwall.app.ui.InputError;
import org.jwall.app.ui.InputPanel;
import org.jwall.web.audit.session.CookieSessionTracker;
import org.jwall.web.audit.session.HeuristicSessionTracker;
import org.jwall.web.audit.session.SessionTracker;

public class SessionTrackerPanel extends InputPanel implements ItemListener, ChangeListener {
    private static final long serialVersionUID = -8104391904615095159L;

    public final static int ACCESS_LOG = 0;
    public final static int SERIAL_MOD_1 = 1;
    public final static int SERIAL_MOD_2 = 2;
    public final static int CONCURRENT = 3;

    int cur = 2;
    String[] types = { "Heuristic IP-Based Tracker", "Cookie-Based Session Tracker" };

    final JComboBox<String> logType = new JComboBox<String>(types);
    JPanel readerPanel;

    JLabel cookieLabel = new JLabel("Cookie Name");
    JPanel cookiePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    JTextField cookieNameField = new JTextField(12);
    JTextField timeOutField = new JTextField(8);
    JCheckBox strictSessions = new JCheckBox();

    public SessionTrackerPanel() {
        // setLayout( new GridLayout(2,0) );
        setLayout(new BorderLayout());
        JEditorPane msg; // = new JLabel();
        msg = new JEditorPane();
        msg.setBackground(Color.WHITE);
        JPanel info = new JPanel(new BorderLayout());
        info.add(msg, BorderLayout.CENTER);
        msg.setContentType("text/html");
        msg.setText(
                "<html><body style=\"font-family: Arial,Verdana,Sansserif; font-size:12pt;\">Please choose the appopriate session tracker based on your application. See "
                        + "<a href=\"file:///docs/session.html\">Help</a> for details!</body></html>");
        msg.setEditable(false);
        msg.setFont(msg.getFont().deriveFont(Font.PLAIN));
        msg.setBorder(new EmptyBorder(25, 15, 15, 15));
        msg.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent e) {

                if (e.getEventType() != HyperlinkEvent.EventType.ACTIVATED)
                    return;

                System.out.println(e);
                URL url = e.getURL();
                System.out.println("URL is " + url);
                if (url != null) {
                    URL doc = SessionTrackerPanel.class.getResource(url.getPath());
                    DisplayDialog dd = new DisplayDialog(doc);
                    dd.setVisible(true);
                }
            }
        });

        info.add(msg, BorderLayout.CENTER);
        info.setBackground(Color.WHITE);
        this.add(info, BorderLayout.NORTH);

        cookieNameField.setText("JSESSIONID");
        cookiePanel.add(cookieNameField);
        timeOutField.setText("3600");
        strictSessions.setSelected(true);
        // logFile.setBounds( 5, 60, 220, 30 );
        // dataDir.setBounds( 5, 60, 220, 30 );
        GridBagLayout g = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        readerPanel = new JPanel();
        readerPanel.setLayout(g);
        readerPanel.setBorder(new EmptyBorder(15, 15, 15, 15));

        Insets defaultInsets = new Insets(2, 0, 2, 0);

        c.gridx = 0;
        c.gridy = 0;
        c.insets = defaultInsets;
        c.weightx = 0.4;
        c.anchor = GridBagConstraints.WEST;

        JLabel l = new JLabel("Session Tracker");
        g.setConstraints(l, c);
        readerPanel.add(l);

        c.gridy = 1;
        cookieLabel = new JLabel("Cookie-Name");
        cookieLabel.setVisible(false);
        g.setConstraints(cookieLabel, c);
        readerPanel.add(cookieLabel);

        c.gridy = 2;
        c.weightx = 1.0;
        l = new JLabel("Time-Out");
        g.setConstraints(l, c);
        readerPanel.add(l);

        c.gridy = 3;
        l = new JLabel("Strict Sessions");
        g.setConstraints(l, c);
        readerPanel.add(l);

        c.fill = GridBagConstraints.REMAINDER;
        c.anchor = GridBagConstraints.WEST;

        c.gridx = 1;
        c.gridy = 0;
        c.insets = new Insets(2, 4, 4, 0);
        g.setConstraints(logType, c);
        readerPanel.add(logType);
        c.insets = defaultInsets;

        c.gridy = 1;
        cookiePanel.setVisible(false);
        g.setConstraints(cookiePanel, c);
        readerPanel.add(cookiePanel);

        c.gridy = 2;
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(timeOutField);
        g.setConstraints(p, c);
        readerPanel.add(p);

        c.gridy = 3;
        p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(strictSessions);
        g.setConstraints(p, c);
        readerPanel.add(p);

        // logType.setPreferredSize( new Dimension( 200, 22 ) );
        // logType.setBounds( 105, 5, 200, 22 );
        logType.addItemListener(this);
        // add( logType );
        add(readerPanel);
    }

    @Override
    public boolean checkInput() throws InputError {

        try {
            Long.parseLong(timeOutField.getText());
        } catch (Exception e) {
            throw new InputError("Invalid time-out value!");
        }

        return true;
    }

    public void itemStateChanged(ItemEvent e) {
        this.select(logType.getSelectedIndex());
    }

    public SessionTracker getSessionTracker() {
        SessionTracker t = null;

        try {
            cur = logType.getSelectedIndex();
            long l = 1000 * Long.parseLong(timeOutField.getText());

            if (cur == 0) {
                t = new HeuristicSessionTracker(l);
            }

            if (cur == 1) {
                String cookie = cookieNameField.getText();
                t = new CookieSessionTracker(cookie);
            }

            if (t != null)
                t.setSessionTimeOut(l);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return t;
    }

    public void select(int i) {
        if (i < 0 || i > types.length)
            return;

        cookieLabel.setVisible(i == 1);
        cookiePanel.setVisible(i == 1);

        cur = i;
        readerPanel.revalidate();
        if (getParent() != null) {
            getParent().validate();
            getParent().repaint();
        }
    }

    public void stateChanged(ChangeEvent e) {
    }
}
