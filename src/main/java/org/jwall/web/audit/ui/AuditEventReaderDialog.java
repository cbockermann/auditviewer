/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.jwall.app.ui.FileSelectionPanel;
import org.jwall.app.ui.InputDialog;
import org.jwall.app.ui.InputError;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;

public class AuditEventReaderDialog extends InputDialog {
    private static final long serialVersionUID = 4366969493021633059L;

    public static final String PROPERTY_FILTER_EXPRESSION = "jwall.file.filterExpression";
    public static final String PROPERTY_FILTER_VARIABLE = "jwall.file.filterVariable";
    public static final String PROPERTY_FILTER_ENABLED = "jwall.file.filterEnabled";
    public static final String PROPERTY_LIMIT = "jwall.file.limit";
    public static final String PROPERTY_FILE_NAME = "jwall.file.fileName";

    public final static int ACCESS_LOG = 0;
    public final static int SERIAL_MOD_1 = 1;
    public final static int SERIAL_MOD_2 = 2;
    public final static int CONCURRENT = 3;

    JTextField fileNameField, limitField, exp;
    long limit = -1;
    String fileName = "";
    JCheckBox enableFilter = new JCheckBox("Filter On");
    String[] filterVars = {
            // ModSecurity.REQUEST_URI,
            // ModSecurity.REQUEST_METHOD,
            // ModSecurity.QUERY_STRING,
            // ModSecurity.REMOTE_ADDR,
            // ModSecurity.REQUEST_FILENAME,
            ModSecurity.RESPONSE_STATUS // ,
            // ModSecurity.SERVER_NAME
    };

    String[] fileFormats = { "Access-Log", "Serial Audit-Log (ModSecurity 1.x)", "Serial Audit-Log (ModSecurity 2.x)",
            "Concurrent Audit-Log" };

    final JComboBox<String> filter = new JComboBox<String>(filterVars);
    final JComboBox<String> format = new JComboBox<String>(fileFormats);

    AuditEventReader reader = null;
    AuditReaderPanel readerPanel;

    public AuditEventReaderDialog() {
        super();
        setTitle("Load Events From File...");
        setModal(true);

        JPanel info = new JPanel(new BorderLayout());
        info.setBackground(Color.WHITE);
        info.setBorder(new EmptyBorder(20, 10, 10, 10));

        readerPanel = new AuditReaderPanel();
        readerPanel.select(2);
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(readerPanel, BorderLayout.CENTER);

        okButton.setEnabled(true);
        setSize(540, 270);
        setResizable(false);
        center();
    }

    /**
     * @see org.jwall.core.ui.InputDialog#checkInput()
     */
    @Override
    public boolean checkInput() throws InputError {
        if (!this.isVisible())
            return true;

        if (!readerPanel.checkInput())
            return false;

        return true;
    }

    /**
     * @see org.jwall.core.ui.InputDialog#doCancel()
     */
    @Override
    public void doCancel() {
        limit = 0;
        fileName = null;
        setVisible(false);
        reader = null;
    }

    /**
     * @see org.jwall.core.ui.InputDialog#doOk()
     */
    @Override
    public void doOk() {
        try {
            if (checkInput()) {
                reader = readerPanel.getAuditEventReader();
                setVisible(false);
                limit = readerPanel.getLimit();

                String history = System.getProperty(FileSelectionPanel.PROPERTY_LAST_FILE_NAMES);
                if (history == null)
                    history = fileName;
                else {
                    StringBuffer h = new StringBuffer(fileName);
                    String[] files = history.split(":");
                    for (int i = 0; i < files.length; i++) {
                        if (!files[i].equals(fileName))
                            h.append(":" + files[i]);
                    }
                    history = h.toString();
                }

                System.setProperty(FileSelectionPanel.PROPERTY_LAST_FILE_NAMES, history);

                readerPanel.saveProperties();
            }
        } catch (InputError e) {
            JOptionPane.showMessageDialog(this,
                    "<html>Input error: <p style=\"padding: 10px;\">" + e.getMessage() + "</html>");
        }
    }

    public AuditEventReader getAuditEventReader() {
        return reader;
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent arg0) {
        if ("set.file".equals(arg0.getActionCommand())) {

            JFileChooser f = new JFileChooser();
            int ret = f.showOpenDialog(this);

            if (ret == JFileChooser.APPROVE_OPTION) {
                fileNameField.setText(f.getSelectedFile().getAbsolutePath());
            }
        }

        if (arg0.getSource() == enableFilter) {
            exp.setEnabled(enableFilter.isSelected());
            filter.setEnabled(enableFilter.isSelected());
        }
    }

    public long getLimit() {
        return limit;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isFilterEnabled() {
        return enableFilter.isSelected();
    }

    public String getFilterExpression() {
        return exp.getText();
    }

    public String getFilterVariable() {
        return filterVars[filter.getSelectedIndex()];
    }

    public void setFilterVariable(String s) {
        for (int i = 0; i < filterVars.length; i++) {
            if (filterVars[i].equals(s)) {
                filter.setSelectedIndex(i);
                return;
            }
        }
    }

    public void setFilterExpression(String s) {
        exp.setText(s);
    }

    public void setFilterEnabled(boolean b) {
        enableFilter.setSelected(b);
        exp.setEnabled(enableFilter.isSelected());
        filter.setEnabled(enableFilter.isSelected());
    }

    public void setProperties() {
        System.setProperty(PROPERTY_FILE_NAME, getFileName());
        System.setProperty(AuditEventReaderDialog.PROPERTY_LIMIT, getLimit() + "");
        System.setProperty(AuditEventReaderDialog.PROPERTY_FILTER_ENABLED, isFilterEnabled() + "");
        System.setProperty(AuditEventReaderDialog.PROPERTY_FILTER_VARIABLE, getFilterVariable());
        System.setProperty(AuditEventReaderDialog.PROPERTY_FILTER_EXPRESSION, getFilterExpression());
    }

    public static void main(String[] args) {
        try {

            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

            System.setProperty(AuditEventReaderDialog.PROPERTY_LIMIT, "1000");
            System.setProperty(FileSelectionPanel.PROPERTY_LAST_FILE_NAMES,
                    "/www/audit.log:/Users/chris/dortmund-dragons.de-audit.log");
            AuditEventReaderDialog d = new AuditEventReaderDialog();
            d.setVisible(true);

            System.out.println("Reader: " + d.getAuditEventReader());
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
