/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.jwall.app.ui.InputDialog;
import org.jwall.app.ui.InputError;
import org.jwall.web.audit.session.SessionTracker;


/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class SessionTrackerDialog
extends InputDialog
{
    private static final long serialVersionUID = 8348132694694267671L;

    public static final String PROPERTY_FILTER_EXPRESSION = "jwall.file.filterExpression";
	public static final String PROPERTY_FILTER_VARIABLE = "jwall.file.filterVariable";
	public static final String PROPERTY_FILTER_ENABLED = "jwall.file.filterEnabled";
	public static final String PROPERTY_LIMIT = "jwall.file.limit";
	public static final String PROPERTY_FILE_NAME = "jwall.file.fileName";

	SessionTracker tracker = null;
	SessionTrackerPanel readerPanel;

	public SessionTrackerDialog(){
		super();
		setTitle("Create Session Tracker");
		setModal( true );

		readerPanel = new SessionTrackerPanel();
		readerPanel.select(2);
		getContentPane().setLayout( new BorderLayout() );
		getContentPane().add( readerPanel, BorderLayout.CENTER );

		okButton.setEnabled( true );
		setSize( 500, 280 );

		center();
	}

	/* (non-Javadoc)
	 * @see org.jwall.core.ui.InputDialog#checkInput()
	 */
	@Override
	public boolean checkInput() throws InputError
	{
		if(! this.isVisible())
			return true;

		if(! readerPanel.checkInput() )
			return false;
		
		return true;
	}

	/* (non-Javadoc)
	 * @see org.jwall.core.ui.InputDialog#doCancel()
	 */
	@Override
	public void doCancel()
	{
		tracker = null;
		setVisible(false);
	}

	/* (non-Javadoc)
	 * @see org.jwall.core.ui.InputDialog#doOk()
	 */
	@Override
	public void doOk()
	{
		try {
			if(checkInput()){
				tracker = readerPanel.getSessionTracker();
				setVisible(false);
			}
		} catch (InputError e){
			JOptionPane.showMessageDialog(this, "<html>Input error: <p style=\"padding: 10px;\">"+e.getMessage() 
					+ "</html>" );
		}
	}

	public SessionTracker getSessionTracker(){
		return tracker;
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0)
	{
	}

	public void setProperties(){
	}
}
