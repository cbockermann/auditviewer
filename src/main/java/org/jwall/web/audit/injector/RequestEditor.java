/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.injector;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.StringReader;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jwall.app.Application;
import org.jwall.app.ui.Dialog;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.viewer.ui.TextPanel;
import org.jwall.web.http.HttpHeader;

/**
 * 
 * This class implements a simple editor for manipulating HTTP request headers
 * and the pay load. The modified requests can then be re-injected back into the
 * server. The purpose of this tool is to test for bugs or the detection ability
 * of previously created rules within a web-application firewall system.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class RequestEditor extends Dialog implements ChangeListener,
		InjectionListener, ActionListener {
	private static final long serialVersionUID = 1224838124274851565L;

	/** The event that is edited */
	AuditEvent event = null;

	/** Some UI components */
	JButton in;
	JTextField server = new JTextField(20);
	JTextField portField = new JTextField(4);
	JCheckBox sslBox = new JCheckBox();
	JCheckBox trustAll = new JCheckBox();
	JCheckBox contentLength = new JCheckBox();
	TextPanel reqHead = new TextPanel();
	TextPanel reqBody = new TextPanel();
	TextPanel resHead = new TextPanel();
	TextPanel resBody = new TextPanel();

	/** The injector thread */
	InjectorThread injector = null;

	/**
	 * 
	 * 
	 * @param l
	 */
	public RequestEditor(AuditEvent evt) {
		this.setModal(false);

		setLayout(new BorderLayout());

		Font mono = new Font("Monospaced", Font.PLAIN, 13);

		JPanel tb = new JPanel(new FlowLayout(FlowLayout.LEFT));
		server.setFont(mono);
		tb.add(new JLabel("WebServer:"));
		tb.add(server);

		tb.add(new JLabel("Port:"));
		tb.add(portField);

		contentLength.setSelected(true);
		tb.add(contentLength);
		tb.add(new JLabel("adjust Content-Length"));

		sslBox.addChangeListener(this);
		tb.add(sslBox);
		tb.add(new JLabel("use SSL"));

		tb.add(trustAll);
		tb.add(new JLabel("trust all Certs"));

		in = new JButton("Inject");
		in.setIcon(Application.getIcon(InjectorThread.START_INJECT));
		in.setActionCommand(InjectorThread.START_INJECT);
		in.addActionListener(this);

		tb.add(in);

		this.getContentPane().add(tb, BorderLayout.NORTH);

		// JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton close = new JButton("Close");
		close.setIcon(Application.getIcon("org.jwall.app.close"));
		close.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		tb.add(close);
		// buttons.add(close);
		// this.getContentPane().add(buttons, BorderLayout.SOUTH);

		Color lb = new Color(0xd7, 0xe0, 0xe8);
		// Color bc = new Color( 0x00, 0x4e, 0x93 );
		Color db = new Color(0x00, 0x53, 0x87);
		// bc = lb;
		lb = new Color(0xce, 0xe2, 0xef);
		Color c1 = new Color(0xc0, 0xd5, 0xe8);

		reqHead.setBackground(lb);
		reqHead.setFont(mono);
		reqHead.setEditable(true);
		reqHead.setForeground(db);

		reqBody.setBackground(c1);
		reqBody.setFont(mono);
		reqBody.setEditable(true);

		reqBody.setForeground(db);
		reqBody.setBackground(new Color(0xe0, 0xe0, 0xf1));
		reqBody.setColumns(80);
		reqBody.setLineWrap(true);
		reqBody.setMinimumSize(new Dimension(30, 30));
		// reqBody.setBorder( BorderFactory.createLineBorder( Color.RED ) );

		resHead = new TextPanel();
		resHead.setFont(mono);
		resHead.setBackground(new Color(0xf2, 0xda, 0xef));
		resHead.setForeground(new Color(0x68, 0x05, 0x5e));

		resBody = new TextPanel();
		resBody.setColumns(80);
		resBody.setLineWrap(true);
		resBody.setFont(mono);
		resBody.setBackground(new Color(0xf2, 0xa7, 0xea));
		resBody.setForeground(new Color(0x68, 0x05, 0x5e));

		JPanel res = new JPanel();
		res.setLayout(new BoxLayout(res, BoxLayout.Y_AXIS));
		res.add(resHead);
		res.add(resBody);
		res.setBorder(BorderFactory
				.createLineBorder(new Color(0x9b, 0x1d, 0x8e)));

		JPanel list = new JPanel();
		list.setLayout(new BoxLayout(list, BoxLayout.Y_AXIS));

		JPanel req = new JPanel();
		// req.setLayout(new BoxLayout(req, BoxLayout.Y_AXIS));
		req.setLayout(new BorderLayout());
		req.add(reqHead, BorderLayout.NORTH);
		req.add(reqBody, BorderLayout.SOUTH);
		req.setBorder(BorderFactory.createLineBorder(db));

		JScrollPane reqScroller = new JScrollPane(req);
		reqScroller.getVerticalScrollBar().setUnitIncrement(30);
		JScrollPane resScroller = new JScrollPane(res);
		resScroller.getVerticalScrollBar().setUnitIncrement(30);
		JSplitPane p = new JSplitPane(0, reqScroller, resScroller);

		getContentPane().add(p, BorderLayout.CENTER);

		setSize(800, 600);
		center();
		setVisible(true);

		setEvent(evt);

		p.setDividerLocation(0.5d);
	}

	public void stateChanged(ChangeEvent e) {
		if (e.getSource() == sslBox) {
			if (sslBox.isSelected())
				portField.setText("443");
			else
				portField.setText(event.get(ModSecurity.SERVER_PORT));
		}
	}

	public void setEvent(AuditEvent e) {
		event = e;
		setTitle(e.getDate() + "");

		String s = e.get(ModSecurity.SERVER_ADDR);
		server.setText(s);

		s = e.get(ModSecurity.SERVER_PORT);
		portField.setText(s);

		sslBox.setSelected(s.endsWith("3"));

		s = e.getSection(ModSecurity.SECTION_REQUEST_HEADER);
		reqHead.setText(s);

		s = e.getSection(ModSecurity.SECTION_REQUEST_BODY);
		// System.out.println( "Setting req-body to '" + s + "'" );
		if (s.trim().equals(""))
			s = "some body...";

		reqBody.setText(s);

		this.repaint();
	}

	public String getServerName() throws Exception {
		return server.getText().trim();
	}

	public Integer getPort() throws Exception {
		return Integer.parseInt(portField.getText());
	}

	public boolean isSSLEnabled() {
		return sslBox.isSelected();
	}

	public boolean isTrustAll() {
		return trustAll.isSelected();
	}

	public void setRequestHeader(String head) {
		reqHead.setText(head);
	}

	public String getRequestHeader() {
		return reqHead.getText();
	}

	public void setRequestBody(String body) {
		reqBody.setText(body);
	}

	public String getRequestBody() {
		return reqBody.getText();
	}

	public void setResponseHeader(String head) {
		resHead.setText(head);
	}

	public void setResponseBody(String body) {
		resBody.setText(body);
	}

	/**
	 * 
	 * This method parses the user modified data (request headers, request body)
	 * and ensures that the header is somewhat "intact". This basically checks
	 * for possible empty lines within the header and adjusts the
	 * <code>Content-Length</code> field if desired to do so.
	 * 
	 * @return A string array holding the request header and (optional) the
	 *         request body.
	 * @throws Exception
	 *             In case an error occurred.
	 */
	public String[] prepareRequest() throws Exception {
		StringBuffer outGoing = new StringBuffer();

		char[] content = reqBody.getText().toCharArray();
		int cs = content.length;
		// System.out.println("Request body size  is " + cs + " bytes");
		BufferedReader r = new BufferedReader(new StringReader(
				reqHead.getText()));
		String line = null;
		boolean csInserted = false;
		boolean isPost = false;
		do {
			line = r.readLine();
			isPost = isPost || (line != null && line.startsWith("POST")); // weather
																			// request
																			// is
																			// post
																			// or
																			// not

			if (line != null)
				line = line.trim();

			if (line != null && !"".equals(line)) {

				if (line != null && line.startsWith("Content-Length:")
						&& cs > 0 && contentLength.isSelected()) {
					line = "Content-Length: " + cs;
					csInserted = true;
				}

				if ((line == null || "".equals(line))
						&& (isPost && !csInserted && cs > 0)) {
					// out.println("Content-Length: "+cs);
					outGoing.append("Content-Length: " + cs + HttpHeader.CRLF);
					csInserted = true;
				}

				if (line != null) {
					// out.println(line);
					outGoing.append(line + HttpHeader.CRLF);
				}
			}

		} while (line != null);

		String finalRequest = outGoing.toString().trim() + HttpHeader.CRLF
				+ HttpHeader.CRLF;
		// content = finalRequest.toCharArray();
		// System.out.println("sending request-header.....\n----------------");
		// System.out.print( finalRequest );
		// System.out.println("------------------");

		return new String[] { finalRequest, new String(content) };
	}

	public void actionPerformed(ActionEvent e) {
		System.out.println(e);
		if (e.getActionCommand().equals(InjectorThread.START_INJECT)) {
			InjectorThread injector = new InjectorThread(this);
			injector.listener.add(this);
			injector.startInjection();
		}

		if (e.getActionCommand().equals(InjectorThread.CANCEL_INJECT)) {
			injector.cancelInjection();
			injector = null;
		}
	}

	public void injectionStarted() {
		in.setIcon(Application.getIcon(InjectorThread.CANCEL_INJECT));
		in.setActionCommand(InjectorThread.CANCEL_INJECT);
	}

	public void injectionFinished() {
		in.setIcon(Application.getIcon(InjectorThread.START_INJECT));
		in.setActionCommand(InjectorThread.START_INJECT);
	}
}
