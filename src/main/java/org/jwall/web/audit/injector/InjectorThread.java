/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.injector;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.Socket;
import java.nio.channels.Channels;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.swing.JOptionPane;

import org.jwall.AuditViewer;
import org.jwall.tools.ZeroTrustManager;
import org.jwall.web.http.HttpChunk;
import org.jwall.web.http.HttpResponse;
import org.jwall.web.http.nio.HttpResponseChannel;
import org.jwall.web.http.nio.TimeOutException;


/**
 * 
 * This class implements the core of the injector. It is connected with the request-editor
 * holding the request and registers itself to the actions of this editor. Thus, the injection 
 * starts, once an &quot;inject&quot;-action has been fired from within the editor.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class InjectorThread
implements ActionListener
{
    public final static String START_INJECT = "org.jwall.web.audit.event.inject.start";
    public final static String CANCEL_INJECT = "org.jwall.web.audit.event.inject.cancel";
    public final static String PROPERTY_CONNECTION_TIME_OUT = "org.jwall.web.audit.viewer.inject_timeout"; 

    static {
        System.setProperty("org.jwall.web.audit.viewer.injector.version", "$Revision$");
    }


    /** The list of trust managers used for certificate validation in case of an ssl-injection */
    TrustManager[] trustManagers;

    /** A list of listeners to be notified once the injection has been completed */
    List<InjectionListener> listener = new LinkedList<InjectionListener>();

    /** A thread which will actually do the injection */
    Thread injectorThread = null;

    /** A flag signaling whether an injection is currently in progress */
    boolean injecting = false;

    /** The editor from which the event to be injected will be acquired */
    RequestEditor editor = null;

    /** The default timeout for a connection is 5 seconds */
    long timeout = 5000L;

    /**
     * This constructor creates a new injector and implicitly a new request editor.
     * The editor is displayed as soon as the <code>show()</code> method of this
     * injector is called.
     * 
     * @param e The event that is displayed within the editor.
     */
    public InjectorThread( RequestEditor editor ){
        this.editor = editor;

        try {
            Class<?> clazz = Class.forName( "org.jwall.security.cert.CustomTrustManager" );
            Method m = clazz.getMethod( "getInstance", (Class[]) null );
            TrustManager tm = (TrustManager) m.invoke(null);
            trustManagers = new TrustManager[]{ tm };
            System.out.println("Using reflected trustManager "+ trustManagers[0] );
        } catch (Exception e) {
            e.printStackTrace();
            trustManagers = new TrustManager[]{ new ZeroTrustManager() };
        }

        if( editor.isTrustAll() || "true".equalsIgnoreCase( System.getProperty( "inject.trust-all" ) ) )
            trustManagers = new TrustManager[]{ new ZeroTrustManager() };
        
        System.out.println("Using trustManager "+ trustManagers[0] );
    }


    public void showEditDialog(){
        editor.setVisible( true );
    }


    public void startInjection(){
        injectorThread = new Thread(new Runnable(){
            public void run(){
                injectionStarted();
                inject();
                injectionFinished();
            }
        });

        injectorThread.start();
    }

    public void addInjectionListener( InjectionListener l ){
        listener.add(l);
    }

    public void removeInjectionListener( InjectionListener l ){
        listener.remove(l);
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed( ActionEvent e ){

        if( e.getActionCommand().equals( START_INJECT ) )
            startInjection();

        if( e.getActionCommand().equals( CANCEL_INJECT ) )
            cancelInjection();

    }


    /**
     * 
     * This method is the really interesting part. It will fetch the request from the
     * request editor and initialize a TCP connection. The connection will be established
     * based on the properties set by the user (SSL, server name, port, etc.)
     * 
     */
    public void inject(){
        Socket socket = null;
        PrintStream out = null;
        InputStream in = null;

        try {

            String serverName = editor.getServerName();
            Integer serverPort = editor.getPort();

            if( editor.isSSLEnabled() ){
                //
                // in case SSL is enabled, we install a ZeroTrustManager which basically
                // verifies all server certificates regardless of their origin...
                //
                // TODO: Maybe we might prompt the user for validation?

                SSLContext context = SSLContext.getInstance("SSL"); 
                context.init(null, trustManagers, null); 

                // once the trust manager is in place, we now proceed by creating the SSLContext
                // using a new socket connection (SSL socket as returned by the SSLSocketFactory)
                //
                SSLSocketFactory sf = context.getSocketFactory();  
                socket = sf.createSocket( serverName, serverPort );

            } else {
                //
                // in case of plain HTTP we simply need an ordinary TCP connection using a plain socket:
                //
                socket = new Socket( serverName, serverPort );
            }

            // so the socket is now a TCP connection (either SSL encrypted or plain) and we can use
            // its streams for reading an writing.
            //
            out = new PrintStream( socket.getOutputStream() );
            in = socket.getInputStream();


            // first we need to retrieve the request from the request-editor. calling
            // "prepareRequest" already ensures that content-length is adjusted, blank lines
            // are removed, i.e. the request-header is in the right format
            //
            String[] request = editor.prepareRequest();
            String requestHeader = request[0];
            out.print( requestHeader );

            //
            // in case the request also contains a body, we need to send this as well
            //
            char[] content = null;
            if( request.length > 0 )
                content = request[1].toCharArray();

            if( content != null && content.length > 0 ){
                out.print( content );
            }

            // finally we flush the output to ensure the request is sent...
            //
            out.flush();


            //
            // after the message has been sent, we read the response...
            //
            HttpResponseChannel rc = new HttpResponseChannel( Channels.newChannel( in ) );
            HttpResponse r = rc.readMessage();
            long start = System.currentTimeMillis();
            long lastRead = start;

            while( r == null && lastRead - start < timeout ){

                if( lastRead - start > timeout )
                    throw new TimeOutException("Time out while reading server response!");

                try {
                    Thread.sleep( 1000 );
                } catch (Exception e){}

                r = rc.readMessage();
                lastRead = System.currentTimeMillis();
            }

            if( r != null ){
                String responseBody = r.getBodyAsString();

                //
                // in case the response is transfered in chunked encoding, we need to read all
                // the chunks and start a reassembly...
                //
                if( r != null && r.isChunked() ){
                    StringBuffer response = new StringBuffer();
                    HttpChunk ch = null;
                    do {
                        
                        do {
                            ch = rc.readChunk();
                            if( ch == null ){
                                System.out.println( "Sleeping for 250ms... until new chunk data arrived!" );
                                Thread.sleep( 250 );
                            }
                        } while ( ch == null );

                        response.append( ch.getBodyAsString() );
                    } while( ch.size() > 0 );

                    responseBody = response.toString();
                }
                //
                // next, we display the response header and body to the user:
                //
                editor.setResponseHeader( r.getHeader() );
                editor.setResponseBody( responseBody );
            } else {
                
                JOptionPane.showMessageDialog( null, "<html>The request timed out!</html>", "Request timed out", JOptionPane.ERROR_MESSAGE );
                
            }

            out.close();
            socket.close();

        } catch (SSLException e) {

            JOptionPane.showMessageDialog( null, "<html>The injection was cancelled due to an untrusted certificate<br>which has not been validated by the user.</html>", "Injection cancelled", JOptionPane.WARNING_MESSAGE );

        } catch (TimeOutException toe ){

            JOptionPane.showMessageDialog( null, "<html>The connection timed out while waiting for a server response. The time-out currently is set to " + timeout + ". </html>", "Connection Time Out", JOptionPane.ERROR_MESSAGE );

        } catch (RuntimeException rte ){
            AuditViewer.handleException( rte );
        } catch (Exception e) {
            e.printStackTrace();
            editor.displayError( e );
        } finally {
            try {
                out.close();
                socket.close();
            } catch (Exception ex) {
            }
        }
    }


    /**
     * Notifies all listeners that the injection has been started.
     */
    public void injectionStarted(){
        for( InjectionListener l : listener )
            l.injectionStarted();
    }


    /**
     * Cancels the injection and notifies all injection listeners.
     */
    public void cancelInjection(){
        if( injectorThread != null){
            injectorThread.interrupt();
            injectorThread = null;
        }

        for( InjectionListener l : listener )
            l.injectionFinished();
    }


    /**
     * Notifies all listeners that the injection has been finished.
     */
    public void injectionFinished(){
        injectorThread = null;
        injecting = false;

        for( InjectionListener l : listener )
            l.injectionFinished();
    }
}
