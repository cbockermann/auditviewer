/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.injector;

import java.util.Collection;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.viewer.ui.EventProcessor;

public class Injector
    implements EventProcessor
{

    public boolean handles(String actionCommand)
    {
        return EventProcessor.ACTION_INJECT.equals( actionCommand );
    }

    public void processEvent(String cmd, Collection<AuditEvent> evt)
    {
        if(EventProcessor.ACTION_INJECT.equals(cmd) && evt.size() == 1){
            RequestEditor inj = new RequestEditor( evt.iterator().next() );
            inj.setVisible( true );
        }
    }
}
