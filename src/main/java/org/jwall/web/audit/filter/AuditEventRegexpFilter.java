package org.jwall.web.audit.filter;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.rules.ValueExtractor;

public class AuditEventRegexpFilter extends AuditEventFilter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 169331275718717038L;

	String variable;
	Pattern pattern;
	
	public AuditEventRegexpFilter( String var, String rx ){
		variable = var;
		pattern = Pattern.compile( rx);
	}

	@Override
	public boolean matches(AuditEvent evt) {
		
		List<String> values = ValueExtractor.extractValues( variable, evt);
		if( values == null || values.isEmpty() )
			return false;
		
		for( String value : values ){
			Matcher m = pattern.matcher( value );
			if( m.find() )
				return true;
		}

		return false;
	}
}
