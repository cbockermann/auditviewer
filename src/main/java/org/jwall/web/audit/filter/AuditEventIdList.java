package org.jwall.web.audit.filter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.jwall.web.audit.AuditEvent;

public class AuditEventIdList extends AuditEventMatch {

	/** The unique class ID */
	private static final long serialVersionUID = -1072665922617802265L;

	Set<String> ids = new HashSet<String>();

	boolean excludeList = false;
	
	public void add( String id ){
		ids.add( id );
	}


	@Override
	public boolean matches(AuditEvent evt) {
		
		String id = evt.getEventId();
		if( id == null )
			return false;
		
		return ids.contains( id );
	}

	public Collection<String> getEventIds(){
		return Collections.unmodifiableSet( ids);
	}

	public boolean isExcludeList() {
		return excludeList;
	}


	public void setExcludeList(boolean excludeList) {
		this.excludeList = excludeList;
	}
}
