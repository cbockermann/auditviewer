/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.jwall.AuditViewer;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.viewer.table.DefaultColumnMapper;
import org.jwall.web.audit.viewer.table.TableColumnMapper;

/**
 * <p>
 * This class implements a table model for displaying a table of audit events.
 * It also implements the AuditDataView interface as it uses instances of this
 * interface as the underlying data store.
 * </p>
 * <p>
 * Basically the implementation consists of a data view, providing the events
 * contained in the table and a table-column-mapper implementation, which
 * extract the cell-values from the list of events found in the data view.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AuditDataTable extends AbstractTableModel implements
		AuditDataView, DataViewListener {
	/** The unique class ID */
	private static final long serialVersionUID = 2821887967019070952L;

	/**
	 * This is the standard implementation for extracting the different
	 * column-values of a row
	 */
	TableColumnMapper<AuditEvent> columnMapper = new DefaultColumnMapper();

	/** A simple format for displaying an event's date */
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yy hh:mm:ss");

	/** The view providing the audit data */
	AuditDataView dataView;

	public AuditDataTable(AuditDataView dataView,
			TableColumnMapper<AuditEvent> customMapper) {
		this(dataView);
		columnMapper = customMapper;
	}

	public AuditDataTable(AuditDataView dataView) {
		this.dataView = dataView;
		this.dataView.addDataViewListener(this);
	}

	public TableColumnMapper<AuditEvent> getColumnMapper() {
		return columnMapper;
	}

	public void setColumnMapper(TableColumnMapper<AuditEvent> mapper) {
		this.columnMapper = mapper;
		this.fireTableStructureChanged();
	}

	/**
	 * This returns the number of events of the session.
	 * 
	 */
	public int getRowCount() {
		return dataView.getEvents().size();
	}

	/**
	 * Returns the value of cell <code>rowIndex</code>, <code>columnIndex</code>
	 * .
	 * 
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		AuditEvent evt = dataView.getEvent(rowIndex);
		if (evt == null)
			return null;

		return columnMapper.extract(evt, columnIndex);
	}

	public Object getValueForEvent(int idx, String var) {
		AuditEvent evt = this.getEvent(idx);
		if (evt == null)
			return null;

		return evt.get(var);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnMapper.getColumnName(column); // .get( column );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return columnMapper.getColumns().size(); // columns.size();
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#getEvent(int)
	 */
	public AuditEvent getEvent(int i) {
		return dataView.getEvent(i);
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#addEvent(org.jwall.web.audit.AuditEvent)
	 */
	public void addEvent(AuditEvent e) {
		dataView.addEvent(e);
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#addEvents(java.util.Collection)
	 */
	public void addEvents(Collection<AuditEvent> c) {
		dataView.addEvents(c);
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#clear()
	 */
	public void clear() {
		dataView.clear();
		fireTableDataChanged();
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#getEvents()
	 */
	public Collection<AuditEvent> getEvents() {
		return dataView.getEvents();
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#firstEventDate()
	 */
	public Date firstEventDate() {
		return dataView.firstEventDate();
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#lastEventDate()
	 */
	public Date lastEventDate() {
		return dataView.lastEventDate();
	}

	public static TableColumnMapper<AuditEvent> getColumnMapper(String layout) {

		AuditViewer viewer = AuditViewer.getAuditViewer();

		try {
			if (viewer != null) {
				if (layout.length() > 0)
					layout = "-" + layout;

				File in = new File(viewer.getPreferenceDirectory()
						+ File.separator + "table-columns" + layout + ".xml");
				return DefaultColumnMapper.read(in);
			}
		} catch (Exception e) {
		}

		return null;
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#getViewFilter()
	 */
	public AuditEventFilter getViewFilter() {
		return dataView.getViewFilter();
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#setViewFilter(org.jwall.web.audit.filter.AuditEventFilter)
	 */
	public void setViewFilter(AuditEventFilter filter) {
		dataView.setViewFilter(filter);
	}

	public void viewHasChanged() {
		applyFilters();
	}

	/**
	 * @return
	 * @see org.jwall.web.audit.viewer.AuditDataView#getFilters()
	 */
	public List<AuditEventFilter> getFilters() {
		return dataView.getFilters();
	}

	/**
	 * @param f
	 * @see org.jwall.web.audit.viewer.AuditDataView#setFilters(java.util.List)
	 */
	public void setFilters(List<AuditEventFilter> f) {
		dataView.setFilters(f);
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#applyFilters()
	 */
	public void applyFilters() {
		dataView.applyFilters();
	}

	public int size() {
		return dataView.size();
	}

	public Collection<AuditEvent> getFilteredEvents() {
		return new ArrayList<AuditEvent>();
	}

	/**
	 * @see org.jwall.web.audit.viewer.DataViewListener#dataViewChanged()
	 */
	public void dataViewChanged() {
		this.fireTableDataChanged();
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#addDataViewListener(org.jwall.web.audit.viewer.DataViewListener)
	 */
	public void addDataViewListener(DataViewListener l) {
		dataView.addDataViewListener(l);
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#removeDataViewListener(org.jwall.web.audit.viewer.DataViewListener)
	 */
	public void removeDataViewListener(DataViewListener l) {
		dataView.removeDataViewListener(l);
	}
}