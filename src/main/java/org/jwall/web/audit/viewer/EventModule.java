/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.jwall.AuditViewer;
import org.jwall.app.AbstractModule;
import org.jwall.app.Action;
import org.jwall.app.ui.ActionHandler;
import org.jwall.app.ui.TaskProgressDialog;
import org.jwall.app.ui.View;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.FeederThread;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.injector.Injector;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditEventSource;
import org.jwall.web.audit.obfuscator.Obfuscator;
import org.jwall.web.audit.session.SessionTracker;
import org.jwall.web.audit.tagging.ClusterModel;
import org.jwall.web.audit.tagging.ClusterTreeView;
import org.jwall.web.audit.tagging.EventSorter;
import org.jwall.web.audit.ui.AuditEventReaderDialog;
import org.jwall.web.audit.ui.SessionTrackerDialog;
import org.jwall.web.audit.viewer.session.SessionTable;
import org.jwall.web.audit.viewer.session.SessionView;
import org.jwall.web.audit.viewer.table.TableViewHandler;
import org.jwall.web.audit.viewer.ui.AuditView;
import org.jwall.web.audit.viewer.ui.EventProcessor;
import org.jwall.web.audit.viewer.ui.EventTableSplitView;

public class EventModule extends AbstractModule
        implements AuditEventSource, AuditEventListener, EventProcessor, ActionHandler<AuditEvent> {
    /** A unique logger for this class */
    protected final static Logger log = Logger.getLogger(EventModule.class.getCanonicalName());

    /* Some version information for the RemoteAuditModule */
    static {
        System.setProperty("org.jwall.web.audit.viewer.EventModule.version", "$Revision$");
    }

    public final static String ACTION_LOAD_FILE = "org.jwall.web.audit.file.load";
    public final static String ACTION_SAVE_FILE = "org.jwall.web.audit.file.save";
    public final static String ACTION_CLEAR_EVENT_TABLE = "org.jwall.web.audit.events.clear";
    public final static String ACTION_CREATE_SESSIONS = "org.jwall.web.audit.events.create-sessions";
    public final static String ACTION_RELOAD_FILE = "events.file.reload";
    public final static String ACTION_JUMP_LAST = "events.jump.last";
    public final static String ACTION_CREATE_TREE_VIEW = "org.jwall.web.audit.tagging.create-view";

    private AuditView auditView;

    private AuditDataTable eventTable;
    private int evtPointer = 0;

    public boolean sleeping = true;
    private EventTableSplitView view = null;
    private List<EventProcessor> eventProcessors = new LinkedList<EventProcessor>();
    private AuditViewer auditViewer;
    private SessionView sessionView = null;
    private ClusterTreeView clusterTreeView = null;

    /**
     * Creates a new event-module that registers itself to the audit-viewer
     * instance.
     * 
     * @param viewer
     *            The viewer instance to register to.
     */
    public EventModule(AuditViewer viewer) {
        super(viewer, "Event View");
        auditViewer = viewer;
        auditView = new AuditView(viewer);
        AuditViewer.getWindow().setView(auditView);
        add(new Action(this, ACTION_LOAD_FILE, "File/Load events", 2.0d, true));
        add(new Action(this, ACTION_SAVE_FILE, "File/Save events", 1.0d, true));
        add(new Action(this, ACTION_CLEAR_EVENT_TABLE, "Events/Clear Table", 1.0d, false));
        add(new Action(this, ACTION_CREATE_SESSIONS, "Events/Create Session-View", 2.0d, false));
        add(new Action(this, ACTION_CREATE_TREE_VIEW, "Events/Tree View", 3.0d, true));

        getAction(ACTION_CLEAR_EVENT_TABLE).setEnabled(false);
        getAction(ACTION_SAVE_FILE).setEnabled(false);
        getAction(ACTION_CREATE_SESSIONS).setEnabled(false);
        getAction(ACTION_CREATE_TREE_VIEW).setEnabled(false);

        eventTable = new AuditDataTable(new AuditDataMemory());
        view = new EventTableSplitView(eventTable, "AuditView");

        view.addEventProcessor(this);

        addEventProcessor(new Obfuscator());
        addEventProcessor(new Injector());
        addEventProcessor(new EventWriter());
        addEventProcessor(new TableViewHandler(eventTable));

        auditView.addView("Audit Events", view);
    }

    /**
     * 
     * This method implements the ActionListener hook and handles various
     * actions, which have been published to the viewer. These action are
     * inserted in the viewer menu or toolbar and as soon as there are events
     * fired, we need to handle these...
     * 
     */
    public void actionPerformed(ActionEvent e) {

        if (ACTION_LOAD_FILE.equals(e.getActionCommand())) {
            loadEvents();
            try {
                app.saveProperties();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            // getAction("events.file.reload").setEnabled( file != null );
        }

        if (ACTION_SAVE_FILE.equals(e.getActionCommand())) {
            EventWriter writer = new EventWriter();
            writer.processEvent(EventProcessor.ACTION_SAVE, eventTable.getFilteredEvents());
        }

        if (ACTION_CLEAR_EVENT_TABLE.equals(e.getActionCommand())) {
            eventTable.clear();
            getAction(ACTION_CLEAR_EVENT_TABLE).setEnabled(false);
        }

        if (ACTION_JUMP_LAST.equals(e.getActionCommand())) {

            view.scrollToRow(eventTable.getRowCount());
        }

        if (ACTION_CREATE_SESSIONS.equals(e.getActionCommand())) {

            SessionTrackerDialog std = new SessionTrackerDialog();
            std.setVisible(true);

            SessionTracker tracker = std.getSessionTracker();
            if (tracker == null)
                return;

            tracker.setStrictSessions(true);
            if (sessionView == null) {
                SessionTable t = new SessionTable();
                sessionView = new SessionView(auditViewer, t, this);
                auditView.addView("Sessions", sessionView);
                // auditViewer.getAuditView().addView( "Sessions", sessionView
                // );
            }

            sessionView.createSessions(tracker, eventTable.getEvents());

        }

        if (ACTION_CREATE_TREE_VIEW.equals(e.getActionCommand())) {

            EventSorter sorter = new EventSorter();
            sorter.sort(getEventTable().getEvents());

            if (clusterTreeView == null) {
                clusterTreeView = new ClusterTreeView(this.app, new ClusterModel(sorter.getRoot()));
                auditView.addView("Tree View", clusterTreeView);
            } else {
                clusterTreeView.setClusterModel(new ClusterModel(sorter.getRoot()));
            }
        }
    }

    public AuditDataView getEventTable() {
        return eventTable;
    }

    class MyFilter extends javax.swing.filechooser.FileFilter {
        public boolean accept(File f) {
            return f.isDirectory() || (f.isFile() && f.toString().endsWith("log"));
        }

        public String getDescription() {
            return "log-files only";
        }
    }

    public void loadEvents() {
        try {
            AuditEventReaderDialog aer = new AuditEventReaderDialog();
            aer.setVisible(true);

            AuditEventReader reader = aer.getAuditEventReader();
            if (reader == null)
                return;

            Long l = aer.getLimit();
            int max = l.intValue();
            if (l <= 0)
                max = -1;

            view.setVisible(false);
            FeederThread feeder = new FeederThread(reader, this, max);
            TaskProgressDialog tpd = new TaskProgressDialog(getWindow(), FeederThread.STATUS_READ_FILE);
            feeder.addTaskMonitor(tpd);
            feeder.start();

            feeder.join();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        view.setVisible(true);

        getAction(ACTION_SAVE_FILE).setEnabled(eventTable.getRowCount() > 0);
        getAction(ACTION_CLEAR_EVENT_TABLE).setEnabled(eventTable.getRowCount() > 0);
        getAction(ACTION_CREATE_SESSIONS).setEnabled(eventTable.getRowCount() > 0);
        getAction(ACTION_CREATE_TREE_VIEW).setEnabled(eventTable.getRowCount() > 0);
    }

    public boolean hasNext() {
        return evtPointer < eventTable.getRowCount();
    }

    public AuditEvent nextEvent() {
        return eventTable.getEvent(++evtPointer);
    }

    public void setFilter(AuditEventFilter f) {
    }

    public void eventArrived(AuditEvent e) {
        getAction(ACTION_SAVE_FILE).setEnabled(eventTable.getRowCount() > 0);
        eventTable.addEvent(e);
    }

    public void eventsArrived(Collection<AuditEvent> evts) {
        eventTable.addEvents(evts);
    }

    public void addEventListener(AuditEventListener l) {
        // if(l != null)
        // viewer.addEventListener( l );
    }

    public void cleanUp() {

    }

    public View getView() {
        return auditView;
        // return this.auditViewer.getAuditView();
    }

    public void addEventProcessor(EventProcessor evtProcessor) {
        eventProcessors.add(evtProcessor);
    }

    public void removeEventProcessor(EventProcessor evtProcessor) {
        eventProcessors.remove(evtProcessor);
    }

    public void processEvent(String cmd, Collection<AuditEvent> evts) {
        for (EventProcessor p : eventProcessors) {
            if (p.handles(cmd))
                p.processEvent(cmd, evts);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.jwall.app.ui.ActionHandler#dispatchEvent(java.awt.event.ActionEvent,
     * java.lang.Object)
     */
    public void dispatchEvent(ActionEvent e, AuditEvent object) {
        // TODO Auto-generated method stub
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.jwall.app.ui.ActionHandler#getActionCommands()
     */
    public List<String> getActionCommands() {
        LinkedList<String> cmds = new LinkedList<String>();
        for (Action act : this.actions)
            cmds.add(act.getCommand());

        return cmds;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.jwall.app.ui.ActionHandler#handlesEvent(java.awt.event.ActionEvent)
     */
    public boolean handlesEvent(ActionEvent e) {
        return handles(e.getActionCommand());
    }

    public boolean handles(String cmd) {

        for (EventProcessor ep : this.eventProcessors) {
            if (ep.handles(cmd))
                return true;
        }

        return false;
    }

    public AuditView getAuditView() {
        return auditView;
    }
}
