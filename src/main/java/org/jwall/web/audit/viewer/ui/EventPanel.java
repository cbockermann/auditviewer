/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import org.apache.commons.codec.net.URLCodec;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;

public class EventPanel
extends JPanel
{
    public final static long serialVersionUID = 123L;
    AuditEvent evt;

    JTextArea[] sect;
    JTextArea txt;
    JScrollPane scroller;
    JPanel list = new JPanel();
    JPanel req = new JPanel();


    public EventPanel(){
        super(new BorderLayout());

        Color lb = new Color(0xd7,0xe0,0xe8);
        Color db = new Color( 0x00, 0x53, 0x87);
        lb = new Color(0xce, 0xe2, 0xef);

        Color c1 = new Color(0xc0, 0xd5, 0xe8);

        Color[] c = new Color[]{
                lb, 
                c1,
                new Color(0xf2, 0xda, 0xef),
                new Color(0xe0, 0xe0, 0xf1),
                Color.CYAN,
                new Color( 0xef, 0xef, 0xef )
        };

        sect = new JTextArea[c.length];

        for(int i = 0; i < sect.length; i++){
            sect[i] = new TextPanel();
            sect[i].setFont(new Font("Monospaced", Font.PLAIN, 13));
            sect[i].setBackground( c[i] );
            sect[i].setEditable(false);
            sect[i].setBorder(new EmptyBorder(4,4,4,4));
        }
        sect[0].setForeground( db );
        sect[1].setForeground( db );
        sect[1].setColumns(120);
        sect[2].setForeground( new Color(0x68, 0x05,0x5e) );

        list.setLayout(new BoxLayout(list, BoxLayout.Y_AXIS));

        req = new JPanel();
        req.setLayout(new BoxLayout(req, BoxLayout.Y_AXIS));
        req.add(sect[0]);
        req.add(sect[1]);

        JPanel res = new JPanel();
        res.setLayout(new BoxLayout(res, BoxLayout.Y_AXIS));
        res.add(sect[2]);

        list.add(req);
        list.add(res);

        for(int i = 3; i < sect.length; i++){
            JPanel p = new JPanel();
            p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
            list.add(sect[i]);
        }

        scroller = new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroller.setAlignmentX(LEFT_ALIGNMENT);
        scroller.getVerticalScrollBar().setUnitIncrement( 10 );

        add(scroller, BorderLayout.CENTER);
    }

    public void setEvent(AuditEvent e){
        evt = e;

        sect[0].setText( e.getSection( ModSecurity.SECTION_REQUEST_HEADER ) );

        if( e.getSection( ModSecurity.SECTION_FORM_DATA ) != null && !"".equals( e.getSection( ModSecurity.SECTION_FORM_DATA ).trim() ) ) {
            sect[1].setText( e.getSection( ModSecurity.SECTION_FORM_DATA ) );
        } else
            sect[1].setText( e.getSection( ModSecurity.SECTION_REQUEST_BODY ) );

        String encoded = sect[1].getText();
        if( encoded != null && encoded.length() > 0 ){
            URLCodec codec = new URLCodec();
            try {
                
                String[] vars = encoded.split( "&" );
                StringBuffer b = new StringBuffer( "<html>" );
                for( String var : vars ){
                    String decoded = codec.decode( var );
                    b.append( decoded + " <br>" );
                }
                b.append( "</html>" );
                sect[1].setToolTipText( b.toString() );
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        sect[1].setColumns( 80 );
        sect[1].setLineWrap( true );
        sect[2].setText( e.getSection( ModSecurity.SECTION_FINAL_RESPONSE_HEADER ) );
        sect[3].setText( e.getSection( ModSecurity.SECTION_AUDIT_TRAILER ) );
        sect[4].setText( e.getSection( ModSecurity.SECTION_RULE_LOG ) );
    }
}
