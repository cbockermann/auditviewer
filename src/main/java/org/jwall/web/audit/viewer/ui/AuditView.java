/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.HashMap;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jwall.AuditViewer;
import org.jwall.app.ui.View;

public class AuditView extends View
implements ChangeListener
{
    public final static long serialVersionUID = 10L;
    JTabbedPane tabs = new JTabbedPane();
    HashMap<Component,ChangeListener> listener = new HashMap<Component,ChangeListener>();

    public AuditView( AuditViewer auditViewer ){
        super( auditViewer );
        tabs.setBorder(null);
        tabs.addChangeListener( this );
        setLayout( new BorderLayout() );
        add( tabs, BorderLayout.CENTER );
        setVisible(true);
    }

    public void addView( String title, Component v ){
        tabs.add( title, v );
    }

    public void addView( String title, Component v, ChangeListener l ){
        addView( title, v );
        listener.put( v, l );
    }

    public void removeView( Component v ){
        tabs.remove( v );
        if( listener.get( v ) != null )
            listener.remove( v );
    }

    public void show( Component p ){
        for( int i = 0; i < tabs.getComponentCount(); i++){
            if( tabs.getComponent(i) == p ){
                tabs.setSelectedIndex(i);
                return;
            }
        }
    }

    /* (non-Javadoc)
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     */
    public void stateChanged(ChangeEvent e)
    {
        Component p = tabs.getSelectedComponent();
        if( listener.get( p ) != null )
            listener.get(p).stateChanged( e );
    }
}
