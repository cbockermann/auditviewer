/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.List;

import org.jwall.app.Action;
import org.jwall.app.ui.ActionHandler;
import org.jwall.web.audit.AuditEvent;

/**
 * 
 * 
 * @author chris@jwall.org
 *
 */
public class EventProcessAction 
    extends Action 
    implements EventProcessor, ActionHandler<AuditEvent>
{
	/**
     * 
     */
    private static final long serialVersionUID = 6203686410219602992L;
    String command;
	EventProcessor evtProcessor;
	
	public EventProcessAction( ActionListener a, String cmd, String menu, EventProcessor p ){
		super( a, cmd, menu, 10.0d, true, false );
		command = cmd;
		evtProcessor = p;
	}
	
	public void processEvent(String cmd, Collection<AuditEvent> evt) {
		evtProcessor.processEvent( cmd, evt );
	}

    public void dispatchEvent(ActionEvent e, AuditEvent object)
    {
        // TODO Auto-generated method stub
    }

    public List<String> getActionCommands()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean handlesEvent(ActionEvent e)
    {
        return handles( e.getActionCommand() );
    }
    
    public boolean handles( String cmd ){
        if( evtProcessor.handles( cmd ) )
            return true;
        
        return false;
    }
}
