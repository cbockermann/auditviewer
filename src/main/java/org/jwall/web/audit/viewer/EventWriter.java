/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer;

import java.io.File;
import java.util.Collection;

import javax.swing.JFileChooser;

import org.jwall.AuditViewer;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.BufferedAuditEventWriter;
import org.jwall.web.audit.io.ModSecurity2AuditWriter;
import org.jwall.web.audit.viewer.ui.EventProcessor;

public class EventWriter
implements EventProcessor
{

    public boolean handles(String actionCommand)
    {
        return EventProcessor.ACTION_SAVE.equals( actionCommand ) || EventModule.ACTION_SAVE_FILE.equals( actionCommand );
    }

    public void processEvent(String cmd, Collection<AuditEvent> events)
    {
        try {

            JFileChooser fc = new JFileChooser();
            String fname = "";

            if(events.size() == 1){
                fname = events.iterator().next().getEventId()+"-audit.log";
                fc.setSelectedFile(new File(fname));
            }

            int r = fc.showSaveDialog( AuditViewer.getWindow() );
            if(r == JFileChooser.APPROVE_OPTION){

                File f = fc.getSelectedFile();

                BufferedAuditEventWriter wr = new BufferedAuditEventWriter( new ModSecurity2AuditWriter( f ) );
                wr.addAll( events );
                wr.start();

                // by immediately closing the writer, we are just signaling it to 
                // quit after all events have been written.
                wr.close(); 
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
