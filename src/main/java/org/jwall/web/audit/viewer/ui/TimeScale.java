package org.jwall.web.audit.viewer.ui;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filters.AuditEventDateFilter;
import org.jwall.web.audit.viewer.AuditDataView;

public class TimeScale
{
    AuditEventDateFilter dateRange;
    
    AuditDataView table;
    
    long stepSize = 1L;
    
    TreeSet<Long> steps = new TreeSet<Long>();
    
    Map<Long,Double> timeScale = new HashMap<Long,Double>();
    
    Double min = Double.MAX_VALUE;
    Double max = Double.MIN_VALUE;
    
    public TimeScale( AuditDataView table, AuditEventDateFilter dateRange, long stepSize ){

        this.table = table;
        this.dateRange = dateRange;
        if( dateRange == null ){
            this.dateRange = new AuditEventDateFilter( this.table.firstEventDate(), this.table.lastEventDate() );
        }
        this.stepSize = stepSize;
        
        calculateTimeScale();
    }
    
    
    public Long getStart(){
        if( dateRange != null && dateRange.getFromDate() != null )
            return dateRange.getFromDate().getTime();
        
        return table.firstEventDate().getTime();
    }
    
    
    public Long getEnd(){
        if( dateRange != null && dateRange.getToDate() != null )
            return dateRange.getToDate().getTime();
        
        return table.lastEventDate().getTime();
    }

    
    private void calculateTimeScale(){
        //System.out.println( "Creating timescale... " );
        Map<Long,Double> ts = new HashMap<Long,Double>( table.getEvents().size() );

        Long start = table.firstEventDate().getTime();
        //System.out.println( "start is " + start );
        Double cur = 0.0d;
        
        for( AuditEvent evt : table.getEvents() ){
            Long evtDate = evt.getDate().getTime();
            //System.out.println( "Processing event @" + evtDate + " cur=" + cur + ", start=" + start );
            cur += 1.0d;
            if( evtDate > start + stepSize ){
                //System.out.println( "Storing value @ " + new Date( start ) + " = " + cur );
                ts.put( start, cur );
                if( cur < min )
                    min = cur;
                
                if( cur > max )
                    max = cur;
                steps.add( start );
                start = evt.getDate().getTime();
                cur = 1.0d;
            }
        }
        
        synchronized( timeScale ){
            timeScale = ts;
        }
    }
    
    
    public SortedSet<Long> getTimeSteps(){
        return this.steps;
    }
    
    public Double getValueAt( Long step ){
        Double d = timeScale.get( step );
        if( d == null )
            d = 0.0d;
        
        return d;
    }
}