/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer;

import java.net.InetAddress;
import java.util.Hashtable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 
 * This is a background-thread that resolves ip-addresses into names and
 * make these pairs available for the resolver. It has a list of addresses
 * (todo-list) which need to be resolved.
 * 
 * After resolving succeeded, it inserts the (address,name)-pair into the
 * caching hashtable.
 * 
 */
public class ResolverThread extends Thread {
    BlockingQueue<String> todo;
    Hashtable<String,String> cache;

    /**
     * Creates a new ResolverThread with an empty todo-list.
     */
    public ResolverThread(Hashtable<String,String> names){
        cache = names;
        todo = new LinkedBlockingQueue<String>();
    }


    /**
     * Adds a new address to the list of addresses that are resolved by
     * the thread.
     * 
     * @param address The address to be added to the todo-list.
     */
    public void add(String address){
        todo.add( address );
    }


    /**
     * The working-loop.
     */
    public void run(){
        while(true){
            try {
                String addr = todo.take();
                String name = resolve( addr );
                cache.put( addr , name );
            } catch (Exception e) {
            }
        }
    }


    public String resolve(String address){
        if( cache.get( address ) != null)
            return cache.get( address );

        try {
            InetAddress addr = InetAddress.getByName(address);

            String name = addr.getCanonicalHostName();

            cache.put( address , name );
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return address;
    }
}
