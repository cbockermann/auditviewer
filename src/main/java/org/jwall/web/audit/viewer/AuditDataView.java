package org.jwall.web.audit.viewer;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;


/**
 * This interface represents a view on audit data, i.e. it is a collection
 * of audit events, which may be provided by several implementations, such
 * as a memory-based event table, a database-index or the like. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface AuditDataView
{
    
    
    /**
     * Returns a specific property of the event at the given index. 
     * 
     * @param idx
     * @param var
     * @return
     */
    public Object getValueForEvent( int idx, String var );

    
    /**
     * Returns the event at position <code>i</code> of the session which is
     * associated with this eventTable.
     * 
     * @param i
     * @return
     */
    public abstract AuditEvent getEvent(int i);

    
    /**
     * This method adds a single event to the table.
     * 
     * @param e
     */
    public abstract void addEvent(AuditEvent e);

    
    /**
     * 
     * This method adds new events to the table. It may be called by a feeder thread
     * which inserts new events, i.e. when in remote-tail mode. 
     * 
     */
    public abstract void addEvents(Collection<AuditEvent> c);

    
    public void setFilters( List<AuditEventFilter> f );
    
    public List<AuditEventFilter> getFilters();
    
    
    /**
     * 
     * This method clears the table, the view collection and sends a notification to
     * all listeners of this view.
     * 
     */
    public abstract void clear();

    /**
     * 
     * This method returns the events currently in the table. This is not the
     * set of events filtered, but ALL events in the table.
     * 
     */
    public abstract Collection<AuditEvent> getEvents();

    
    public abstract Collection<AuditEvent> getFilteredEvents();
    
    
    /**
     * Returns the earliest date of all events contained in this view.
     * 
     * @return
     */
    public abstract Date firstEventDate();

    
    /**
     * Returns the latest date over all events of this view.
     * 
     * @return
     */
    public abstract Date lastEventDate();

    
    /**
     * Returns the filter which this view is based on. May be <code>null</code>
     * if this view does not do any filtering at all.
     * 
     * @return
     */
    public abstract AuditEventFilter getViewFilter();

    
    /**
     * 
     * @param filter
     */
    public abstract void setViewFilter(AuditEventFilter filter);

    
    /**
     * Applies newly set filters to the view and revalidates the view.
     */
    public abstract void applyFilters();
    
    
    public void addDataViewListener( DataViewListener l );
    
    public void removeDataViewListener( DataViewListener l );
    
    
    public int size();
}