/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.swing.table.AbstractTableModel;

import org.jwall.AuditViewer;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.viewer.table.DefaultColumnMapper;
import org.jwall.web.audit.viewer.table.TableColumnMapper;

/**
 * 
 * This class implements a table model for displaying a table of audit events.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * @deprecated
 */
public class EventTable extends AbstractTableModel // implements AuditDataView
{
	private static final long serialVersionUID = 2821887967019070952L;

	/** The columns which are to be displayed in the table */
	ArrayList<String> columns = new ArrayList<String>();

	/**
	 * This is the standard implementation for extracting the different
	 * column-values of a row
	 */
	TableColumnMapper<AuditEvent> columnMapper = new DefaultColumnMapper();

	/** A simple format for displaying an event's date */
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yy hh:mm:ss");

	/** This is the list of events to be backed by the table */
	ArrayList<AuditEvent> events;

	/**
	 * The events to be shown, which might differ from the events-vector in case
	 * of a filter-expression
	 */
	public ArrayList<AuditEvent> view;

	/** A filter for selecting only a subset of the events to display */
	public AuditEventFilter filter = null;

	public AuditEventFilter viewFilter = null;

	Date firstEvent = null;
	Date lastEvent = null;

	/**
	 * 
	 * Creates a new table model with the default columns displayed.
	 * 
	 */
	public EventTable() {
		this(10000);
	}

	private EventTable(int size) {
		events = new ArrayList<AuditEvent>();
		view = new ArrayList<AuditEvent>();
		columnMapper = new DefaultColumnMapper();
	}

	public EventTable(int size, TableColumnMapper<AuditEvent> customMapper) {
		this(10000);
		columnMapper = customMapper;
	}

	public TableColumnMapper<AuditEvent> getColumnMapper() {
		return columnMapper;
	}

	public void setColumnMapper(TableColumnMapper<AuditEvent> mapper) {
		this.columnMapper = mapper;
		this.fireTableStructureChanged();
	}

	/**
	 * Fills this eventTable with the events of the given session.
	 * 
	 * @param s
	 */
	public void setEvents(Collection<AuditEvent> s) {
		events.clear();
		view.clear();

		for (AuditEvent evt : s)
			addEvent(evt);

		this.fireTableDataChanged();
	}

	/**
	 * This returns the number of events of the session.
	 * 
	 */
	public int getRowCount() {
		if (events == null)
			return 0;
		if (filter == null && viewFilter == null)
			return events.size();
		else
			return view.size();
	}

	/**
	 * Returns the value of cell <code>rowIndex</code>, <code>columnIndex</code>
	 * .
	 * 
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		AuditEvent evt = null;

		if (filter == null)
			evt = events.get(rowIndex);
		else
			evt = view.get(rowIndex);

		return columnMapper.extract(evt, columnIndex);
	}

	public Object getValueForEvent(int idx, String var) {
		AuditEvent evt = this.getEvent(idx);
		if (evt == null)
			return null;

		return evt.get(var);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columnMapper.getColumnName(column); // .get( column );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return columnMapper.getColumns().size(); // columns.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#getEvent(int)
	 */
	public AuditEvent getEvent(int i) {
		if (filter == null)
			return events.get(i);

		return view.get(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#addEvent(org.jwall.web.audit
	 * .AuditEvent)
	 */
	public void addEvent(AuditEvent e) {

		if (this.firstEvent == null
				|| e.getDate().getTime() < firstEvent.getTime()) {
			firstEvent = e.getDate();
		}

		if (this.lastEvent == null || lastEvent.before(e.getDate())) {
			// System.out.println( "Found new last event" );
			lastEvent = e.getDate();
		}

		events.add(e);

		// if there is a filter applied, we only add the event to the
		// view if it matches the filter
		//
		if (viewIncludes(e) && filterMatches(e))
			view.add(e);

		// notify all listeners
		//
		this.fireTableDataChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#addEvents(java.util.Collection)
	 */
	public void addEvents(Collection<AuditEvent> c) {

		for (AuditEvent evt : c)
			this.addEvent(evt);

		// notify all listeners about new rows being available
		//
		this.fireTableDataChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#setFilter(org.jwall.web.audit
	 * .filter.AuditEventFilter)
	 */
	public void setFilter(AuditEventFilter f) {
		filter = f;
		System.out.println("Setting filter to:\n" + f);
		applyFilters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#clear()
	 */
	public void clear() {
		events.clear();
		view = events;
		fireTableDataChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#getEvents()
	 */
	public Collection<AuditEvent> getEvents() {
		return getFilteredEvents();
	}

	/**
	 * 
	 * @return
	 */
	public Collection<AuditEvent> getFilteredEvents() {
		if (filter != null || viewFilter != null)
			return view;
		else
			return events;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#firstEventDate()
	 */
	public Date firstEventDate() {
		return this.firstEvent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#lastEventDate()
	 */
	public Date lastEventDate() {
		return this.lastEvent;
	}

	public static TableColumnMapper<?> getColumnMapper(String layout) {

		AuditViewer viewer = AuditViewer.getAuditViewer();

		try {
			if (viewer != null) {
				if (layout.length() > 0)
					layout = "-" + layout;

				File in = new File(viewer.getPreferenceDirectory()
						+ File.separator + "table-columns" + layout + ".xml");
				return DefaultColumnMapper.read(in);
			}
		} catch (Exception e) {
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#getViewFilter()
	 */
	public AuditEventFilter getViewFilter() {
		return viewFilter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#setViewFilter(org.jwall.web.
	 * audit.filter.AuditEventFilter)
	 */
	public void setViewFilter(AuditEventFilter filter) {
		viewFilter = filter;
	}

	public void viewHasChanged() {
		System.out.println("Adjusting event-table-view, view-filter is : \n"
				+ viewFilter.toXML());
		applyFilters();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#applyFilters()
	 */
	public void applyFilters() {
		view = new ArrayList<AuditEvent>(events.size());
		Iterator<AuditEvent> it = events.iterator();
		while (it.hasNext()) {
			AuditEvent e = it.next();
			if (viewIncludes(e) && filterMatches(e))
				view.add(e);
		}
		fireTableDataChanged();
	}

	private boolean viewIncludes(AuditEvent e) {

		if (viewFilter != null && viewFilter.matches(e))
			return false;

		return true;
	}

	private boolean filterMatches(AuditEvent e) {
		boolean b = filter == null || filter.matches(e);
		return b;
	}
}