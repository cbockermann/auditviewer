/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.Component;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jwall.app.Action;
import org.jwall.app.Application;


/**
 * 
 * 
 * @author chris
 */
public class PopupMenu extends JPopupMenu {

	private static final long serialVersionUID = 8760949304100882124L;
	public List<JMenu> menues = new LinkedList<JMenu>();
	
	
	private JMenu getMenu( JMenu m, String name ){
		for( int i = 0; i < m.getMenuComponentCount(); i++ ){
			if( m.getMenuComponent(i).getName().equals(name) )
				return m;
		}
		
		JMenu nm = new JMenu( name );
		m.add( nm );
		return nm;
	}
	
	private JMenu getMenu( String name ){
		for( JMenu m : menues )
			if( m.getName().equals( name ) )
				return m;
		
		JMenu m = new JMenu( name );
		add( m );
		return m;
	}
	
	public Component add( JMenu m ){
		menues.add( m );
		return super.add(m);
	}
	
	public void addAction( Action a ){
		if( a.getMenuLabel().indexOf("/") >= 0 ){
			String[] path = a.getMenuLabel().split("/");
			add( path, a );
		} else {
			JMenuItem mi = new JMenuItem( a.getMenuLabel() );
			mi.setActionCommand( a.getCommand() );
			mi.setIcon( Application.getIcon( a ) );
			mi.addActionListener( a );
			add( mi );
		}
	}
	
	private void add( String[] path, Action a ){
		JMenu m = getMenu( path[0] );
		if( m == null ){
			m = new JMenu( path[0] );
			add( m );
		}
		
		for( int i = 1; i < path.length -1; i++ ){
			JMenu menu = this.getMenu( m, path[i] );
			if( menu == null ){
				menu = new JMenu( path[i] );
				m.add( menu );
			}
			m = menu;
		}
		
		JMenuItem mi = new JMenuItem( a.getMenuLabel() );
		mi.setActionCommand( a.getCommand() );
		mi.setIcon( Application.getIcon( a ) );
		mi.addActionListener( a );
		m.add( mi );
	}
}
