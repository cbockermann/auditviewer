/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jwall.app.Application;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.filter.AuditEventRegexpFilter;
import org.jwall.web.audit.filters.AuditEventSectionRegexpFilter;
import org.jwall.web.audit.filters.AuditEventSectionSubstringFilter;
import org.jwall.web.audit.viewer.AuditDataView;

public class FilterPanel extends JPanel implements ItemListener, ActionListener, TableModelListener {
    public final static long serialVersionUID = 0L;
    AuditEventFilter filter = null;
    AuditDataView table;

    JTextField exp = new JTextField(20);
    JComboBox<String> sel = new JComboBox<String>();
    JLabel count = new JLabel();
    JButton ok, cancel;
    String filterIcon = "org.jwall.web.audit.event.filter";
    String unfilterIcon = "org.jwall.web.audit.event.unfilter";

    String filterByMessage = "Log message with substring";
    String filterByMessageRx = "Log message with regexp";

    String[] vars = { ModSecurity.REQUEST_URI, ModSecurity.REQUEST_METHOD, ModSecurity.QUERY_STRING,
            ModSecurity.REMOTE_ADDR, ModSecurity.REQUEST_FILENAME, ModSecurity.RESPONSE_STATUS, ModSecurity.SERVER_NAME,
            ModSecurity.REQUEST_COOKIES, filterByMessage, filterByMessageRx };

    public FilterPanel(AuditDataView t) {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        table = t;

        add(new JLabel("Quick-Filter on"));

        sel = new JComboBox<String>(vars);
        sel.setPreferredSize(new Dimension((int) sel.getPreferredSize().getWidth(), 19));
        add(sel);

        exp.addActionListener(this);
        exp.setPreferredSize(new Dimension((int) sel.getPreferredSize().getWidth(), 20));
        add(exp);

        ok = new JButton("Filter");
        ok.setIcon(Application.getIcon(filterIcon));
        ok.setPreferredSize(new Dimension(90, 20));
        ok.addActionListener(this);
        ok.setActionCommand("filter");
        add(ok);

        count.setText("Showing " + table.getEvents().size() + "/" + table.size() + " events.");
        add(count);
    }

    public void updateDisplay() {
        if (filter != null)
            count.setText("Showing " + table.getEvents().size() + "/" + table.size() + " events.");
        else
            count.setText("Showing all " + table.getEvents().size() + " events.");
    }

    public AuditEventFilter getFilter() {
        return filter;
    }

    public void itemStateChanged(ItemEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if ("filter".equals(e.getActionCommand())) {
            try {
                System.out.println("Creating new filter expression... ");
                LinkedList<AuditEventFilter> filter = new LinkedList<AuditEventFilter>();

                if (filterByMessage.equals(vars[sel.getSelectedIndex()])
                        || filterByMessageRx.equals(vars[sel.getSelectedIndex()])) {
                    if (filterByMessage.equals(vars[sel.getSelectedIndex()]))
                        filter.add(new AuditEventSectionSubstringFilter(ModSecurity.SECTION_AUDIT_TRAILER,
                                exp.getText(), true));
                    else
                        filter.add(new AuditEventSectionRegexpFilter(ModSecurity.SECTION_AUDIT_TRAILER, exp.getText()));
                } else
                    filter.add(new AuditEventRegexpFilter(vars[sel.getSelectedIndex()], exp.getText()));

                table.setFilters(filter);
                table.applyFilters();
                count.setText("Showing " + table.getEvents().size() + "/" + table.size() + " events.");
                ok.setIcon(Application.getIcon(unfilterIcon));
                ok.setActionCommand("unfilter");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(exp, ex.getMessage());
                filter = null;
            }
        } else {
            System.out.println("Removing filter expression... ");
            filter = null;
            table.setFilters(new LinkedList<AuditEventFilter>());
            table.applyFilters();
            count.setText("Showing all " + table.size() + " events.");
            ok.setIcon(Application.getIcon(filterIcon));
            ok.setActionCommand("filter");
        }
    }

    public void tableChanged(TableModelEvent e) {
        updateDisplay();
    }
}