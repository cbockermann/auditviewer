/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.table;

import java.util.ArrayList;


/**
 * <p>
 * This interface defines a simple column mapper which extracts entries for a specific
 * row of a table. The mapper also specifies a number of columns and provides their
 * names.
 * </p>
 * <p>
 * This interface is intended to allow an easy generalization of additional columns
 * being added to a table, which may be extracted in a customized fashion.
 * </p>
 * 
 * 
 * @author chris@jwall.org
 *
 */
public interface TableColumnMapper<E>
{

    /**
     * Return the list of column-names. These are the names which will be requested
     * by a table-renderer to render the column header fields.
     * 
     * @return A list of table column names.
     */
    public ArrayList<TableColumn> getColumns();

    
    public void setName( String name );
    
    public String getName();
    

    /**
     * 
     * 
     * @param col
     * @return
     */
    public String getColumnName( int col );
    
    /**
     * This method extracts the value of the specified column from a given row object. 
     * 
     * @param row
     * @param column
     * @return
     */
    public Object extract( E row, int column );
}
