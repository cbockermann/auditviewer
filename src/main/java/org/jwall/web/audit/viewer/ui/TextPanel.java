/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTextArea;


/**
 * 
 * 
 * @author chris
 */
public class TextPanel 
extends JTextArea 
implements MouseListener
{
    public final static long serialVersionUID = 123L;
    private int heightClosed = 0;
    private int heightOpen = 0;
    private boolean closed = false;
    private boolean interactive = false;

    public TextPanel(){
        super();
        init();
    }

    public TextPanel(String s){
        super(s);
        init();
        addMouseListener(this);
        setEditable(false);
        setLineWrap(true);
        setWrapStyleWord(false);
        setFont( new Font("Monospaced", Font.PLAIN, 14) );
    }

    public void setInteractive(boolean b){
        interactive = b;
    }
    
    public Dimension getPreferredSize(){
        if(closed)
            return new Dimension(super.getPreferredSize().width,  heightClosed);

        return super.getPreferredSize();
    }

    public Dimension getMaximumSize(){
        if(closed)
            return new Dimension(super.getPreferredSize().width ,heightClosed);

        return new Dimension(super.getMaximumSize().width, super.getPreferredSize().height);
    }

    private void init(){
        //this.setBackground( Color.GREEN );
        //this.setBackground( new Color(0xBF, 0xFF, 0xF5) );
        //setBorder( BorderFactory.createLineBorder( Color.BLUE ) );

        Dimension d = getPreferredSize();
        heightOpen = (int) d.getHeight();
        heightClosed = getRowHeight();
        setSize( new Dimension( (int) d.getWidth(), heightClosed ) );
    }

    public boolean isVisible(){
        return !getText().equals("");
        //return super.isVisible();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        if(interactive)
            setBackground( Color.GREEN );
    }

    public void mouseExited(MouseEvent e) {
        //setBackground( Color.WHITE );
    }

    public void mouseClicked(MouseEvent e) {
        /*
         */
        if(interactive){
            if(closed){
                grow();
            } else
                shrink();
        }
    }

    public void grow(){
        Thread t = new Thread(new Runnable(){
            public void run(){
                Dimension d = getSize();
                int height = (int) heightClosed;
                while( height < heightOpen ){
                    setSize( new Dimension( (int) d.getWidth(), height ) );
                    height += 1;
                    getParent().validate();
                }
                closed = false;
            }
        });

        t.start();
    }

    public void shrink(){
        Thread t = new Thread(new Runnable(){
            public void run(){
                Dimension d = getSize();
                int height = (int) d.getHeight();
                while( height > heightClosed ){
                    setSize( new Dimension( (int) d.getWidth(), height ) );
                    height -= 1;
                    getParent().validate();
                }
                closed = true;
            }
        });

        t.start();
    }

}
