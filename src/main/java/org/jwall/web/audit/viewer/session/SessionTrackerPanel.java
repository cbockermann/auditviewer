/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.session;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jwall.web.audit.session.SessionTracker;

public class SessionTrackerPanel
    extends JPanel
    implements ItemListener
{
    public final static long serialVersionUID = 1232L;
    SessionTracker tracker = null;
    JComboBox sel;
    JTextField timeOut = new JTextField(10);
    String[] availTrackers = {
            "Heuristic IP-based sessions"
            //"Cookie-based sessions"
    };
    ActionListener smodule;
    
    public SessionTrackerPanel(ActionListener act){
        super(new FlowLayout(FlowLayout.LEFT));
        smodule = act; 
        
        add(new JLabel("Session Tracker: "));
        
        sel = new JComboBox(availTrackers);
        sel.addItemListener( this );
        add(sel);
        
        add(new JLabel("Session Timeout:"));
        add(timeOut);
        add(new JLabel("seconds"));
        
        JButton cr = new JButton("Create Sessions");
        cr.setActionCommand("create.sessions");
        cr.addActionListener(act);
        
        add(cr);
    }
    
    public SessionTracker getSessionTracker(){
        return tracker;
    }
    
    public void itemStateChanged(ItemEvent e) {
        if(e.getSource() == sel){
            int idx = sel.getSelectedIndex(); 
            System.out.println("selected: "+idx);
        }
    }
}
