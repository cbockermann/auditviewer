/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.session;

import java.awt.event.MouseEvent;

import org.jwall.web.audit.viewer.AuditDataTable;
import org.jwall.web.audit.viewer.ui.EventTableSplitView;

public class SessionEventTableView
    extends EventTableSplitView
{

    
    /**
     * 
     */
    private static final long serialVersionUID = -8830311704282470766L;

    public SessionEventTableView( AuditDataTable table ){
        super( table, "SessionView" );
    }
    
    
    public void mouseClicked(MouseEvent e)
    {
        /*
        if(e.getSource() == this.etable && e.getClickCount() > 1 && e.getButton() == MouseEvent.BUTTON1){
            int row = etable.getSelectedRow();
            
            if( row >= 0 ){
                AuditEvent evt = eventTable.getEvent( row );
                
                EventDisplayDialog d = new EventDisplayDialog();
                LinkedList<AuditEvent> evts = new LinkedList<AuditEvent>();
                evts.addAll( eventTable.events );
                
                d.setEvents( evts );
                d.setEvent( evt );

                d.setVisible( true );
                return;
            }
        }
        */
        //super.mouseClicked( e );
    }
}
