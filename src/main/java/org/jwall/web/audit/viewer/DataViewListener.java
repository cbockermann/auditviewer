package org.jwall.web.audit.viewer;

public interface DataViewListener
{
    public void dataViewChanged();
}
