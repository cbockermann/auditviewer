/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.jwall.app.ui.Dialog;
import org.jwall.web.audit.AuditEvent;

public class EventDisplayDialog
extends Dialog
{
    private static final long serialVersionUID = -51370768724266199L;
    EventPanel p = new EventPanel();
    JButton prev, next;
    EventTableView eventTableView = null;
    int idx = 0;

    public EventDisplayDialog(){
        getContentPane().setLayout( new BorderLayout() );


        JPanel buttons = new JPanel( new FlowLayout( FlowLayout.LEFT) );

        prev = new JButton("Previous");
        prev.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                prev();
            }
        });

        next = new JButton("Next");
        next.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                next();
            }
        });

        buttons.add( prev );
        buttons.add( next );

        getContentPane().add( buttons, BorderLayout.NORTH );

        //p.setEvent( evt );
        getContentPane().add( p, BorderLayout.CENTER );

        setSize( 800, 600 );
        center();
    }


    public void setEventTableView( EventTableView view ){
        this.eventTableView = view;
    }


    public void next(){
        idx++;
        setEvent( idx );
    }

    public void prev(){
        idx--;
        setEvent( idx );
    }

    public void setEvent( int index ){
        idx = index;
        AuditEvent evt = null;
        
        if( eventTableView != null ){
            evt = eventTableView.eventTable.getEvent( idx );
            eventTableView.etable.getSelectionModel().setSelectionInterval( idx, idx );
        }
        
        
        p.setEvent( evt );
        checkButtons();
    }

    private void checkButtons(){
        next.setEnabled( idx + 1 < eventTableView.eventTable.getRowCount() );
        prev.setEnabled( idx - 1 >= 0 );
    }


    /* (non-Javadoc)
     * @see java.awt.event.WindowListener#windowClosed(java.awt.event.WindowEvent)
     */
    public void windowClosed(WindowEvent e)
    {
        next.setEnabled( false );
        prev.setEnabled( false );
    }
}
