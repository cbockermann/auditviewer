package org.jwall.web.audit.viewer.table;

import java.util.Collection;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.filter.AuditEventIdList;
import org.jwall.web.audit.filter.AuditEventMatch;
import org.jwall.web.audit.viewer.AuditDataView;
import org.jwall.web.audit.viewer.ui.EventProcessor;


/**
 * <p>
 * This class implements an event-processor which handles all actions resulting in view-changes. It
 * is associated with a single event table and manages all view-related stuff, i.e. requests for
 * removing events from the view, etc. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class TableViewHandler
    implements EventProcessor
{

    public final static String ACTION_SHOW_FILTERS = "org.jwall.web.audit.events.view.show-filters";
    
    public final static String ACTION_REMOVE_BY_RULE = "org.jwall.web.audit.events.view.remove-by-rule";
    
    public final static String ACTION_REMOVE_BY_ID = "org.jwall.web.audit.events.view.remove-by-id";


    
    /* The table which this processor acts upon */
    AuditDataView table;
    

    /**
     * 
     * @param table
     */
    public TableViewHandler( AuditDataView table ){
        this.table = table;
    }
    
    
    /**
     * @see org.jwall.web.audit.viewer.ui.EventProcessor#handles(java.lang.String)
     */
    public boolean handles(String actionCommand)
    {
        return ACTION_REMOVE_BY_ID.equals( actionCommand );
    }

    
    /**
     * @see org.jwall.web.audit.viewer.ui.EventProcessor#processEvent(java.lang.String, java.util.Collection)
     */
    public void processEvent(String cmd, Collection<AuditEvent> evt)
    {
        if( ACTION_REMOVE_BY_ID.equals( cmd ) ){
            System.out.println( "RemoveById has been called..." );
            AuditEventFilter filter = table.getViewFilter();
            
            if( filter == null ){
                filter = new AuditEventFilter();
                table.setViewFilter( filter );
            }
            
            AuditEventIdList idList = null;
            
            for( AuditEventMatch match : filter.getMatches() ){

                if( match instanceof AuditEventIdList ){
                    System.out.println("Found existing id-list-filter" );
                    idList = (AuditEventIdList) match;
                    break;
                }
            }

            if( idList == null ){
                System.out.println( "Creating new id-list-filter" );
                idList = new AuditEventIdList();
                filter.add( idList );
                idList.setExcludeList( true );
            }
            
            for( AuditEvent e : evt ){
                System.out.println( "Adding event-id " + e.getEventId() );
                idList.add( e.getEventId() );
            }

            table.applyFilters();
        }
    }
}