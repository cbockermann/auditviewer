package org.jwall.web.audit.viewer.session;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.session.Session;

public class EventSession  {
	
	final Session session;
	final List<AuditEvent> events = new ArrayList<AuditEvent>();
	
	
	public EventSession( Session s ){
		session = s;
	}
	

	public void addEvent(AuditEvent e) {
		session.addEvent(e);
		events.add( e);
	}
	
	public List<AuditEvent> getEvents(){
		return events;
	}


	public int getType() {
		return session.getType();
	}


	public void setInetAddress(InetAddress addr) {
		session.setInetAddress(addr);
	}


	public InetAddress getInetAddress() {
		return session.getInetAddress();
	}


	public Date lastAccessed() {
		return session.lastAccessed();
	}


	public Date firstAccessed() {
		return session.firstAccessed();
	}


	public String getId() {
		return session.getId();
	}


	public double getScore() {
		return session.getScore();
	}


	public void setScore(double d) {
		session.setScore(d);
	}


	public String getVariable(String name) {
		return session.getVariable(name);
	}


	public Set<String> getVariableNames() {
		return session.getVariableNames();
	}


	public void increment(String var, int count) {
		session.increment(var, count);
	}


	public void setVariable(String name, String value, Date expires) {
		session.setVariable(name, value, expires);
	}


	public void removeVariable(String name) {
		session.removeVariable(name);
	}
	
	
}
