/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.session;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.table.DefaultTableModel;

import org.jwall.web.audit.viewer.Resolver;

public class SessionTable
extends DefaultTableModel
{
    public final static long serialVersionUID = 1L;
    static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
    SortedSet<EventSession> sessions;
    SortedSet<EventSession> activeSessions;
    Comparator<EventSession> comparator;
    String[] columns = new String[]{
            "Date (Start)",                            // 0
            "Session ID",           // 1
            "IP adress",
            "Length (Events)"
    };
    long lastUpdate = System.currentTimeMillis();
    boolean onlyActive = false;
    
    
    /**
     * 
     *
     */
    public SessionTable(){
        comparator = new Comparator<EventSession>(){
            public int compare(EventSession s1, EventSession s2){
                return s1.firstAccessed().compareTo(s2.firstAccessed());
            }
        };
        
        sessions = new TreeSet<EventSession>(comparator);
        activeSessions = new TreeSet<EventSession>(comparator);
        
        for(EventSession s : sessions){
            if(System.currentTimeMillis() - s.lastAccessed().getTime() < 2*3600*1000 )
                activeSessions.add( s );
        }
    }


    /**
     * Fills this eventTable with the events of the given session.
     * 
     * @param s
     */
    public void setSessions(Collection<EventSession> s){
        sessions.clear();
        sessions.addAll( s );
        activeSessions.clear();
        for(EventSession sess : sessions){
            if(System.currentTimeMillis() - sess.lastAccessed().getTime() < 2*3600*1000 )
                activeSessions.add( sess );
        }
        lastUpdate = System.currentTimeMillis();

        this.fireTableDataChanged();
    }


    /**
     * Returns the session which is displayed by this eventTable.
     * 
     * @return
     */
    public EventSession getSession(int i){
        int k = 0;
        EventSession s = null;
        
        SortedSet<EventSession> set = sessions;
        if(onlyActive)
            set = activeSessions;
        
        Iterator<EventSession> it = set.iterator();
        while(it.hasNext() && k <= i){
            s = it.next();
            if(k == i)
                return s;
            k++;
        }
            
        return s;
    }

    
    public void sessionChanged(EventSession s){
        int i = 0;
        
        if(! sessions.contains(s) ){
            sessions.add( s );
            
            if(System.currentTimeMillis() - s.lastAccessed().getTime() <= 2 * 3600*1000)
                activeSessions.add( s );
        }
        
        Iterator<EventSession> it = sessions.iterator();
        while(it.hasNext()){
            EventSession se = it.next();
            if(se.equals(s)){
                this.fireTableRowsUpdated(i, i+1);
                return;
            }
            i++;
        }
    }

    /**
     * This returns the number of events of the session.
     * 
     */
    public int getRowCount()
    {
        if(sessions == null)
            return 0;

        if(onlyActive)
            return activeSessions.size();
        
        return sessions.size();
    }


    /**
     * Returns the value of cell <code>rowIndex</code>, <code>columnIndex</code>. 
     * 
     */
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        EventSession s = getSession( rowIndex );

        //System.out.println("row: "+rowIndex+", evt: "+evt);

        if(s != null){
            if(columnIndex == 0)
                return fmt.format( s.firstAccessed() );

            if(columnIndex == 1){
                return ""+s.getId();
            }


            if(columnIndex == 2)
                return Resolver.resolve( s.getInetAddress().getHostAddress() );

            return ""+s.getEvents().size();
        }

        return null;
    }


    /* (non-Javadoc)
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(int column)
    {
        return columns[column];
    }


    /*
     * (non-Javadoc)
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    public int getColumnCount()
    {
        return columns.length;
    }

    
    public void sortBy( int col ){
    	
        if(col == 0){
            Comparator<EventSession> c = new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    int c = s1.firstAccessed().compareTo(s2.firstAccessed());
                    if(c == 0)
                        return s1.getId().compareTo( s2.getId() );
                    return c;
                }
            };

            setComparator(c);
            return;
        }

        if(col == 1){
            Comparator<EventSession> c = new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    return s1.getId().compareTo(s2.getId());
                }
            };
            setComparator(c);
            return;
        }

        
        if(col == 2){
            setComparator(new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    BigInteger i1 = new BigInteger(s1.getInetAddress().getAddress());
                    BigInteger i2 = new BigInteger(s2.getInetAddress().getAddress());
                    int c = i1.compareTo( i2 );
                    if(c == 0)
                        return s1.getId().compareTo( s2.getId() );
                    return c;
                }
            });
            return;
        }
        
        if(col == 3){
            setComparator(new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    int c = (new Integer(s1.getEvents().size())).compareTo( s2.getEvents().size() );
                    if(c == 0)
                        return s1.getId().compareTo( s2.getId() );
                    return c;
                }
            });
            return;
        }
    }

    public void reverseBy( int col ){
    	
        if(col == 0){
            Comparator<EventSession> c = new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    int c = s1.firstAccessed().compareTo(s2.firstAccessed());
                    if(c == 0)
                        return - s1.getId().compareTo( s2.getId() );
                    return c;
                }
            };

            setComparator(c);
            return;
        }

        if(col == 1){
            Comparator<EventSession> c = new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    return - s1.getId().compareTo(s2.getId());
                }
            };
            setComparator(c);
            return;
        }

        
        if(col == 2){
            setComparator(new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    BigInteger i1 = new BigInteger(s1.getInetAddress().getAddress());
                    BigInteger i2 = new BigInteger(s2.getInetAddress().getAddress());
                    int c = i1.compareTo( i2 );
                    if(c == 0)
                        return s1.getId().compareTo( s2.getId() );
                    return - c;
                }
            });
            return;
        }
        
        if(col == 3){
            setComparator(new Comparator<EventSession>(){
                public int compare(EventSession s1, EventSession s2){
                    int c = (new Integer(s1.getEvents().size())).compareTo( s2.getEvents().size() );
                    if(c == 0)
                        return s1.getId().compareTo( s2.getId() );
                    return - c;
                }
            });
            return;
        }
    }

    public void setComparator(Comparator<EventSession> c){
        comparator = c;
        
        SortedSet<EventSession> sessionsNew = new TreeSet<EventSession>(comparator);
        sessionsNew.addAll( sessions );
        sessions.clear();
        sessions = sessionsNew;
        
        fireTableDataChanged();
    }
    
    public void addSession(EventSession s){
        if(! sessions.contains( s )){
            sessions.add( s ); 
            
            if(System.currentTimeMillis() - s.lastAccessed().getTime() <= 2*3600*1000 )
                activeSessions.add( s );
        }
    }


    /* (non-Javadoc)
     * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(int arg0, int arg1)
    {
        return false;
    }
    
    public void setShowActiveOnly(boolean b){
        onlyActive = b;
    }
    
    public void clear(){
        sessions.clear();
        activeSessions.clear();
        fireTableDataChanged();
    }
}
