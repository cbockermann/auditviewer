/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.table;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.AuditViewer;

public class TableColumnListCellRenderer
    extends DefaultTreeCellRenderer
{
    private static final long serialVersionUID = -2409041191358129205L;
    TableColumnListModel tlm;

    
    int width = 500;
    JScrollPane scrollPane;
    
    public TableColumnListCellRenderer( TableColumnListModel lm ){
        tlm = lm;
    }

    
    public void setJScrollPane( JScrollPane scroller ){
        scrollPane = scroller;
    }
    
    
    /**
     * @see javax.swing.DefaultListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean isSelected, boolean expanded, boolean leaf, int index,
            boolean cellHasFocus)
    {
        super.getTreeCellRendererComponent( tree, value, isSelected, expanded, leaf, index, hasFocus );
        
        JPanel p = new JPanel( new BorderLayout() );
        
        //p.setPreferredSize( new Dimension( Math.max( 400, super.getPreferredSize().width ), tree.getPreferredSize().height ) );
        
        this.setVerticalTextPosition( JLabel.CENTER );
        this.setIconTextGap( 14 );
        this.setAlignmentY( Component.CENTER_ALIGNMENT );
        
        if( isSelected ){
            p.setBackground( this.getBackgroundSelectionColor() );
            p.setForeground( this.getTextSelectionColor() );
        } else {
            p.setBackground( this.getBackgroundNonSelectionColor() );
            p.setForeground( this.getTextNonSelectionColor() );
        }
        
        ImageIcon icon = AuditViewer.getIcon( "org.jwall.web.audit.viewer.table.TableColumn" );
        
        if( icon != null )
            setIcon( icon );

        
        JLabel i = new JLabel( icon );
        i.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
        i.setOpaque( true );
        i.setBackground( getBackground() );
        i.setForeground( getForeground() );
        
        if( isSelected )
            i.setBackground( this.getBackgroundSelectionColor() );
        else
            i.setBackground( this.getBackgroundNonSelectionColor() );
        
        p.add( i, BorderLayout.WEST );
        
        p.setOpaque( true );
        
        if( index >= 0 && index < tlm.getSize() ){
            StringBuffer txt = new StringBuffer("<html><body style=\"font-family: Verdana,Arial,Sansserif; font-size:11pt;\">");
            
            TableColumn column = (TableColumn) tlm.getElementAt( index );
            txt.append( "Name: <b>" );
            txt.append( column.getColumnName() );
            txt.append( "</b> Variable: <b>" );
            txt.append( column.getVariable() );
            txt.append( "</b></body></html>" );

            JLabel text = new JLabel( txt.toString() );
            text.setAlignmentY( Component.CENTER_ALIGNMENT );
            text.setVerticalTextPosition( JLabel.CENTER );
            text.setBorder( null ); //new EmptyBorder( 5, 5, 5, 5 ) );
            
            if( isSelected ){
                text.setForeground( this.getTextSelectionColor() );
                text.setBackground( this.getBackgroundSelectionColor() );
            } else {
                text.setForeground( this.getTextNonSelectionColor() );
                text.setBackground( this.getBackgroundNonSelectionColor() );
            }
            
            p.add( text, BorderLayout.CENTER );
        }
        
        int spWidth = tree.getWidth(); //scrollPane.getWidth();
        int treeRowHeight = tree.getRowHeight();
        p.setPreferredSize( new Dimension( Math.max( width, spWidth ), treeRowHeight ) );
        
        return p;
    }
}
