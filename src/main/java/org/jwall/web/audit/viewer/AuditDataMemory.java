package org.jwall.web.audit.viewer;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jwall.AuditViewer;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.viewer.table.DefaultColumnMapper;
import org.jwall.web.audit.viewer.table.TableColumnMapper;

/**
 * This class implements a memory-backed storage of audit data. The events are
 * stored in a large array list.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AuditDataMemory implements AuditDataView {

	LinkedList<DataViewListener> dataListener = new LinkedList<DataViewListener>();

	/** The columns which are to be displayed in the table */
	ArrayList<String> columns = new ArrayList<String>();

	/** A simple format for displaying an event's date */
	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yy hh:mm:ss");

	/** This is the list of events to be backed by the table */
	ArrayList<AuditEvent> events;

	/**
	 * The events to be shown, which might differ from the events-vector in case
	 * of a filter-expression
	 */
	public ArrayList<AuditEvent> view;

	/** Filters for selecting only a subset of the events to display */
	List<AuditEventFilter> filters = new LinkedList<AuditEventFilter>();

	public AuditEventFilter viewFilter = null;

	Date firstEvent = null;
	Date lastEvent = null;

	/**
	 * 
	 * Creates a new table model with the default columns displayed.
	 * 
	 */
	public AuditDataMemory() {
		this(10000);
	}

	public AuditDataMemory(int size) {
		events = new ArrayList<AuditEvent>();
		view = new ArrayList<AuditEvent>();
	}

	/**
	 * Fills this eventTable with the events of the given session.
	 * 
	 * @param s
	 */
	public void setEvents(Collection<AuditEvent> s) {
		events.clear();
		view.clear();

		for (AuditEvent evt : s)
			addEvent(evt);
	}

	/**
	 * This returns the number of events of the session.
	 * 
	 */
	public int getRowCount() {
		if (events == null)
			return 0;

		if (filters.isEmpty() && viewFilter == null)
			return events.size();
		else
			return view.size();
	}

	public Object getValueForEvent(int idx, String var) {
		AuditEvent evt = this.getEvent(idx);
		if (evt == null)
			return null;

		return evt.get(var);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#getEvent(int)
	 */
	public AuditEvent getEvent(int i) {
		if (filters.isEmpty())
			return events.get(i);

		return view.get(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#addEvent(org.jwall.web.audit
	 * .AuditEvent)
	 */
	public void addEvent(AuditEvent e) {

		if (this.firstEvent == null
				|| e.getDate().getTime() < firstEvent.getTime()) {
			firstEvent = e.getDate();
		}

		if (this.lastEvent == null || lastEvent.before(e.getDate())) {
			// System.out.println( "Found new last event" );
			lastEvent = e.getDate();
		}

		events.add(e);

		// if there is a filter applied, we only add the event to the
		// view if it matches the filter
		//
		if (viewIncludes(e) && filterMatches(e))
			view.add(e);

		// notify all listeners
		//
		this.dataViewChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#addEvents(java.util.Collection)
	 */
	public void addEvents(Collection<AuditEvent> c) {

		for (AuditEvent evt : c)
			this.addEvent(evt);

		// notify all listeners about new rows being available
		//
		this.dataViewChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#clear()
	 */
	public void clear() {
		events.clear();
		view = events;
		this.dataViewChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#getEvents()
	 */
	public Collection<AuditEvent> getEvents() {
		return getFilteredEvents();
	}

	/**
	 * 
	 * @return
	 */
	public Collection<AuditEvent> getFilteredEvents() {
		if (!filters.isEmpty() || viewFilter != null) {
			// System.out.println("Returning subset view, viewFilter = " +
			// viewFilter );
			return view;
		} else
			return events;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#firstEventDate()
	 */
	public Date firstEventDate() {
		return this.firstEvent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#lastEventDate()
	 */
	public Date lastEventDate() {
		return this.lastEvent;
	}

	public static TableColumnMapper<AuditEvent> getColumnMapper(String layout) {

		AuditViewer viewer = AuditViewer.getAuditViewer();

		try {
			if (viewer != null) {
				if (layout.length() > 0)
					layout = "-" + layout;

				File in = new File(viewer.getPreferenceDirectory()
						+ File.separator + "table-columns" + layout + ".xml");
				return DefaultColumnMapper.read(in);
			}
		} catch (Exception e) {
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.jwall.web.audit.viewer.AuditDataView#getViewFilter()
	 */
	public AuditEventFilter getViewFilter() {
		return viewFilter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.jwall.web.audit.viewer.AuditDataView#setViewFilter(org.jwall.web.
	 * audit.filter.AuditEventFilter)
	 */
	public void setViewFilter(AuditEventFilter filter) {
		viewFilter = filter;
	}

	public void viewHasChanged() {
		System.out.println("Adjusting event-table-view, view-filter is : \n"
				+ viewFilter.toXML());
		applyFilters();
	}

	public void setFilters(List<AuditEventFilter> filterList) {
		if (filterList == null)
			this.filters = new LinkedList<AuditEventFilter>();
		else
			filters = filterList;
	}

	public List<AuditEventFilter> getFilters() {
		return filters;
	}

	/**
	 * @see org.jwall.web.audit.viewer.AuditDataView#applyFilters()
	 */
	public void applyFilters() {
		view = new ArrayList<AuditEvent>(events.size());
		Iterator<AuditEvent> it = events.iterator();
		while (it.hasNext()) {
			AuditEvent e = it.next();
			if (viewIncludes(e) && filterMatches(e))
				view.add(e);
		}

		this.dataViewChanged();
	}

	public int size() {
		return events.size();
	}

	private boolean viewIncludes(AuditEvent e) {

		if (viewFilter != null)
			return !viewFilter.matches(e);

		return true;
	}

	private boolean filterMatches(AuditEvent e) {

		if (filters.isEmpty())
			return true;

		for (AuditEventFilter f : filters)
			if (f.matches(e))
				return true;

		return false;
	}

	public void dataViewChanged() {
		for (DataViewListener l : this.dataListener)
			l.dataViewChanged();
	}

	public void addDataViewListener(DataViewListener l) {
		dataListener.add(l);
	}

	public void removeDataViewListener(DataViewListener l) {
		dataListener.remove(l);
	}
}