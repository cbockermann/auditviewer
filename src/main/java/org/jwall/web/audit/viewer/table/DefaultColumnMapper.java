/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.table;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Logger;

import org.jwall.AuditViewer;
import org.jwall.app.Application;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.session.HttpProtocol;
import org.jwall.web.audit.viewer.Resolver;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * 
 * This is the default implementation of a column mapper.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
@XStreamAlias("ColumnSetup")
public class DefaultColumnMapper implements TableColumnMapper<AuditEvent> {

	public final static String COLUMN_DATE = "DATE";
	public final static String COLUMN_REMOTE_ADDR = Application
			.getMessage("org.jwall.web.audit.event.table.column.remote-address");
	public final static String COLUMN_REQUEST_METHOD = Application
			.getMessage("org.jwall.web.audit.event.table.column.request-method");
	public final static String COLUMN_RESPONSE_STATUS = Application
			.getMessage("org.jwall.web.audit.event.table.column.response-status");
	public final static String COLUMN_HEADER_HOST = "Host";
	public final static String COLUMN_REQUEST_FILENAME = Application
			.getMessage("org.jwall.web.audit.event.table.column.request-filename");
	public final static String COLUMN_QUERY_STRING = Application
			.getMessage("org.jwall.web.audit.event.table.column.query-string");
	public final static String COLUMN_AUTH_TYPE = Application
			.getMessage("org.jwall.web.audit.event.table.column.auth-type");
	public final static String COLUMN_AUTHORIZATION = Application
			.getMessage("org.jwall.web.audit.event.table.column.authorization");
	public final static String COLUMN_SESSION_ID = ModSecurity.SESSIONID;

	/* A shared logger for this class */
	static Logger log = Logger.getLogger(DefaultColumnMapper.class
			.getCanonicalName());

	/** These columns are displayed by default */
	transient String[] defaultColumns = new String[] { COLUMN_DATE,
			ModSecurity.REMOTE_ADDR, // 1
			ModSecurity.REQUEST_METHOD, // 2
			ModSecurity.RESPONSE_STATUS, // 3
			COLUMN_HEADER_HOST, ModSecurity.REQUEST_FILENAME, // 4
			ModSecurity.QUERY_STRING, // 5
			ModSecurity.AUTH_TYPE, // 6
			HttpProtocol.REQUEST_HEADER_AUTHORIZATION, // 7
			ModSecurity.SESSIONID // 8
	};

	@XStreamImplicit
	ArrayList<TableColumn> tableColumns;

	@XStreamAsAttribute
	String name = "";

	/** Display the date in a localized way */
	transient DateFormat fmt = DateFormat.getDateTimeInstance(DateFormat.SHORT,
			DateFormat.SHORT, Locale.getDefault());

	public DefaultColumnMapper() {
		// fill in the default columns
		//
		tableColumns = new ArrayList<TableColumn>();

		for (String column : defaultColumns) {
			tableColumns.add(new TableColumn(column, Application
					.getMessage(column)));
		}

		name = "default";
	}

	public DefaultColumnMapper(ArrayList<TableColumn> columns) {
		tableColumns = columns;

		name = "default";
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/**
	 * @see org.jwall.web.audit.viewer.table.TableColumnMapper#getColumns()
	 */
	public ArrayList<TableColumn> getColumns() {
		return tableColumns;
	}

	/**
	 * @see org.jwall.web.audit.viewer.table.TableColumnMapper#getColumnName(int)
	 */
	public String getColumnName(int col) {
		return tableColumns.get(col).getColumnName();
	}

	/**
	 * @see org.jwall.web.audit.viewer.table.TableColumnMapper#extract(java.lang.Object,
	 *      int)
	 */
	public Object extract(AuditEvent evt, int colIndex) {

		if (evt == null)
			return null;

		String column = getColumns().get(colIndex).getVariable();
		if (column == null)
			return "null";

		if (COLUMN_DATE.equals(column))
			return fmt.format(evt.getDate());

		if (column.equals(ModSecurity.REMOTE_ADDR))
			return Resolver.resolve(evt.get(ModSecurity.REMOTE_ADDR));

		if (column.equals(ModSecurity.SESSIONID))
			return evt.getSessionId();

		return evt.get(column);
	}

	public Object readResolve() {
		this.fmt = DateFormat.getDateTimeInstance(DateFormat.SHORT,
				DateFormat.MEDIUM, Locale.getDefault());
		return this;
	}

	public static XStream getXStream() {
		XStream xs = new XStream(new DomDriver());
		xs.processAnnotations(TableColumn.class);
		xs.processAnnotations(DefaultColumnMapper.class);
		return xs;
	}

	public static TableColumnMapper<AuditEvent> read(File in)
			throws IOException {
		return read(new FileInputStream(in));
	}

	@SuppressWarnings("unchecked")
	public static TableColumnMapper<AuditEvent> read(InputStream in)
			throws IOException {
		TableColumnMapper<AuditEvent> mapper = (TableColumnMapper<AuditEvent>) getXStream()
				.fromXML(in);
		return mapper;
	}

	public static void write(TableColumnMapper<AuditEvent> mapper, File out)
			throws IOException {
		if (!out.getParentFile().exists())
			out.getParentFile().mkdirs();

		getXStream().toXML(mapper, new FileOutputStream(out));
	}

	public static TableColumnMapper<AuditEvent> getColumnMapper(
			String layoutName) {

		try {
			String layout = layoutName;
			if (!layoutName.startsWith("-"))
				layout = "-" + layoutName;

			File in = new File(AuditViewer.getAuditViewer()
					.getPreferenceDirectory()
					+ File.separator
					+ "table-columns" + layout + ".xml");
			log.fine("Trying to read column-layout from "
					+ in.getAbsolutePath());
			if (!in.canRead())
				return null;

			TableColumnMapper<AuditEvent> mapper = DefaultColumnMapper.read(in);
			if (mapper != null)
				return mapper;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new DefaultColumnMapper();
	}
}
