/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.session;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;

import org.jwall.AuditViewer;
import org.jwall.app.Application;
import org.jwall.app.ui.DisplayDialog;
import org.jwall.app.ui.TaskProgressDialog;
import org.jwall.app.ui.View;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventQueue;
import org.jwall.web.audit.FeederThread;
import org.jwall.web.audit.session.Session;
import org.jwall.web.audit.session.SessionTracker;
import org.jwall.web.audit.viewer.AuditDataMemory;
import org.jwall.web.audit.viewer.AuditDataTable;
import org.jwall.web.audit.viewer.ui.EventProcessor;

public class SessionView
extends View
implements ListSelectionListener, ActionListener, MouseListener
{
	public final static long serialVersionUID = 123L;

	private JScrollPane tableScroller;
	private JTable table; //, etable;
	private SessionTable sessionTable;
	private AuditDataTable eventTable = new AuditDataTable( new AuditDataMemory() );
	private JCheckBox activeOnly;
	private int lastSort = -1;

	
	/**
	 * 
	 *
	 */
	public SessionView(AuditViewer viewer, SessionTable stable, EventProcessor ep ){
		super( viewer );
		setLayout(new BorderLayout());

		sessionTable = stable;
		table = new JTable( sessionTable );
		table.setShowGrid( true );
		table.setRowHeight(20);
		table.setSelectionMode( ListSelectionModel.SINGLE_INTERVAL_SELECTION );
		table.getSelectionModel().addListSelectionListener(this);
		table.getTableHeader().addMouseListener(this);
		table.addMouseListener( this );

		tableScroller = new JScrollPane(table);
		/*
		etable = new JTable(eventTable);
		etable.setShowGrid( true );
		etable.setRowHeight(20);
		etable.setSelectionMode( ListSelectionModel.SINGLE_INTERVAL_SELECTION );
		etable.addMouseListener( this );
        */
		SessionEventTableView evtTableView = new SessionEventTableView( eventTable );
		evtTableView.addEventProcessor( ep );
		
		//JPanel events = new JPanel(new BorderLayout());
		//events.add(new FilterPanel(eventTable), BorderLayout.NORTH);
		//events.add(new JScrollPane(etable), BorderLayout.CENTER);

		JPanel sPanel = new JPanel(new BorderLayout());
		sPanel.add( tableScroller, BorderLayout.CENTER );

		JPanel opt = new JPanel(new FlowLayout(FlowLayout.LEFT));
		activeOnly = new JCheckBox("Show only active sessions");
		activeOnly.setEnabled( false );
		activeOnly.addActionListener(this);
		opt.add(activeOnly);
		sPanel.add( opt, BorderLayout.NORTH );

		JSplitPane sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, sPanel, evtTableView );
		sp.setDividerLocation(350);

		add(sp, BorderLayout.CENTER);
	}

	/**
	 * @param c
	 */
	public void createSessions(SessionTracker tracker, Collection<AuditEvent> c){
		sessionTable.clear();
		
		AuditEventQueue queue = new AuditEventQueue( c );
		FeederThread ft = new FeederThread( queue, tracker, queue.size() );
		TaskProgressDialog pd = new TaskProgressDialog( AuditViewer.getWindow(), FeederThread.STATUS_READ_FILE );
		pd.setIcon( Application.getIcon("org.jwall.web.audit.events.process-events") );
		pd.setTitle( "Creating sessions..." );
		ft.addTaskMonitor( pd );
		ft.start();

		try {
			ft.join();
		} catch (Exception ex){
			ex.printStackTrace();
		}

		Collection<EventSession> sess = new ArrayList<EventSession>();
		for( Session s : tracker.getSessions() )
			sess.add( new EventSession(s) );
		
		sessionTable.setSessions( sess );
	}

	public void setSessions( Collection<Session> sessions ){
		
		Collection<EventSession> sess = new ArrayList<EventSession>();
		for( Session s : sessions )
			sess.add( new EventSession(s) );

		sessionTable.setSessions( sess);
	}


	public void actionPerformed(ActionEvent e){

		if("help.usage".equals(e.getActionCommand())){
			URL url = SessionView.class.getResource("/org/jwall/http/usage.en.html");

			if(url != null){
				DisplayDialog d = new DisplayDialog(url);
				d.setModal(false);
				d.setVisible(true);
			}
		}

		if("help.http".equals(e.getActionCommand())){

			URL url = SessionView.class.getResource("/org/jwall/http/ResponseCodes.en.html");

			if(url != null){
				DisplayDialog d = new DisplayDialog(url);
				d.setModal(false);
				d.setVisible(true);
			}
		}

		if("help.about".equals(e.getActionCommand())){

			URL url = SessionView.class.getResource("/org/jwall/http/about.html");

			if(url != null){
				DisplayDialog d = new DisplayDialog(url);
				d.setVisible(true);
			}
		}

	}


	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent e)
	{
		//System.out.println(e);
		if(! e.getValueIsAdjusting()){
			int row = table.getSelectedRow();
			if(row >= 0 ){
			    eventTable.clear();
			    eventTable.addEvents( sessionTable.getSession( row ).getEvents() );
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	public void mouseClicked(MouseEvent arg0)
	{
		if(arg0.getSource() == table.getTableHeader()){
			JTableHeader h = table.getTableHeader();
			int col = h.getColumnModel().getColumnIndexAtX( arg0.getX());
			if( lastSort == col )
				sessionTable.reverseBy( col );
			else
				sessionTable.sortBy( col );
			lastSort = col;
		}

		/*
		if(arg0.getSource() == etable && arg0.getClickCount() > 1){
			int col = etable.getSelectedRow();
			if(col >= 0){
				AuditEvent evt = eventTable.getEvent( col );
				RequestEditor inj = new RequestEditor( evt );
				inj.setVisible( true );
			}
		}
		*/

		if(arg0.getSource() == table && arg0.getButton() != MouseEvent.BUTTON1){
			Vector<Session> s = new Vector<Session>();

			if(table.getSelectedRowCount() > 0){
				int[] ids = table.getSelectedRows();
				for(int i = 0; i < ids.length; i++){
					//Session sess = smodule.getSessionTable().getSession( ids[i] );
					EventSession sess = sessionTable.getSession( ids[i] );
					s.add(sess.session);
				}
			} else {
				int idx = table.rowAtPoint( arg0.getPoint() );
				table.getSelectionModel().setSelectionInterval(idx, idx);

				//s.add( smodule.getSessionTable().getSession( idx ) );
				s.add( sessionTable.getSession( idx ).session );
			}

			//SessionPopupMenu m = new SessionPopupMenu( smodule, s );
			//m.show( eventTable, arg0.getX(), arg0.getY() );
		}
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	public void mouseEntered(MouseEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	public void mouseExited(MouseEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	public void mousePressed(MouseEvent arg0)
	{
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	public void mouseReleased(MouseEvent arg0)
	{
	}
}
