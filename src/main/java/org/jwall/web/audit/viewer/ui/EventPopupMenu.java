/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Vector;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jwall.app.Application;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.viewer.table.TableViewHandler;


/**
 * 
 * 
 * @author chris@jwall.org
 *
 */
public class EventPopupMenu
extends PopupMenu
implements ActionListener
{
    public final static long serialVersionUID = 12312L;
    private Collection<AuditEvent> events;
    private Vector<EventProcessor> evtProcessors = new Vector<EventProcessor>();

    public EventPopupMenu(Collection<EventProcessor> handler, Collection<AuditEvent> evt){
        //super("AuditEvent");
        evtProcessors = new Vector<EventProcessor>();
        evtProcessors.addAll( handler );
        events = evt;


        if( isHandled( EventProcessor.ACTION_OBFUSCATE_TO_CLIPBOARD, evtProcessors )
                || isHandled( EventProcessor.ACTION_OBFUSCATE_TO_DISK, evtProcessors ) ){
            JMenuItem m3 = new JMenu("Obfuscate");
            m3.setIcon( Application.getIcon("org.jwall.web.audit.events.obfuscate"));

            Toolkit tk = Toolkit.getDefaultToolkit();
            Clipboard clip = tk.getSystemClipboard();
            if( clip.isDataFlavorAvailable( DataFlavor.stringFlavor ) ){
                JMenuItem m32 = new JMenuItem("to Clip-Board");
                m32.setIcon( Application.getIcon( EventProcessor.ACTION_OBFUSCATE_TO_CLIPBOARD ) );
                m32.setActionCommand( EventProcessor.ACTION_OBFUSCATE_TO_CLIPBOARD );
                m32.addActionListener( this );
                m3.add( m32 );
                add( m3 );
            }

            if( isHandled( EventProcessor.ACTION_OBFUSCATE_TO_DISK, evtProcessors ) ){
                JMenuItem m31 = new JMenuItem("to File");
                m31.setIcon( Application.getIcon( EventProcessor.ACTION_OBFUSCATE_TO_DISK ) );
                m31.setActionCommand( EventProcessor.ACTION_OBFUSCATE_TO_DISK );
                m31.addActionListener( this );
                m3.add( m31 );
            }
        }

        if( isHandled( EventProcessor.ACTION_SAVE, evtProcessors ) ) {
            JMenuItem m2 = new JMenuItem("Save event");
            m2.setIcon( Application.getIcon( EventProcessor.ACTION_SAVE ) );
            m2.setActionCommand( EventProcessor.ACTION_SAVE );
            m2.addActionListener(this);
            add(m2);
        }

        if( isHandled( EventProcessor.ACTION_INJECT, evtProcessors ) ) {
            JMenuItem m = new JMenuItem("Re-inject event");
            m.setIcon( Application.getIcon( EventProcessor.ACTION_INJECT ) );
            m.setActionCommand( EventProcessor.ACTION_INJECT );
            m.addActionListener(this);
            add(m);
        }
        
        JMenu view = new JMenu( "View" );
        if( isHandled( TableViewHandler.ACTION_REMOVE_BY_ID, evtProcessors ) ){

            String label = "Remove Event";
            if( evt.size() > 1 )
                label = "Remove Events";
        
            JMenuItem removeById = new JMenuItem( label );
            removeById.setActionCommand( TableViewHandler.ACTION_REMOVE_BY_ID );
            removeById.addActionListener( this );
            view.add( removeById );
        }
        
        if( isHandled( TableViewHandler.ACTION_REMOVE_BY_RULE, evtProcessors ) ){
            String label = "Remove by rule";
            JMenuItem removeByRule = new JMenuItem( label );
            removeByRule.setActionCommand( TableViewHandler.ACTION_REMOVE_BY_RULE );
            removeByRule.addActionListener( this );
            view.add( removeByRule );
        }
        
        add( view );
    }


    public boolean isHandled( String action, Collection<EventProcessor> processors ){

        for( EventProcessor ep : processors )
            if( ep.handles( action ) ){
                return true;
            } else {
                System.out.println( "EventProcessor " + ep + " does not handle " + action );
            }
        

        return false;
    }


    public void actionPerformed(ActionEvent e){
        System.out.println("AuditEventAction: " + e );
        for( EventProcessor evtProcessor : evtProcessors ){
            System.out.println( "Delegating event to " + evtProcessor );
            evtProcessor.processEvent( e.getActionCommand(), events );
        }
    }
}
