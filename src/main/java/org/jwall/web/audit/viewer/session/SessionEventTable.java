/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.session;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.swing.table.AbstractTableModel;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.viewer.Resolver;
import org.jwall.web.http.HttpRequest;

public class SessionEventTable extends AbstractTableModel {
	public final static long serialVersionUID = 1L;
	static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yy hh:mm:ss");
	EventSession session;
	String[] columns = new String[] { "Datum", // 0
			"Time since last events", // 1
			ModSecurity.REMOTE_ADDR, // 2
			ModSecurity.REQUEST_METHOD, // 3
			ModSecurity.RESPONSE_STATUS, // 4
			ModSecurity.REQUEST_FILENAME, // 5
			ModSecurity.QUERY_STRING, // 6
			ModSecurity.AUTH_TYPE, // 7
			HttpRequest.USER_AGENT, // 8
			HttpRequest.REFERER, // 9
			HttpRequest.AUTHORIZATION };

	/**
     * 
     *
     */
	public SessionEventTable() {
		session = null;
	}

	/**
	 * Fills this eventTable with the events of the given session.
	 * 
	 * @param s
	 */
	public void setSession(EventSession s) {
		session = s;

		this.fireTableDataChanged();
	}

	/**
	 * Returns the session which is displayed by this eventTable.
	 * 
	 * @return
	 */
	public EventSession getSession() {
		return session;
	}

	/**
	 * This returns the number of events of the session.
	 * 
	 */
	public int getRowCount() {
		if (session == null)
			return 0;

		return session.getEvents().size();
	}

	/**
	 * Returns the value of cell <code>rowIndex</code>, <code>columnIndex</code>
	 * .
	 * 
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		int idx = -1;
		AuditEvent last = null;
		AuditEvent evt = null;
		Iterator<AuditEvent> it = session.getEvents().iterator();
		while (it.hasNext() && idx < rowIndex) {
			last = evt;
			evt = it.next();
			idx++;
		}
		// System.out.println("row: "+rowIndex+", evt: "+evt);

		if (evt != null) {
			if (columnIndex == 0)
				return fmt.format(evt.getDate());

			if (columnIndex == 1) {
				if (last != null && last.getDate().getTime() > 1000) {
					long d = (evt.getDate().getTime() - last.getDate()
							.getTime()) / 1000;

					// System.out.println("calculating: "+evt.getDate().getTime()+
					// " - "+last.getDate().getTime());

					long h = d / 3600;
					long m = (d % 3600) / 60;
					long s = (d % 3600) - m * 60;

					DecimalFormat fmt = new DecimalFormat("00");

					if (h > 0)
						return fmt.format(h) + ":" + fmt.format(m) + ":"
								+ fmt.format(s);
					else
						return fmt.format(m) + ":" + fmt.format(s);
				} else
					return "00:00";
			}

			if (columns[columnIndex] == ModSecurity.REMOTE_ADDR)
				return Resolver.resolve(evt.get(ModSecurity.REMOTE_ADDR));

			if (columnIndex > 7)
				return evt.get(ModSecurity.REQUEST_HEADER);

			/*
			 * if(columnIndex == 1) return evt.get( ModSecurity.REQUEST_METHOD
			 * );
			 * 
			 * if(columnIndex == 2) return evt.get( ModSecurity.REQUEST_URI );
			 * 
			 * if(columnIndex == 3) return evt.get( ModSecurity.QUERY_STRING );
			 * 
			 * 
			 * if(columnIndex == 5) return evt.getRequestHeader(
			 * HttpProtocol.REQUEST_HEADER_USER_AGENT );
			 */

			return evt.get(columns[columnIndex]);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) {
		return columns[column];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return columns.length;
	}

	/**
	 * Returns the event at position <code>i</code> of the session which is
	 * associated with this eventTable.
	 * 
	 * @deprecated
	 * @param i
	 * @return
	 */
	public AuditEvent getEvent(int i) {

		int idx = -1;
		AuditEvent evt = null;
		Iterator<AuditEvent> it = session.getEvents().iterator();
		while (it.hasNext() && idx < i) {
			evt = it.next();
			idx++;
		}

		return evt;
	}
}
