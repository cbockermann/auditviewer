/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.util.Collection;

import org.jwall.web.audit.AuditEvent;


/**
 * 
 * This interface defines a processor for manipulating events in some way.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface EventProcessor
{
    public final static String ACTION_VIEW = "event.view";
    public final static String ACTION_SAVE = "org.jwall.web.audit.event.save";
    public final static String ACTION_INJECT = "org.jwall.web.audit.event.re-inject";
    
    public final static String ACTION_OBFUSCATE_TO_DISK = "org.jwall.web.audit.events.obfuscate.to-file";
    public final static String ACTION_OBFUSCATE_TO_CLIPBOARD = "org.jwall.web.audit.events.obfuscate.to-clipboard";
    
    public boolean handles( String actionCommand );
    
    public void processEvent(String cmd, Collection<AuditEvent> evt);
}
