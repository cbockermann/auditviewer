/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jwall.AuditViewer;
import org.jwall.app.Application;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.filters.AuditFilterListDialog;
import org.jwall.web.audit.viewer.AuditDataTable;
import org.jwall.web.audit.viewer.table.DefaultColumnMapper;
import org.jwall.web.audit.viewer.table.TableColumn;
import org.jwall.web.audit.viewer.table.TableColumnListDialog;
import org.jwall.web.audit.viewer.table.TableColumnMapper;


/**
 * 
 * This class implements a view displaying a list of audit-event objects within a table.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class EventTableView
extends JPanel
implements MouseListener, ListSelectionListener
{
    /** The unique class id */
    private static final long serialVersionUID = 3634721591798608348L;

    /** A shared logger instance for this class */
    static Logger log = Logger.getLogger( EventTableView.class.getCanonicalName() );
    
    /** The event table displaying the events */
    final AuditDataTable eventTable;

    /** The read event-table? */
    JTable etable;

    /** A scroller providing a scrollable view on to the table */
    JScrollPane tableScroller;

    /** The filter-panel provides mechanisms to create sub-set view of the events within the table */
    FilterPanel filterPanel;

    /** A flag indicating whether new events appended to the table should be jumped to */
    boolean tableFollows = false;

    /** A list of classes providing actions which can be applied on a (set of) event(s) */
    List<EventProcessor> eventProcessors = new LinkedList<EventProcessor>();

    List<EventSelectionListener> selectionListener = new LinkedList<EventSelectionListener>();

    String layout = "";

    JButton time;

    public EventTableView( AuditDataTable evtTable, String layout ){
        this( evtTable, true, layout );
    }

    /**
     * 
     * 
     * @param evtTable
     */
    public EventTableView( AuditDataTable evtTable, boolean showFilterPanel, String layout ){
        setLayout(new BorderLayout());

        eventTable = evtTable;

        this.layout = layout;
        TableColumnMapper<AuditEvent> mapper = DefaultColumnMapper.getColumnMapper( this.layout );
        if( mapper != null ){
            log.fine( "Found table-layout " + layout );
            eventTable.setColumnMapper( mapper );
        }

        etable = new JTable(eventTable);
        etable.setShowGrid( true );
        etable.setRowHeight(20);
        etable.addMouseListener(this);
        etable.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
        etable.getSelectionModel().addListSelectionListener( this );

        etable.getModel().addTableModelListener(new TableModelListener(){
            public void tableChanged(TableModelEvent e){
                if(getTableFollows())
                    scrollToEnd();
            }
        });
        etable.setBorder( null );

        tableScroller = new JScrollPane(etable);
        tableScroller.setBorder( null );

        if( showFilterPanel ){
            JPanel f = new JPanel(new FlowLayout(FlowLayout.LEFT));


            JButton edit = new JButton();
            edit.setIcon( AuditViewer.getIcon( "org.jwall.web.audit.viewer.table.TableLayout" ) );
            edit.setBorderPainted( false );
            edit.setPreferredSize( new Dimension( 20, 20 ) );

            edit.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e ){

                    try {
                        String layout = getTableLayout();
                        TableColumnListDialog d = new TableColumnListDialog( eventTable.getColumnMapper().getColumns() );
                        d.setVisible( true );

                        if( d.getReturnStatus() > 0 ){
                            ArrayList<TableColumn> cols = d.getColumnList();
                            DefaultColumnMapper mapper = new DefaultColumnMapper( cols );
                            eventTable.setColumnMapper( mapper );


                            if( d.getReturnStatus() > 1 ){

                                try {
                                    AuditViewer viewer = AuditViewer.getAuditViewer();
                                    if( viewer != null ){
                                        if( layout.length() > 0 )
                                            layout = "-" + layout;
                                        File out = new File( viewer.getPreferenceDirectory() + File.separator + "table-columns" + layout + ".xml" );
                                        log.fine( "Writing table-layout to " + out.getAbsolutePath() );
                                        DefaultColumnMapper.write( mapper, out );
                                    }
                                } catch (IOException ex) {
                                    JOptionPane.showMessageDialog( AuditViewer.getWindow(), 
                                            "An error occurred while writing the new table column-mappings to disk:" + ex.getMessage(), 
                                            "Error while saving table column mappings", 
                                            JOptionPane.ERROR_MESSAGE );
                                }
                            }
                        }
                    } catch (Exception e2) {
                        Application.handleException( e2 );
                    }
                }
            });

            f.add( edit );

            JCheckBox follow = new JCheckBox("Autoscroll");
            follow.setSelected(getTableFollows());
            follow.setToolTipText("If enabled, causes the eventTable to automatically scroll to the end as new events arrive");
            follow.addActionListener( new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    setTableFollows( ! getTableFollows() );
                }
            });
            filterPanel = new FilterPanel(eventTable);
            eventTable.addTableModelListener( filterPanel );
            f.add(filterPanel);
            
            JButton showFilters = new JButton( "Show filters" );
            showFilters.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent e) {
                    
                    List<AuditEventFilter> filters = getViewFilters();
                    AuditFilterListDialog d = new AuditFilterListDialog( filters );
                    d.setVisible(true);
                }
            });
            f.add( showFilters );
            
            time = new JButton( "time" );
            time.addActionListener( new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    JDialog d = new JDialog();
                    d.getContentPane().setLayout( new BorderLayout() );
                    TimeScalePanel tsp = new TimeScalePanel( getTimeScale() );
                    d.getContentPane().add( tsp, BorderLayout.CENTER );
                    d.pack();
                    d.setLocation( time.getLocation().x, time.getLocation().y + 30 );
                    d.setModal( true );
                    d.setAlwaysOnTop( true );
                    d.setVisible( true );
                }
            });
            
            add(f, BorderLayout.NORTH);
        }

        add(tableScroller, BorderLayout.CENTER);
    }

    public List<AuditEventFilter> getViewFilters(){
        AuditEventFilter f = this.eventTable.getViewFilter();
        List<AuditEventFilter> filters = new LinkedList<AuditEventFilter>();
        filters.add( f );
        
        return filters;
    }
    

    public String getTableLayout(){
        return this.layout;
    }


    public void scrollToRow(int row){
        etable.scrollRectToVisible( etable.getCellRect( row, 0, true) );
    }

    public void scrollToEnd(){
        scrollToRow( etable.getRowCount() );
    }


    public void scrollToEnd(int row){
        tableScroller.scrollRectToVisible( etable.getCellRect( row, 0, true ) );
    }

    public void setTableFollows(boolean b){
        tableFollows = b;
    }

    public boolean getTableFollows(){
        return tableFollows;
    }

    
    public TimeScale getTimeScale(){
        return new TimeScale( getEventTable(), null, 1000 );
    }
    
    public AuditDataTable getEventTable(){
        return this.eventTable;
    }

    public void setColumnMapper( TableColumnMapper<AuditEvent> mapper ){
        this.eventTable.setColumnMapper( mapper );
    }

    public TableColumnMapper<AuditEvent> getColumnMapper() {
        return eventTable.getColumnMapper();
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent e)
    {
        if(e.getSource() == this.etable && e.getClickCount() > 1 && e.getButton() == MouseEvent.BUTTON1){
            int row = etable.getSelectedRow();


            if( row >= 0 ){
                EventDisplayDialog d = new EventDisplayDialog();
                d.setEventTableView( this );
                d.setEvent( row );

                d.setVisible( true );
                return;
            }
        }

    }


    public void handleMouseClick( MouseEvent evt ){

    }

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent arg0)
    {
    }

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent arg0)
    {
    }

    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent e)
    {
        //if( (e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3 ) && e.getSource() == this.etable && etable.getSelectedRowCount() > 0 ){
        if( (e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3 ) && e.getSource() == this.etable && etable.getSelectedRowCount() > 0 ){
            Vector<AuditEvent> evts = new Vector<AuditEvent>();
            int row = etable.rowAtPoint( e.getPoint() );

            if(row >= 0){
                int[] rows = etable.getSelectedRows();
                for(int i = 0; i < rows.length; i++){
                    AuditEvent evt = eventTable.getEvent(rows[i]);
                    evts.add( evt );
                }
                EventPopupMenu m = new EventPopupMenu( eventProcessors, evts);
                m.show(etable, e.getX(), e.getY() );    
            }
        }
    }


    /* (non-Javadoc)
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent arg0)
    {
    }


    /**
     * This adds a new event processor instance to the list of processors. Each
     * processor can provide an action or set of actions that it is able to apply
     * to a (set of) event(s) selected in the table.
     * 
     * @param evtProcessor The event-processor to add.
     */
    public void addEventProcessor( EventProcessor evtProcessor ){
        if( ! eventProcessors.contains( evtProcessor ) )
            eventProcessors.add( evtProcessor );
    }


    /**
     * Remove the given processor from the list.
     * 
     * @param evtProcessor The processor to be removed.
     */
    public void removeEventProcessor( EventProcessor evtProcessor ){
        eventProcessors.remove( evtProcessor );
    }


    /**
     * This dispatches a collection of events to all event-processor instances
     * which match the given command-string.
     * 
     * @param cmd The command string denoting the action to be applied to the events.
     * @param evts The list of events to be processed.
     */
    public void processEvents( String cmd, Collection<AuditEvent> evts ){
        for( EventProcessor p : eventProcessors ){
            p.processEvent( cmd, evts );
        }
    }

    public void addEventSelectionListener( EventSelectionListener l ){
        selectionListener.add( l );
    }

    public void removeEventSelectionListener( EventSelectionListener l ){
        selectionListener.remove( l );
    }

    public void valueChanged(ListSelectionEvent e){
        if(! e.getValueIsAdjusting()){
            int idx = etable.getSelectedRow();

            if( idx >= 0 ){

                Vector<AuditEvent> events = new Vector<AuditEvent>();
                events.add( eventTable.getEvent( idx ) );

                for( EventSelectionListener l : selectionListener )
                    l.eventsSelected( events );

            }
        }
    }
}
