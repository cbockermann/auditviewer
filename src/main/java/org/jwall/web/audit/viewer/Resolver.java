/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer;

import java.net.InetAddress;
import java.util.Hashtable;

public class Resolver
{
    private static Hashtable<String, String> names = new Hashtable<String, String>();
    private static ResolverThread thread = null;

    static {
        if(thread == null){
            thread = new ResolverThread(names);
            thread.start();
        }
    }


    /**
     * This method returns the name of the given address. This is done in an
     * asynchronous way. Resolving is done by a hashmap as a cache.
     * 
     * @param address The address to resolve.
     * @return The name for the address or the address itself if resolving failed.
     */
    public static String resolve(String address){

        if( !"true".equals( System.getProperty( "org.jwall.AuditViewer.resolve-addresses" ) ) )
            return address;
        
        if( names.containsKey( address ) )
            return names.get( address );
        else {
            if(thread == null)
                thread = new ResolverThread(names);
            
            thread.add( address );
        }
        return address;
    }



    public static String resolveReally(String address){
        if( names.get( address ) != null)
            return names.get( address );
        
        try {
            InetAddress addr = InetAddress.getByName(address);

            String name = addr.getCanonicalHostName();

            names.put( address , name );
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return address;
    }
}
