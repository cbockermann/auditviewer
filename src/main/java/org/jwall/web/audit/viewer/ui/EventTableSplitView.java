/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.viewer.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.Collection;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.viewer.AuditDataTable;
import org.jwall.web.audit.viewer.AuditDataView;


/**
 * 
 * This class implements a view displaying a list of audit-event objects within a table.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class EventTableSplitView
extends JPanel
implements EventSelectionListener
{
    private static final long serialVersionUID = 3634721591798608348L;

    /** The panel displaying the event details (currently in raw text) */
    EventPanel txt = new EventPanel();

    /** The event table displaying the events */
    AuditDataView eventTable;

    EventTableView eventTableView;

    /** A scroller providing a scrollable view on to the table */
    JScrollPane tableScroller;


    public EventTableSplitView( AuditDataTable evtTable, String layoutName ){
        this( evtTable, true, layoutName );
    }

    /**
     * 
     * 
     * @param evtTable
     */
    public EventTableSplitView( AuditDataTable evtTable, boolean b, String layoutName ){
        setLayout(new BorderLayout());

        eventTableView = new EventTableView( evtTable, b, layoutName );
        eventTableView.addEventSelectionListener( this );
        
        JSplitPane sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, eventTableView, txt);
        sp.setDividerLocation(350);
        add(sp, BorderLayout.CENTER);
    }

    public void scrollToRow(int row){
        eventTableView.scrollToEnd( row );
    }

    public void scrollToEnd(){
        eventTableView.scrollToEnd();
    }


    /**
     * 
     * @param e
     */
    public void displayEvent(int i){
        if( i >= 0 ){
            AuditEvent evt = eventTable.getEvent( i );
            txt.setEvent( evt );
        }
    }

    public void displayEvent(AuditEvent e){
        txt.setEvent( e );
        txt.scroller.scrollRectToVisible( new Rectangle( 0, 0, 1, 1 ) );
    }
    
    public void addEventProcessor( EventProcessor ep ){
        eventTableView.addEventProcessor( ep );
    }
    
    
    public void eventsSelected( Collection<AuditEvent> events ){
        txt.setEvent( events.iterator().next() );
    }
}
