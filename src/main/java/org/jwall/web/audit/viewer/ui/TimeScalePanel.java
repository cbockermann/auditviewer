package org.jwall.web.audit.viewer.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class TimeScalePanel
    extends JPanel
    implements MouseMotionListener
{
    /** */
    private static final long serialVersionUID = -4069870582843237610L;
    TimeScale timeScale;

    Double xRange = Double.NaN;
    Double yRange = Double.NaN;
    
    public TimeScalePanel( TimeScale ts ){
        timeScale = ts;
        this.setPreferredSize( new Dimension( 400, 100 ) );
        this.addMouseMotionListener( this );
    }

    
    protected Double getRangeY(){
        if( yRange == Double.NaN )
            yRange = new Double( timeScale.max - Math.max( 0, timeScale.min ) );
        return yRange;
    }
    
    protected Double getRangeX(){
        if( xRange == Double.NaN )
            xRange = timeScale.getEnd().doubleValue() - timeScale.getEnd().doubleValue();
        return xRange;
    }

    /* (non-Javadoc)
     * @see javax.swing.JComponent#paint(java.awt.Graphics)
     */
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        
        int border = 10;
        int width = this.getSize().width;
        int height = this.getSize().height;
        
        Double range = this.getRangeX();
        if( range == Double.NaN ){
            range = timeScale.getEnd().doubleValue() - timeScale.getStart().doubleValue();
            //range = timeScale.steps.last().doubleValue() - timeScale.steps.first().doubleValue();
        }
        System.out.println( "x-range is " + range );
        Double offset = timeScale.getStart().doubleValue();
        
        Double xRange = new Double( width - 2*border );
        System.out.println( "x-range after border-substraction: " + xRange );
        
        Double yRange = new Double( timeScale.max );
        System.out.println( "yRange is " + yRange );
        System.out.println( " minY = " + timeScale.min  + ", maxY = " + timeScale.max );
        
        
        int x0 = 0 + border;
        int y0 = height;
        int yMax = y0 - height - border;
        int xMax = width - border;
        
        Color old = g.getColor();
        g.setColor( Color.DARK_GRAY );
        g.drawLine( x0, y0, x0, yMax );
        g.drawLine( x0, y0, xMax, y0 );
        
        int x = x0;
        Double scaleStep = timeScale.stepSize / range;
        while( x < xMax ){
            g.drawLine( x, y0, x, y0 + 2 );
            x += scaleStep;
        }

        g.setColor( old );
        
        Double scaleRange = new Double( height - 2 * border );
        
        
        for( Long step : timeScale.getTimeSteps() ){
            Double pos = x0 + 1 + xRange * (step.doubleValue() - offset) / range;
            x = pos.intValue();
            
            //Double valFrac = timeScale.getValueAt( step ) / yRange;
            //System.out.println( "Value fraction is " + valFrac );
            Double val = scaleRange * (timeScale.getValueAt( step ) / yRange);
            
            //System.out.println( "Value is: " + timeScale.getValueAt( step ) );
            //System.out.println( "plotting point for " + step + " at x-coord " + x + " (pos = " + pos + ") height is from 0 to " + ( height - val.intValue() ) );
            
            old = g.getColor();
            g.setColor( Color.RED );
            int x1 = x;
            int x2 = x1;
            int y1 = y0 - 1;
            int y2 = y1 - val.intValue();
            //System.out.println( "drawing line from (" + (width+x) + "," + height + ") to (" + x2 + "," + y2 + ")" );
            g.drawLine( x1, y1, x2, y2 );
            g.setColor( old );
        }
    }


    public void mouseDragged(MouseEvent e)
    {
        // TODO Auto-generated method stub
        
    }


    public void mouseMoved(MouseEvent e)
    {
        System.out.println( e );
    }
}
