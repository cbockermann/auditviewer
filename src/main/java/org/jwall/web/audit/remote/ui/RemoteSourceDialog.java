/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.remote.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import org.jwall.app.ui.InputDialog;
import org.jwall.web.audit.remote.RemoteConfig;

public class RemoteSourceDialog
    extends InputDialog
{
    public final static long serialVersionUID = 34234L;
    RemoteConfig config;
    RemoteConfigPanel cPanel = new RemoteConfigPanel();
    
    public RemoteSourceDialog(RemoteConfig cfg){
        super();
        setTitle("Configure Remote Source");
        setResizable( false );
        
        try {
            cPanel.setConfig(cfg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //JPanel t = new JPanel();
        //t.setBackground(Color.BLUE);
        //getContentPane().add( t, BorderLayout.CENTER );
        //getContentPane().add( cPanel, BorderLayout.EAST );
        
        getContentPane().add( cPanel, BorderLayout.CENTER );
        setSize(490,290);
    }
    
    public RemoteConfig getConfig(){
        return config;
    }
    
    public void doCancel(){
        config = null;
        setVisible(false);
    }
    
    public void doOk(){
        try {
            config = cPanel.getConfig();
            setVisible(false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error", ""+e.getMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void actionPerformed( ActionEvent e ){
    }
    

    public boolean checkInput(){
        return true;
    }
}
