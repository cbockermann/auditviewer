/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.remote.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.AuditEventRegexpFilter;
import org.jwall.web.audit.remote.RemoteConfig;

public class RemoteConfigPanel
extends JPanel
implements ActionListener
{
    public final static long serialVersionUID = 12341L;
    RemoteConfig cfg;
    JTextField server, exp;
    String[] authMethods = {
            "Password Authentication"
            //"Certificate Authentication"
    };

    String[] filterVars = {
            ModSecurity.REQUEST_URI,
            ModSecurity.REQUEST_METHOD,
            ModSecurity.QUERY_STRING,
            ModSecurity.REMOTE_ADDR,
            ModSecurity.REQUEST_FILENAME,
            ModSecurity.RESPONSE_STATUS,
            ModSecurity.SERVER_NAME
    };

    JComboBox filter = new JComboBox(filterVars);
    JComboBox auth = new JComboBox(authMethods);
    JCheckBox enableFilter = new JCheckBox("Filter events on server-side");

    public RemoteConfigPanel(){
        setLayout(null);

        setPreferredSize(new Dimension(400, 400));
        //setLayout(new GridLayout(4,2));

        filter.setEnabled( false );


        JLabel l = new JLabel("AuditConsole URL:");
        l.setBounds( 20, 30, 120, 22 );
        add(l);

        l = new JLabel("Authentication");
        l.setBounds( 20, 60, 120, 22 );
        add(l);

        //l = new JLabel("Initial Filter");
        //l.setBounds( 20, 110, 120, 22 );
        //add(l);
        enableFilter.setBounds(20, 110, 220, 22);
        enableFilter.addActionListener(this);
        add(enableFilter);

        l = new JLabel("Filter on");
        l.setBounds( 55, 140, 100, 22 );
        add(l);

        l = new JLabel("Expression");
        l.setBounds( 55, 170, 100, 22 );
        add(l);


        Font f = new Font("Monospaced", Font.PLAIN, 13);
        server = new JTextField(16);
        server.setFont(f);
        server.setBounds( 160, 30, 218, 22 );
        add(server);

        auth.setBounds( 160, 60, 218, 22 );
        //auth.setEnabled( false );
        add(auth);


        filter.setBounds( 160, 140, 218, 22 );
        add(filter);

        exp = new JTextField();
        exp.setEnabled( false );
        exp.setBounds(160, 170, 160, 22);
        add(exp);
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == enableFilter){
            exp.setEnabled( enableFilter.isSelected() );
            filter.setEnabled( enableFilter.isSelected() );
        }
    }

    public void setConfig(RemoteConfig c)
    throws Exception
    {
        cfg = c;

        server.setText( cfg.getServer() );

        enableFilter.setSelected( cfg.isFilterEnabled() );

        for(int i = 0; i < filterVars.length; i++){
            if(filterVars[i].equals( cfg.getFilterVar() )){
                filter.setSelectedIndex( i );
                i = filterVars.length + 1;
            }
        }

        filter.setEnabled( cfg.isFilterEnabled() );
        exp.setEnabled( cfg.isFilterEnabled() );
        exp.setText( cfg.getFilterExp() );
    }

    public RemoteConfig getConfig()
    throws Exception
    {
        cfg.setServer( server.getText() );
        cfg.setFilterEnabled( enableFilter.isSelected() );
        cfg.setFilterVar( filterVars[filter.getSelectedIndex()] );
        cfg.setFilterExp( exp.getText() );

        return cfg;
    }

    public boolean checkInput(){
        if(enableFilter.isSelected()){

            String var = filterVars[filter.getSelectedIndex()];
            String regexp = exp.getText();
            
            try {
                new URL( server.getText() );
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.getParent(), "Error in AuditConsole URL: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
            

            try {
                AuditEventRegexpFilter f = new AuditEventRegexpFilter(var, regexp);
                if(f != null){
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this.getParent(), "Error in FilterExpression: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        return false;
    }
}
