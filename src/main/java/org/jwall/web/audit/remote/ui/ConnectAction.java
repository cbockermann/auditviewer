/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.remote.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.jwall.app.Action;
import org.jwall.app.Application;

public class ConnectAction
    extends Action
{
    private static final long serialVersionUID = 8959168733630857190L;
    public final static String ACTION_SOURCE_CONNECT = "org.jwall.web.audit.viewer.remote.connect";
    public final static String ACTION_SOURCE_DISCONNECT = "org.jwall.web.audit.viewer.remote.disconnect";


    public ConnectAction( String cmd, String menu, double idx ){
        super( cmd, menu, idx );
    }
    
    public ConnectAction( ActionListener l, String cmd, String ml, double idx, boolean tool, boolean enabled ){
        super( l, cmd, ml, idx);
        toolbar = tool;
        setEnabled( enabled );
        showConnect();
    }
    
    
    public void showConnect(){
        putValue( ACTION_COMMAND_KEY, ACTION_SOURCE_CONNECT );
        putValue( SMALL_ICON, Application.getLargeIcon( ACTION_SOURCE_CONNECT ) );
    }
    
    public void showDisconnect(){
        putValue( ACTION_COMMAND_KEY, ACTION_SOURCE_DISCONNECT );
        putValue( SMALL_ICON, Application.getLargeIcon( ACTION_SOURCE_DISCONNECT ) );
    }
    
    public void actionPerformed( ActionEvent e) {
        super.actionPerformed( e );
    }

    /* (non-Javadoc)
     * @see org.jwall.app.Action#handles(java.awt.event.ActionEvent)
     */
    @Override
    public boolean handles(ActionEvent e)
    {
        return ACTION_SOURCE_CONNECT.equals( e.getActionCommand() ) || ACTION_SOURCE_DISCONNECT.equals( e.getActionCommand() );
    }
}
