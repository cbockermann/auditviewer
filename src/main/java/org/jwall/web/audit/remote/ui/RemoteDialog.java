/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.remote.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class RemoteDialog
    extends JDialog
{
    public final static long serialVersionUID = 123L;
    
    public RemoteDialog(){
        setLayout(new BorderLayout());
        setTitle("Remote Event Source");

        JPanel p = new JPanel();
        p.setLayout(null);
        p.setMinimumSize(new Dimension(200, 100));
        p.setMaximumSize(new Dimension(200, 100));
        
        JLabel l = new JLabel("Server");
        l.setBounds(30, 15, 60, 20);
        p.add(l);
        
        l = new JLabel("Login");
        l.setBounds(30, 40, 60, 20);
        p.add(l);
        
        l = new JLabel("Password");
        l.setBounds(30, 65, 60, 20);
        p.add(l);
        
        JTextField s = new JTextField(20);
        s.setBounds(120, 15, 110, 20);
        p.add(s);
        
        JTextField port = new JTextField(4);
        port.setBounds(240, 15, 40, 20);
        p.add(port);
        
        JTextField login = new JTextField(25);
        login.setBounds(120, 40, 161, 20);
        p.add(login);
        
        JPasswordField pass = new JPasswordField();
        pass.setBounds(120, 65, 161, 20);
        p.add(pass);
        
        getContentPane().add( p, BorderLayout.CENTER );
        
        //
        // create the buttons
        //
        JPanel b = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton ok = new JButton("Ok");
        ok.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                System.exit(0);
            }
        });
        
        JButton cl = new JButton("Cancel");
        cl.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                setVisible(false);
                System.exit(0);
            }
        });

        b.add(ok);
        b.add(cl);
        
        getContentPane().add(b, BorderLayout.SOUTH);
        
        setSize(300, 180);
        
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension d = tk.getScreenSize();

        int x = (d.width - this.getWidth()) / 2;
        int y = (d.height - this.getHeight()) /2;

        this.setLocation(x, y);
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        RemoteDialog r = new RemoteDialog();
        r.setVisible(true);
    }
}
