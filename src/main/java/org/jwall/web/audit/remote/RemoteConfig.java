/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.remote;

import java.net.MalformedURLException;
import java.net.URL;

import org.jwall.AuditViewer;

public class RemoteConfig
{
    String server;
    int port;
    URL keystoreFile;
    String password;
    boolean ssl = false;
    boolean enableFilter = false;
    String filterVar = "";
    String filterExp = "";
    
    public RemoteConfig(){
        server = System.getProperty("jwall.remote.server.name");
        try {
            port = Integer.parseInt( System.getProperty("jwall.remote.server.port") );
        } catch (NumberFormatException e) {
            port = 10001;
        }
        
        enableFilter = AuditViewer.getProperty("jwall.remote.server."+server+".filter_variable") != null && AuditViewer.getProperty("jwall.remote.server."+server+".filter_expression") != null;
        if(enableFilter){
            filterVar = AuditViewer.getProperty("jwall.remote.server."+server+".filter_variable");
            filterExp = AuditViewer.getProperty("jwall.remote.server."+server+".filter_expression");
        }
        
        try {
            keystoreFile = RemoteConfig.class.getResource("/org/jwall/core/keystore");
        } catch (Exception e) {
            
        }
        password = "geheim";
    }

    /**
     * @return the login
     */
    public URL getKeystoreFile()
    {
        return keystoreFile;
    }

    /**
     * @param login the login to set
     */
    public void setKeystoreFile(URL login)
        throws MalformedURLException
    {
        keystoreFile = login;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return the port
     */
    public int getPort()
    {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port)
        throws NumberFormatException
    {
        this.port = Integer.parseInt( port );
    }

    /**
     * @return the server
     */
    public String getServer()
    {
        return server;
    }

    /**
     * @param server the server to set
     */
    public void setServer(String server)
    {
        this.server = server;
    }

    /**
     * @return the ssl
     */
    public boolean isSsl()
    {
        return ssl;
    }

    /**
     * @param ssl the ssl to set
     */
    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    /**
     * @return the filterVar
     */
    public String getFilterVar()
    {
        return filterVar;
    }

    /**
     * @param filterVar the filterVar to set
     */
    public void setFilterVar(String filterVar)
    {
        this.filterVar = filterVar;
    }

    /**
     * @return the fitlerExp
     */
    public String getFilterExp()
    {
        return filterExp;
    }

    /**
     * @param fitlerExp the fitlerExp to set
     */
    public void setFilterExp(String fitlerExp)
    {
        this.filterExp = fitlerExp;
    }

    public boolean isFilterEnabled(){
        return enableFilter;
    }
    
    public void setFilterEnabled(boolean b){
        enableFilter = b;
    }
}
