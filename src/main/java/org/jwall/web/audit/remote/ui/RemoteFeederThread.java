/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.remote.ui;

import java.util.LinkedList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.net.NetworkAuditEventSource;

public class RemoteFeederThread extends Thread {
	boolean running = true;
	NetworkAuditEventSource src;
	List<AuditEventListener> listeners = new LinkedList<AuditEventListener>();
	
	public RemoteFeederThread( NetworkAuditEventSource source, AuditEventListener listener ){
		src = source;
		listeners.add( listener );
	}
	
	public void run(){
		
		while( running ){
			
		    while( !src.hasNext() ){
		        try {
		            Thread.sleep(256);
		        } catch (Exception e){
		            e.printStackTrace();
		        }
		    }
		    
		    AuditEvent evt = src.nextEvent();
		    eventArrived( evt );
		}
	}
	
	public void eventArrived( AuditEvent e ){
		for( AuditEventListener l : listeners )
			l.eventArrived( e );
	}
	
	public void connect(){
	}
	
	public void disconnect(){
		running = false;
	}
}
