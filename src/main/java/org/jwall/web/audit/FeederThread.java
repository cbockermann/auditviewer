/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.util.ArrayList;

import org.jwall.AuditViewer;
import org.jwall.app.Task;
import org.jwall.web.audit.io.AuditEventReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FeederThread extends Task {

    static Logger log = LoggerFactory.getLogger(FeederThread.class);

    public final static String STATUS_READ_FILE = "org.jwall.app.file.read";
    public final static String STATUS_PROCESS_FILE = "org.jwall.app.file.read";
    public boolean finished = false;

    AuditEventReader reader = null;
    AuditEventListener sink = null;
    int maxEvents = -1;
    int cnt = 0;
    String status = "";

    public FeederThread(AuditEventReader reader, AuditEventListener sink) {
        this.reader = reader;
        this.sink = sink;
    }

    public FeederThread(AuditEventReader reader, AuditEventListener l, int maxEvents) {
        this(reader, l);
        this.maxEvents = maxEvents;
    }

    public void run() {

        int blockSize = 100;
        ArrayList<AuditEvent> block = new ArrayList<AuditEvent>(blockSize);
        log.info("Reading events, blockSize is '{}'", blockSize);

        status = STATUS_READ_FILE;
        AuditEvent evt = null;

        String lastSuccessfulId = "";

        do {
            try {
                block.clear();
                evt = reader.readNext();
                if (evt != null)
                    lastSuccessfulId = evt.getEventId();

                int read = 0;
                while (evt != null && block.size() < blockSize && cnt < maxEvents) {
                    block.add(evt);
                    evt = reader.readNext();
                    read++;
                    log.debug("{} events read...", read);

                    if (read % 100 == 0)
                        advanced();
                }

                if (block.size() > 0) {
                    for (AuditEvent aevt : block) {
                        sink.eventArrived(aevt);
                    }

                    cnt += block.size();
                }
            } catch (Exception e) {
                log.error("Last successful event read was event with ID = '{}'", lastSuccessfulId);
                evt = null;
                AuditViewer.handleException(e);
                e.printStackTrace();
            }
        } while (cnt < maxEvents && reader.bytesAvailable() > 0 && evt != null);

        status = STATUS_PROCESS_FILE;
        advanced();

        log.debug("Reading events finished.");
        finish();
    }

    public String getStatus() {
        return status;
    }

    public double percentageCompleted() {
        if (finished)
            return 100;

        double d = 0.0d;

        if (maxEvents > 0) {
            double done = (double) cnt;
            double max = (double) maxEvents;
            d = 95 * (done / max);
        } else {
            double done = (double) reader.bytesRead();
            double max = (double) reader.bytesAvailable();
            d = 95 * done / max;
        }

        return d;
    }

    public boolean done() {
        return finished;
    }
}
