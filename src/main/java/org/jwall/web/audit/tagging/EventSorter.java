/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.util.QuotedStringTokenizer;

public class EventSorter
implements AuditEventListener
{
    /** The name of the catch-all cluster */
    public final static String DEFAULT_CLUSTER_NAME = "__catch_all__";
    
    static Logger log = Logger.getLogger( EventSorter.class.getCanonicalName() );
    
    /** The root cluster for this sorter */
    Cluster root = new Cluster( "All" );
    
    /** The default cluster instance */
    Cluster defaultCluster = new Cluster( DEFAULT_CLUSTER_NAME );

    /** The list of tag-rules aka taggers */
    List<EventTagger> taggers = new LinkedList<EventTagger>();
    
    
    /**
     * 
     * Create a new instance of this class with the standard tagger-hierarchie.
     * 
     */
    public EventSorter(){
        root = new Cluster( "All" );
        
        taggers.add( new HSectionEventTagger() );
        /*
        taggers.add( new SeverityTagRule() );
        taggers.add( new IdTagRule() );
        taggers.add( new MessageTagRule() );
        taggers.add( new TagTagRule() );
         */
        //taggers.add( new VariableRule( ModSecurity.REMOTE_ADDR ) );
    }


    public Cluster getRoot(){
        return root;
    }

    public void clearClusters(){
        LinkedList<Cluster> toClear = new LinkedList<Cluster>();
        toClear.add( root );

        while( ! toClear.isEmpty() ){

            Cluster cur = toClear.removeFirst();
            toClear.addAll( cur.getChildren() );
            cur.events.clear();
        }
    }


    public void sort( Collection<AuditEvent> events ){
        for( AuditEvent evt : events )
            eventArrived( evt );
    }


    public void eventArrived(AuditEvent evt)
    {
        List<String> tags = getTagsFor( evt );
        if( tags.isEmpty() ) {
            defaultCluster.add( evt );
            
            if( ! root.children.contains( defaultCluster ) )
                root.add( defaultCluster );
            
        } else {
            
            for( String tag : tags ){
                Cluster c = getClusterFor( tag );
                c.add( evt );
            }
        }
    }

    public void eventsArrived( Collection<AuditEvent> events ){
        for( AuditEvent evt: events )
            eventArrived( evt );
    }


    public void processEvent( String cmd, Collection<AuditEvent> events ){
        sort( events );
    }


    public Cluster getClusterFor( String name ){
        List<String> path = QuotedStringTokenizer.splitRespectQuotes( name, '/' );

        Cluster cur = root;
        for( String pe : path ){
            Cluster next = cur.getChild( pe );
            if( next == null ){
                //if( pe.equals( "" ) )
                //    pe = "/";
                next = new Cluster( pe );
                cur.add( next );
            } 
            
            cur = next;
        }

        return cur;
    }

    public List<String> getTagsFor( AuditEvent evt ){
        LinkedList<String> tags = new LinkedList<String>();
        
        
        
        
        for( EventTagger rule : taggers ){
            List<String> rt = rule.tag( evt );
            
            for( String tag : rt )
                if( ! tags.contains( tag ) )
                    tags.add( tag );
            
            //if( ! rt.isEmpty() )
            //    tags.add( tagString( rt ) );
        }

        //String ts = tagString( tags );
        //LinkedList<String> path = new LinkedList<String>();
        //path.add( ts );
        
        System.out.println("Tags for event " + evt.getEventId() + ": " + tags );
        return tags; 
        //return tagger.tagString( tagger.tag(evt) );
    }
    
    public String tagString( List<String> tags ){
        StringBuffer s = new StringBuffer();
        for( String tag : tags ){
            if( s.length() > 0 )
                s.append( "/" );
            s.append( tag );
        }
        
        return s.toString();
    }
    
    public static void main( String[] args ){
        
        try {
            
            EventSorter sorter = new EventSorter();

            for( int i = 0; i < 1; i++ ){            
                AuditEventReader reader = new ModSecurity2AuditReader( new File( args[0]  ) );
                AuditEvent evt = reader.readNext();

                Integer cnt = 0;
                long start = System.currentTimeMillis();
                while( evt != null ){
                    cnt++;
                    sorter.eventArrived( evt );
                    evt = reader.readNext();
                }
                long end = System.currentTimeMillis();
                log.info( i + ": Sorting " + cnt + " events took " + (end-start) + " ms" );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
