/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.jwall.app.Application;
import org.jwall.app.ui.View;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.injector.Injector;
import org.jwall.web.audit.obfuscator.Obfuscator;
import org.jwall.web.audit.viewer.AuditDataMemory;
import org.jwall.web.audit.viewer.AuditDataTable;
import org.jwall.web.audit.viewer.EventWriter;
import org.jwall.web.audit.viewer.ui.EventTableView;


/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ClusterTreeView
extends View
implements ActionListener, TreeSelectionListener
{
    /** The unique class id */
    private static final long serialVersionUID = 5036257174958887334L;

    ClusterModel treeModel;

    JTree clusterTree;


    JTable eventTable;
    AuditDataTable clusterEvents = new AuditDataTable( new AuditDataMemory() );
    EventTableView tableView;
    EventSorter sorter;
    JScrollPane sp;

    public ClusterTreeView(Application app, ClusterModel clusterModel)
    {
        super(app);

        this.setLayout( new BorderLayout() );

        treeModel = clusterModel;

        clusterTree = new JTree( treeModel );
        clusterTree.setShowsRootHandles( true );
        clusterTree.setRootVisible( true );
        clusterTree.setRowHeight( 34 );
        clusterTree.putClientProperty( "JTree.lineStyle", "Angled" );
        clusterTree.setCellRenderer( new ClusterTreeCellRenderer() );
        clusterTree.addTreeSelectionListener( this );
        sp = new JScrollPane( clusterTree );
        
        tableView = new EventTableView( clusterEvents, "TreeView" );
        tableView.addEventProcessor( new Injector() );
        tableView.addEventProcessor( new Obfuscator() );
        tableView.addEventProcessor( new EventWriter() );
        
        /*
        eventTable = new JTable( clusterEvents );
        eventTable.setBorder( null );
        eventTable.setGridColor( Color.GRAY );
        eventTable.setShowGrid( true );
        eventTable.setRowHeight(20);
        eventTable.setSelectionMode( ListSelectionModel.SINGLE_INTERVAL_SELECTION );
        //eventTable.addMouseListener(this);
        */
        JSplitPane splitPane = new JSplitPane( JSplitPane.VERTICAL_SPLIT, sp, tableView );
        splitPane.setBorder( null );
        splitPane.setDividerLocation(350);

        add( splitPane, BorderLayout.CENTER );
        
        //actions.add( new Action( this, "", null, 1.0d, true ) );
    }


    public void setClusterModel( ClusterModel cm ){
        clusterTree.setModel( cm );
        clusterTree.treeDidChange();
        clusterTree.validate();
    }
    
    
    public void createTree( Collection<AuditEvent> events ){
        clusterTree.treeDidChange();
        clusterTree.doLayout();
        clusterTree.validate();
    }


    public void actionPerformed( ActionEvent e ){

    }


    public void valueChanged(TreeSelectionEvent e)
    {
        if( clusterTree.getSelectionPath() != null ){
            Cluster cluster = (Cluster) clusterTree.getSelectionPath().getLastPathComponent();
            if( cluster != null ){
                clusterEvents.clear();
                clusterEvents.addEvents( cluster.getAllEvents() );
            }
        }
    }
}
