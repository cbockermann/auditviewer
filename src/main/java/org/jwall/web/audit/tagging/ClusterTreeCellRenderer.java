/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;


import java.awt.Component;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.AuditViewer;

public class ClusterTreeCellRenderer
    extends DefaultTreeCellRenderer
{
    /** The unique class id */
    private static final long serialVersionUID = 103656140300102928L;
    
    static Logger log = Logger.getLogger( "ClusterTreeCellRenderer" );
    
    
    
    public ClusterTreeCellRenderer(){
    }
    
    
    /**
     * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row,
            boolean hasFocus)
    {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
                row, hasFocus);
        
        
        if( value instanceof Cluster ){
            Cluster clusterNode = (Cluster) value;
            
            String tag = cleanTag( clusterNode.getTag() );
            String text = tag;
            
            String comment = clusterNode.getComment();
            
            if( comment != null )
                setToolTipText( comment );
            
            if( tag.startsWith( "severity" ) ){
                String severity = tag.substring( "severity \"".length(), tag.length() ).toLowerCase();

                text = severity.toUpperCase();
                
                String iconName = "org.modsecurity.severity." + severity;
                
                ImageIcon icon = AuditViewer.getIcon( iconName );
                if( icon != null ){
                    log.finest( "Setting icon to " + iconName + " => " + AuditViewer.getResource( iconName ) );
                    setIcon( icon );
                }
            } 
            
            if ( tag.startsWith( "msg" ) ){
             
                String msg = AuditViewer.getMessage( "org.modsecurity.alert.message" );
                if( msg != null )
                    text = msg + " " + tag.substring( 4 ); 
                else
                    text = tag;
                
                ImageIcon icon = AuditViewer.getIcon( "org.modsecurity.alert.message" );
                if( icon != null ){
                    setIcon( icon );
                }
            }

            if( tag.startsWith( "id" ) ){
                
                ImageIcon icon = AuditViewer.getIcon( "org.modsecurity.alert.rule" );
                if( icon != null )
                    setIcon( icon );
                
                text = "Rule ID " + tag.substring( 4 );
            }
            
            
            if( clusterNode.size() > 0 )
                text = text + " (" + clusterNode.size() + " events)"; 
            
            setText( text );

            if( tree.getModel().getRoot() == clusterNode ){
                
                ImageIcon icon = AuditViewer.getIcon( "org.jwall.web.audit.tagging.root-node" );
                if(icon != null )
                    setIcon( icon );
            }
            
            
            if( clusterNode.getTag().equals( EventSorter.DEFAULT_CLUSTER_NAME ) ){
                
                String msg = AuditViewer.getMessage( "org.jwall.web.audit.tagging.default-node" );
                if( msg != null )
                    setText( msg );
                
                ImageIcon icon = AuditViewer.getIcon( "org.jwall.web.audit.tagging.default-node" );
                if( icon != null )
                    setIcon( icon );
            }
            return this;
        } else
            log.severe("Shouldn't happen..." );
        
        return this;
    }
    
    
    public String cleanTag( String str ){
        String tag = str;
        if( tag.startsWith("[") )
            tag = tag.substring(1);
        
        if( tag.endsWith( "]" ) )
            tag = tag.substring(0, tag.length() - 1 );

        if( tag.endsWith( "\"" ) )
            tag = tag.substring(0, tag.length() - 1 );
        
        return tag;
    }
}
