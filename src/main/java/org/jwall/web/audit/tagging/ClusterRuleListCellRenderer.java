/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jwall.AuditViewer;

public class ClusterRuleListCellRenderer extends DefaultListCellRenderer {
    private static final long serialVersionUID = -2409041191358129205L;
    ClusterRuleListModel tlm;

    public ClusterRuleListCellRenderer(ClusterRuleListModel lm) {
        tlm = lm;
    }

    /**
     * @see javax.swing.DefaultListCellRenderer#getListCellRendererComponent(javax.swing.JList,
     *      java.lang.Object, int, boolean, boolean)
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        JPanel p = new JPanel(new BorderLayout());

        this.setVerticalTextPosition(JLabel.CENTER);
        this.setIconTextGap(14);
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        p.setBackground(this.getBackground());
        p.setForeground(this.getForeground());

        ImageIcon icon = AuditViewer.getIcon("org.jwall.web.audit.tagging.cluster-rule");

        if (icon != null) {
            // this.setBorder( new EmptyBorder( 20, 0, 0, 0 ) );
            setIcon(icon);
        }

        JLabel i = new JLabel(icon);
        i.setBorder(new EmptyBorder(15, 15, 15, 15));
        i.setOpaque(true);
        i.setBackground(getBackground());
        i.setForeground(getForeground());

        p.add(i, BorderLayout.WEST);

        p.setOpaque(true);

        if (index >= 0 && index < tlm.templateDescriptions.size()) {
            StringBuffer txt = new StringBuffer(
                    "<html><body style=\"font-family: Verdana,Arial,Sansserif; font-size:11pt;\">");
            txt.append(tlm.templateDescriptions.elementAt(index));
            txt.append("</body></html>");
            // setText( txt.toString() );

            JEditorPane ep = new JEditorPane();
            ep.setContentType("text/html");
            ep.setText(txt.toString());
            ep.setEditable(false);
            ep.setBorder(new EmptyBorder(5, 5, 5, 5));

            ep.setForeground(this.getForeground());
            ep.setBackground(this.getBackground());

            JLabel text = new JLabel(txt.toString());
            text.setAlignmentY(Component.CENTER_ALIGNMENT);
            text.setVerticalTextPosition(JLabel.TOP);
            text.setBorder(null); // new EmptyBorder( 5, 5, 5, 5 ) );

            p.add(ep, BorderLayout.CENTER);
        }
        return p;

        // return this;
    }
}
