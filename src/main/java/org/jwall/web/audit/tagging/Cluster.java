/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import org.jwall.web.audit.AuditEvent;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


@XStreamAlias("Events")
public class Cluster
    implements Comparable<Cluster>
{
    @XStreamAlias("tag")
    @XStreamAsAttribute
    String tag;

    @XStreamAlias("Comment")
    String comment = null;
    
    @XStreamImplicit
    LinkedList<Cluster> children = new LinkedList<Cluster>();
    
    transient Collection<AuditEvent> events = new Vector<AuditEvent>();

    transient Map<String,Double> statistics = new HashMap<String,Double>();
    

    final static String[] SEVERITY_ORDER = new String[]{ 
            "emergency",
            "alert",
            "critical",
            "error",
            "warning",
            "notice",
            "info",
            "debug"
    };

    
    
    public Cluster( String tag ){
        this.tag = tag;
        events = new Vector<AuditEvent>();
        children = new LinkedList<Cluster>();
    }
    
    public void setTag( String tag ){
        this.tag = tag;
    }
    
    public String getTag(){
        return tag;
    }
    
    public void setComment( String desc ){
        comment = desc;
    }
    
    public String getComment(){
        return comment;
    }
    
    public Collection<AuditEvent> getEvents(){
        return events;
    }
    
    public Collection<AuditEvent> getAllEvents(){
        Collection<AuditEvent> all = new TreeSet<AuditEvent>( events );
        
        for( Cluster c : children )
            all.addAll( c.getAllEvents() );
        
        return all;
    }
    
    public void add( AuditEvent evt ){
        events.add( evt );
    }
    
    
    public void add( Cluster c ){
        children.add( 0, c );
        
        Collections.sort( children );
    }
    
    
    public List<Cluster> getChildren(){
        return children;
    }
    
    
    public Cluster getChild( String name ){
        for( Cluster ch : children )
            if( ch.tag.equals( name ) )
                return ch;
        
        return null;
    }
    
    
    public Set<String> getTagNames(){
        TreeSet<String> tagNames = new TreeSet<String>();
        
        tagNames.add( tag );

        for( Cluster ch : children ){
            Set<String> names = ch.getTagNames();
            tagNames.addAll( names );
        }
        
        return tagNames;
    }

    
    public int size(){
        
        int size = events.size();
        for( Cluster ch : children )
            size += ch.size();
        
        return size;
    }
    
    
    public int compareTo( Cluster other ){
        
        if( this == other || this.tag.equals( other.tag ) )
            return 0;

        if( this.tag.equals( EventSorter.DEFAULT_CLUSTER_NAME ) )
            return 1;
        
        if( other.tag.equals( EventSorter.DEFAULT_CLUSTER_NAME ) )
            return -1;
        
        if( this.tag.indexOf( "severity" ) >= 0 ){
            
            if(other.tag.indexOf( "severity" ) >= 0 )
                return severity2double( tag ).compareTo( severity2double( other.tag ) );
            else
                return -1;
        }
        
        if( other.tag.startsWith( "[severity" ) )
            return 1;
        
        return this.tag.compareTo( other.tag );
    }
    
    public String toString(){
        return tag;
    }
    
    
    /**
     * 0 - EMERGENCY
     * 1 - ALERT
     * 2 - CRITICAL
     * 3 - ERROR
     * 4 - WARNING
     * 5 - NOTICE
     * 6 - INFO
     * 7 - DEBUG
     * 
     * @param severity
     * @return
     */
    public static Double severity2double( String severity ){
        String level = severity.toLowerCase();
        
        for( int i = 0; i < SEVERITY_ORDER.length; i++ ){
            if( level.indexOf( SEVERITY_ORDER[i] ) >= 0 )
                return new Double( i );
        }

        return Double.MAX_VALUE;
    }
}
