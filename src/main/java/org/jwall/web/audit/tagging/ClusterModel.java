/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.util.LinkedList;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class ClusterModel implements TreeModel {
    /** The unique class id */
    public static final long serialVersionUID = 4475483852928645460L;

    /** The root node of this cluster tree */
    Cluster root;

    /* The list of model-listeners */
    LinkedList<TreeModelListener> listener = new LinkedList<TreeModelListener>();

    public ClusterModel(Cluster top) {
        root = top;
    }

    public Object getChild(Object parent, int index) {
        Cluster p = (Cluster) parent;
        Cluster ch = p.getChildren().get(index);
        // System.out.println( "getChild(" + p + ", " + index + ") = " + ch );
        return ch;
    }

    public int getChildCount(Object parent) {
        Cluster p = (Cluster) parent;
        // System.out.println( "getChildCount(" + p.getTag() + ") = " +
        // p.getChildren().size() );
        return p.getChildren().size();
    }

    public int getIndexOfChild(Object parent, Object child) {
        Cluster p = (Cluster) parent;
        int idx = p.getChildren().indexOf(child);
        // System.out.println( "getIndexOfChild( " + p.getTag() + ", " + child +
        // ") = " + idx );
        return idx;
    }

    public Object getRoot() {
        // System.out.println( "getRoot() = " + root );
        return root;
    }

    public boolean isLeaf(Object node) {
        Cluster c = (Cluster) node;
        return c.getChildren().isEmpty();
    }

    public void addTreeModelListener(TreeModelListener l) {
        listener.add(l);
    }

    public void removeTreeModelListener(TreeModelListener l) {
        listener.remove(l);
    }

    public void valueForPathChanged(TreePath path, Object newValue) {

        for (TreeModelListener l : listener)
            l.treeStructureChanged(new TreeModelEvent(newValue, path));
    }
}
