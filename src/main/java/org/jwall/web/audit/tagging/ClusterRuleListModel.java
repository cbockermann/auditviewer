/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class ClusterRuleListModel implements ListModel<String> {

    List<ListDataListener> listener = new LinkedList<ListDataListener>();
    Vector<String> clusterRule = new Vector<String>();
    Vector<String> templateDescriptions = new Vector<String>();

    public ClusterRuleListModel() {
    }

    public String getElementAt(int index) {
        return clusterRule.elementAt(index);
    }

    public int getSize() {
        return clusterRule.size();
    }

    public void addListDataListener(ListDataListener l) {
        listener.add(l);
    }

    public void removeListDataListener(ListDataListener l) {
        listener.remove(l);
    }
}
