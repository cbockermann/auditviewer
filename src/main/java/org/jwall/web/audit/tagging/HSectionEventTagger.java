/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.tagging.rules.IdTagRule;
import org.jwall.web.audit.tagging.rules.SeverityTagRule;
import org.jwall.web.audit.tagging.rules.TagTagRule;
import org.jwall.web.audit.tagging.rules.VariableRule;

public class HSectionEventTagger implements EventTagger {
	Pattern p = Pattern.compile("\\[(msg|id|severity|tag)\\s[^\\]]*\\]");

	public HSectionEventTagger() {
	}

	public HSectionEventTagger(String labels) {
		p = Pattern.compile("\\[" + labels + "\\s[^\\]]*\\]");
	}

	public List<String> tag(AuditEvent evt) {
		LinkedList<String> tags = new LinkedList<String>();

		String msgSection = evt.getSection(ModSecurity.SECTION_AUDIT_TRAILER);

		if (msgSection == null)
			return tags;

		String[] lines = msgSection.split("\\n");

		for (String line : lines) {
			if (line.startsWith("Message:")) {

				Matcher m = p.matcher(line);
				int st = 0;
				LinkedList<String> tagList = new LinkedList<String>();
				while (st < line.length() && m.find(st)) {

					String tag = line.substring(m.start() + 1, m.end() - 1);
					tagList.add(tag);
					st = m.end();
				}

				tags.add(tagString(tagList));
			}
		}
		System.out.println("Tag-list for event " + evt.getEventId() + ": "
				+ tags);
		return tags;
	}

	public String tagString(List<String> tags) {
		StringBuffer s = new StringBuffer();
		for (String tag : tags) {

			if (tag.startsWith("severity")) {
				s.insert(0, "" + tag);
			} else {
				s.append("/");
				s.append(tag);
			}
		}

		return s.toString();
	}

	public static void main(String args[]) {

		try {
			EventTagger tagger = new HSectionEventTagger();

			List<EventTagger> rules = new LinkedList<EventTagger>();
			rules.add(new SeverityTagRule());
			rules.add(new IdTagRule());
			rules.add(new TagTagRule());

			List<EventTagger> rules2 = new LinkedList<EventTagger>();
			rules2.add(new VariableRule(ModSecurity.REMOTE_ADDR));
			rules2.add(new TagTagRule());
			rules2.add(new SeverityTagRule());
			rules2.add(new IdTagRule());

			for (int i = 0; i < 1; i++) {
				AuditEventReader reader = new ModSecurity2AuditReader(new File(
						args[0]));
				AuditEvent evt = reader.readNext();

				Integer cnt = 0;
				long start = System.currentTimeMillis();
				while (evt != null && cnt < 10) {
					cnt++;
					List<String> tags = tagger.tag(evt);

					System.out
							.println("Alert on "
									+ evt.get(ModSecurity.REQUEST_URI)
									+ " tagged as: ");

					for (String tag : tags)
						System.out.println("\t" + tag);

					for (EventTagger rule : rules) {

						List<String> rtags = rule.tag(evt);
						System.out.print("\ttags by \"" + rule + "\": ");
						for (String t : rtags) {
							System.out.print(" " + t);
						}
						System.out.println();
					}

					System.out.println("----2nd-rule-set----");

					for (EventTagger rule : rules2) {

						List<String> rtags = rule.tag(evt);
						System.out.print("\ttags by \"" + rule + "\": ");
						for (String t : rtags) {
							System.out.print(" " + t);
						}
						System.out.println();
					}

					evt = reader.readNext();
				}
				long end = System.currentTimeMillis();
				System.out.println(i + ": Tagging " + cnt + " events took "
						+ (end - start) + " ms");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
