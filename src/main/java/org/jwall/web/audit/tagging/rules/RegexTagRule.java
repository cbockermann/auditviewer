/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.tagging.rules;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.tagging.EventTagger;

public abstract class RegexTagRule
    implements EventTagger
{
    String exp;
    Pattern pattern;

    public RegexTagRule( String exp ){
        this.exp = exp;
        pattern = Pattern.compile( this.exp );
    }

    
    public List<String> tag(AuditEvent evt)
    {
        LinkedList<String> list = new LinkedList<String>();
        
        String str = getRegion( evt );
        if( str == null )
            return list;
        
        Matcher m = pattern.matcher( str );
        int i = 0;
        while( i < str.length() && m.find( i ) ){
            list.add( trim( str.substring( m.start(), m.end() ) ) );
            i = m.end() + 1;
        }
        
        return list;
    }
    
    public String trim( String tag ){
        return tag;
    }
    
    public abstract String getRegion( AuditEvent evt );
}
