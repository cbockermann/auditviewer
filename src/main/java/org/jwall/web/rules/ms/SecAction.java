/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules.ms;

import java.util.LinkedList;
import java.util.List;

import org.jwall.web.rules.Rule;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("action")
public class SecAction
    implements Rule
{

    @XStreamAsAttribute
    private String id;
    
    @XStreamAsAttribute
    private String phase;
    
    @XStreamAlias("actions")
    protected Actions actions;
    
    @XStreamImplicit(itemFieldName = "tag")
    protected LinkedList<String> tags;
    
    protected transient SecRule parent = null;
    
    @XStreamAlias("chain")
    @XStreamImplicit
    protected LinkedList<SecAction> chain;
    
    @XStreamAlias("comment")
    private String comment = null;

    
    public SecAction()
    {
        super();
    }

    /**
     * @return the phase
     */
    public String getPhase()
    {
        return phase;
    }

    /**
     * @param phase the phase to set
     */
    public void setPhase(String phase)
    {
        this.phase = phase;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id)
    {
        this.id = id;
    }

    /**
     * @return the actions
     */
    public List<Action> getActions()
    {
        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(LinkedList<Action> actionList)
    {
        actions.clear();
        
        for( Action a : actionList ){
            
            if( a.getName().equals( "phase" ) ){
                phase = a.getValue();
                continue;
            }
            
            if( a.getName().equals( "id") ){
                id = a.getValue();
                continue;
            }
            
            actions.add( a );
        }
    }

    public List<String> getTags()
    {
        return tags;
    }

    public void setTags(LinkedList<String> tags)
    {
        this.tags = tags;
    }

    public void setComment(String com)
    {
        comment = com;
    }

    public String getComment()
    {
        return comment;
    }
}
