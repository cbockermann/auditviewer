/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules.ms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.jwall.web.audit.util.QuotedStringTokenizer;


/**
 * 
 * This class implements a parser which reads ModSecurity rules.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class SecRuleParser
{
    static Logger log = Logger.getLogger("SecRuleParser");
    LinkedList<Action> defaultActions = new LinkedList<Action>();
    private boolean relativeLocations = true;
    SecRule last = null;
    
    public SecRuleParser(){
        defaultActions = parseActions( "phase:2,log,auditlog,pass" );
    }


    public void setDefaultAction( String s ){
        defaultActions = parseActions( s );
        for( Action a : defaultActions )
        	a.setInherited( true );
    }


    private static LinkedList<Action> parseActions( String s ){
        LinkedList<Action> actions = new LinkedList<Action>();

        String str = s.trim();
        if( str.startsWith( "\"" ) )
            str = str.substring( 1 );

        if( str.endsWith( "\"" ) )
            str = str.substring( 0, str.length() - 1 );


        log.fine("actionString: " + str );
        List<String> a = QuotedStringTokenizer.splitRespectQuotes( str, ',' );
        for( String action : a ){
            log.fine("   Action = " + action );
            String st = action.replaceAll( "'", "" ).trim();

            Action act = null;
            if( st.indexOf(":") > 0 ){
                String[] tok = st.split(":", 2);
                act = new Action( tok[0], tok[1] );
            } else
                act = new Action( st, null );


            log.fine("Adding action " + act );
            actions.add( act );
        }
        
        log.fine("Parsed "+actions.size()+" actions");
        return actions;
    }



    public SecRule parseFromString( String ruleString ){

        //System.out.println("parsing SecRule from >"+ruleString+"<");

        if(! ruleString.trim().startsWith( "SecRule" ) )
            return null;


        List<String> tok = QuotedStringTokenizer.splitRespectQuotes( ruleString.trim(), ' ' );

        //for( int i = 0; i < tok.length; i++ )
        //   System.out.println( "part["+i+"] =  " + tok[i] );


        if( tok.size() == 4 ){
            SecRule rule = new SecRule( tok.get(1), tok.get(2) );
            
            if( last == null )
                rule.setActions( defaultActions );

            List<Action> actions = parseActions( tok.get(3) );
            for( Action a : actions ){
            	
            	LinkedList<Action> overwrite = new LinkedList<Action>();
            	for( Action old : rule.getActions() ){
            	    if( old.getName().equals( a.getName() ) && (old.getValue() == null || old.getValue().equals( a.getValue() ) )){
            	        log.fine("Removing action " + old + " which will be overwritten by " + a);
            			overwrite.add( old );
            		}
            	}
            	
            	rule.getActions().removeAll( overwrite );
            	
            	rule.add( a );
            }
            
            if( last != null && last.isChained() ){
                rule.setPhase( "" );
                rule.setId( "" );
            }
            
            return rule;
        }

        return null;
    }
    
    /**
     * 
     * This method parses the given file for all ModSecurity-related configuration
     * directives. If an &quot;Include&quot; directive is found, the file matching
     * this directive is parsed recursively.
     * 
     * @param f The file to be parsed.
     * @return An list of rules, ordered in the way they are found within the config. 
     * @throws Exception
     */
    public List<SecRule> parse( URL url ) throws Exception {
        List<SecRule> rules = new LinkedList<SecRule>();

        BufferedReader r = new BufferedReader( new InputStreamReader( url.openStream() ) );
        String line = r.readLine();
        int lineNr = 1;

        StringBuffer comment = new StringBuffer();

        while( line != null ) {
            
            if(! line.trim().startsWith("#") ){
                log.fine(" line: " + line.trim() );
            } else 
                if( !line.trim().equals("#") ){
                    if( comment.length() > 0 )
                        comment.append( "\n" );
                    comment.append( line.trim().substring( 1 ) );
                }

            if( line.trim().equals( "" ) )
                comment = new StringBuffer();

            if( line.trim().startsWith( "SecDefaultAction" ) ){
                log.fine( "SecDefaultAction:  " + line.trim() );
                String[] tok = line.trim().split("\\s+");
                for( int i = 0; i < tok.length; i++ ){
                    log.fine("  part[" + i + "] = " + tok[i] );
                    defaultActions = parseActions( tok[1] );
                }

                comment = new StringBuffer();
            }

            if( line.trim().startsWith("SecRule ") ){
                String loc = url.toString() + ":" + lineNr;
                StringBuffer b = new StringBuffer();

                if( line.trim().endsWith( "\\" ) ){
                    String l = line.trim();
                    l = l.substring(0, l.length() - 1 );
                    b.append( l );
                } else
                    b.append( line );

                while( line != null && line.trim().endsWith("\\") ){
                    line = r.readLine();
                    lineNr++;
                    if( line != null ){
                        String l = line.trim();

                        if( line.trim().endsWith( "\\" ) )
                            l = l.substring(0, l.length() - 1 );

                        //String l = line.trim().substring( 0, line.trim().length() - 1 ) + " ";
                        //l = l.replaceAll( "\'", "\"" );
                        b.append( l + " " );
                    }
                }

                SecRule rule = parseFromString( b.toString() );
                log.fine( "rule = " + rule );
                if( rule != null && rule.isChained() ){
                    log.fine("rule is chained!");
                }

                if( rule != null ){
                    if( relativeLocations )
                        loc = "$CORE_RULES" + loc.substring( url.toString().length() ); //f.getParent().length() );
                    rule.setLocation( loc );

                    if( comment != null && comment.length() > 0 )
                        rule.setComment( comment.toString() );
                    comment = new StringBuffer();

                    if( last != null ){
                        last.addRule( rule );
                    } else {
                        if( ! rules.contains( rule ) )
                            rules.add( rule );
                    }
                    
                    if( rule.isChained() )
                        last = rule;
                    else
                        last = null;
                }
            }

            line = r.readLine();
            lineNr++;
        }

        return rules;
    }
    
    
    public static void main( String[] args ){
        
        try {
            /*
            if( args.length != 1 ){
                System.out.println("Usage:    java org.jwall.tools.SecRuleParser  /path/to/httpd.conf\n");
                System.exit(-1);
            }
            */
            
            SecRuleParser parser = new SecRuleParser();
            List<SecRule> rules = parser.parse( new URL( "http://192.168.10.14/rules/modsecurity_crs_10_config.conf" ) ); 
            
            for( SecRule rule: rules ){
                System.out.println( rule );
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
