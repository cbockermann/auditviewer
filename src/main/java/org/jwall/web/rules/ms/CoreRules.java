/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules.ms;

import java.util.Collection;
import java.util.Vector;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.io.xml.DomDriver;

@XStreamAlias("core-rules")
public class CoreRules
{
    @XStreamAlias("rules")
    @XStreamImplicit
    private Vector<SecRule> rules = new Vector<SecRule>();
    
    @XStreamAsAttribute
    private String revision;
    
    public CoreRules(){
    }

    public void setRevision( String rev ){
        revision = rev;
    }
    
    public String getRevision(){
        return revision;
    }
    
    public Collection<SecRule> getRules(){
        return rules;
    }
    
    public void addAll( Collection<SecRule> rulz ){
        for( SecRule r : rulz )
            if( ! rules.contains( r ) )
                rules.add( r );
    }
    
    public void setRules( Collection<SecRule> rulz ){
        rules.clear();
        rules.addAll( rulz );
    }
    
    
    public static XStream getXStream(){
        XStream xstream = new XStream( new DomDriver() );
        
        xstream.registerConverter( new ActionsConverter( xstream.getMapper() ) );
        xstream.processAnnotations( Actions.class );
        xstream.processAnnotations( SecRule.class );
        xstream.processAnnotations( CoreRules.class );

        return xstream;
    }
}
