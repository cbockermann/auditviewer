/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules.ms;

import java.util.LinkedList;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.collections.CollectionConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

public class ActionsConverter
    extends CollectionConverter
{

    
    public ActionsConverter( Mapper mapper ){
        super( mapper );
    }
    
    
    /* (non-Javadoc)
     * @see com.thoughtworks.xstream.converters.collections.AbstractCollectionConverter#canConvert(java.lang.Class)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean canConvert(Class clazz)
    {
        return clazz.equals( Actions.class );
    }


    /* (non-Javadoc)
     * @see com.thoughtworks.xstream.converters.collections.AbstractCollectionConverter#readItem(com.thoughtworks.xstream.io.HierarchicalStreamReader, com.thoughtworks.xstream.converters.UnmarshallingContext, java.lang.Object)
     */
    @Override
    protected Object readItem(HierarchicalStreamReader reader,
            UnmarshallingContext ctx, Object current)
    {
        String name = reader.getNodeName();
        String value = reader.getValue();
        return new Action( name, value );
    }

    /* (non-Javadoc)
     * @see com.thoughtworks.xstream.converters.collections.AbstractCollectionConverter#writeItem(java.lang.Object, com.thoughtworks.xstream.converters.MarshallingContext, com.thoughtworks.xstream.io.HierarchicalStreamWriter)
     */
    @Override
    protected void writeItem(Object item, MarshallingContext ctx,
            HierarchicalStreamWriter writer)
    {
        if( item != null ){
            
            Action action = (Action) item;
            writer.startNode( action.getName() );
            if( action.getValue() != null )
                writer.setValue( action.getValue() );
            writer.endNode();
        }
    }

    public static void main( String[] args ){
        
        
        LinkedList<Action> as = new LinkedList<Action>();
        as.add( new Action("msg", "test") );
        as.add( new Action( "t", "none" ) );
        as.add( new Action( "t", "toLower" ) );
        
        SecAction rule = new SecRule("HTTP_HEADER:X", "bla", as );
        
        System.out.println(CoreRules.getXStream().toXML( rule ));
    }
}
