/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules.ms;

import java.util.LinkedList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * Instance of this class represent a simple ModSecurity rule. These can also contain
 * nested - or <i>chained</i> - rules.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("rule")
public class SecRule
extends SecAction
{
	@XStreamAlias("location")
	@XStreamAsAttribute
	private String location = "";
	
    @XStreamAlias("selector")
    private String selector;

    @XStreamAlias("operator")
    private String operator;

    /**
     * 
     * 
     * @param selector
     * @param pattern
     * @param action
     */
    public SecRule( String sel, String op, List<Action> action ){
        this( sel, op );
        actions = new Actions();
        actions.addAll( action );
    }
    
    
    public SecRule( String sel, String op ){
        selector = sel;
        operator = op;
        if( operator.startsWith("\"") || operator.startsWith( "'" ) )
            operator = operator.substring( 1 );
        
        if( operator.endsWith( "\"" ) || operator.endsWith( "'" ) )
            operator = operator.substring( 0, operator.length() - 1 );
        
        chain = new LinkedList<SecAction>();
        tags = new LinkedList<String>();
        actions = new Actions();
    }

    
    public void add( Action action ){
    	
    	if( action.getName().equals("id") && action.getValue() != null ){
    		setId( action.getValue() );
    		return;
    	}
    	
    	if( action.getName().equals("phase") && action.getValue() != null ){
    		setPhase( action.getValue() );
    		return;
    	}
    	
    	actions.add( action );
    }

    
    public void setLocation( String loc ){
    	location = loc;
    }
    
    public String getLocation(){
    	return location;
    }

    /**
     * @return the selector
     */
    public String getSelector()
    {
        return selector;
    }

    /**
     * @param selector the selector to set
     */
    public void setSelector(String selector)
    {
        this.selector = selector;
    }

    /**
     * @return the operator
     */
    public String getOperator()
    {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator)
    {
        this.operator = operator;
    }

    public void addRule( SecRule rule ){
        chain.add( rule );
        rule.parent = this;
    }
    
    public SecRule getParent(){
        return parent;
    }
    
    
    public List<SecAction> getChainedRules(){
        return chain;
    }
    
    public boolean equals( Object o ){
        if( ! (o instanceof SecRule ) )
            return false;
        
        if( this == o )
            return true;
        
        SecRule other = (SecRule) o;
        return other.location.equals( location );
    }
    
    public boolean isChained(){
        for( Action action : actions )
            if( action.getName().toLowerCase().equals( "chain" ) )
                return true;
            
        return false;
    }
}
