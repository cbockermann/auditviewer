/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules.ms;

public class Action
{
    
    public static String[] ACTION_NAMES = {
        "allow", "append", "auditlog", "block","capture", "chain",
        "ctl", "deny", "deprecatevar", "drop", "exec", "expirevar",
        "id", "initcol", "log", "logdata", "msg", "multiMatch", "noauditlog",
        "nolog", "pass", "pause", "phase", "prepend", "proxy", "redirect",
        "rev", "sanitiseArg", "sanitiseMatched", "sanitiseRequestHeader",
        "sanitiseResponseHeader", "severity", "setuid", "setsid", "setenv",
        "setvar", "skip", "skipAfter", "status", "t", "xmlns"
    };
    
    public static String[] META_ACTIONS = {
        "id", "rev", "severity"
    };
    
    private String name;
    private String value;
    private boolean inherited = false;
    
    public Action( String name, String val ){
        this.name = name;
        this.value = val;
    }
    
    public void setName( String n ){
        name = n;
    }
    
    public String getName(){
        return name;
    }
    
    public void setValue( String val ){
        this.value = val;
    }
    
    public String getValue(){
        return value;
    }
    
    public void setInherited( boolean b ){
    	inherited = b;
    }
    
    public boolean isInherited(){
    	return inherited;
    }
    
    public String toString(){
        if( value == null )
            return name;
        
        return "#"+name + ">" + value + "</" + name + "#";
    }
    
    public static boolean isMetaAction( String act ){
        for( String ma : META_ACTIONS )
            if( ma.equals( act ) )
                return true;
        return false;
    }
}
