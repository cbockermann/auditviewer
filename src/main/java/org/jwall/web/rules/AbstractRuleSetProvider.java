/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * An abstract implementation of the rule set provider interface, which is based on
 * linked lists for managing rules and nested providers.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public abstract class AbstractRuleSetProvider
    implements RuleSetProvider
{
    
    /** The rules provided by this provider */
    LinkedList<Rule> rules = new LinkedList<Rule>();
    
    /** A list of nested providers */
    LinkedList<RuleSetProvider> nested = new LinkedList<RuleSetProvider>();

    
    
    public abstract URL getLocation();
    
    
    /**
     * @see org.jwall.web.rules.RuleSetProvider#getAllRules()
     */
    public List<Rule> getAllRules()
    {
        LinkedList<Rule> allRules = new LinkedList<Rule>();
        allRules.addAll( rules );
        
        for( RuleSetProvider ch : nested )
            allRules.addAll( ch.getAllRules() );
        
        return allRules;
    }

    
    /**
     * @see org.jwall.web.rules.RuleSetProvider#getNested()
     */
    public List<RuleSetProvider> getNested()
    {
        return nested;
    }

    
    /**
     * @see org.jwall.web.rules.RuleSetProvider#getRules()
     */
    public List<Rule> getRules()
    {
        return rules;
    }
}
