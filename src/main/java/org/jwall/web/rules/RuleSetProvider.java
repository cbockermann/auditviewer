/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.rules;

import java.net.URL;
import java.util.List;


/**
 * 
 * This interface defines an abstract instance which provides a rule set. Implementing
 * instances can provide rule sets using a simple web-server interface or file based
 * storage of rules.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface RuleSetProvider
{
    
    /**
     * This method returns the URL of this rule set provider. This will for instance return
     * a file-URL for a file-based provider or a HTTP URL for a provider, that is accessible
     * via the web.
     * 
     * @return The URL used to access this provider.
     */
    public URL getLocation();        
    

    /**
     * Returns all rules of this provider.
     * 
     * @return A list of rules, provided by this instance.
     */
    public List<Rule> getRules();
    
    
    /**
     * Returns a list of all rules of this provider, including the rules of all nested
     * providers.
     * 
     * @return The complete list of rules.
     */
    public List<Rule> getAllRules();
    
    
    /**
     * Return a list of nested rule set providers. This can e.g. be file-based providers,
     * which are referenced within a parent file.
     * 
     * @return
     */
    public List<RuleSetProvider> getNested();
}
