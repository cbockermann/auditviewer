package org.jwall.security.cert.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.cert.X509Certificate;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.jwall.app.Application;
import org.jwall.app.ui.Dialog;
import org.jwall.app.ui.ImagePanel;
import org.jwall.app.ui.InputDialog;


/**
 * 
 * This dialog class prompts the users for validating a specific certificate chain which
 * can not be validated by the already known issuer certificates.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class CertificateValidationDialog
    extends Dialog
    implements TreeSelectionListener
{
    /* A unique class-id */
    private static final long serialVersionUID = 7628738223759855695L;

    /** The panel for displaying a specific certificate */
    CertificatePanel certPanel;
    
    /** The certificate chain waiting for approval */
    X509Certificate[] certificates;

    /** A flag indicating whether the user approved the chain or not */
    boolean chainApproved = false;

    /** One flag for each certificate that shall be permanently approved */
    boolean permanentApproved = false;
    
    /**
     * Create a new, modal dialog presenting the certificates of the given chain.
     * 
     * @param certs The certificate chain to present to the user.
     */
    public CertificateValidationDialog( X509Certificate[] certs ){
        super();
        setModal( true );
        //setResizable( false );
        
        certificates = certs;
        certPanel = new CertificatePanel();

        this.getContentPane().setLayout( new BorderLayout() );
        
        ImagePanel i = new ImagePanel(InputDialog.class.getResource("/icons/logo_tall_60x513.jpg"), 0, 0, 60, 513);
        i.setHeight( 513 );
        i.setWidth( 60 );
        i.setBackground( Color.BLACK );
        this.getContentPane().add( i, BorderLayout.WEST );
        
        
        JEditorPane msg = new JEditorPane();
        msg.setBackground( Color.WHITE );
        msg.setFont( msg.getFont().deriveFont( Font.PLAIN ) );
        JPanel info = new JPanel( new BorderLayout() );
        info.setBackground( Color.WHITE );
        //info.add( new ImagePanel( CertificateValidationDialog.class.getResource("/icons/24x24/stock_certificate.png"), 0, 0, 24, 24 ), BorderLayout.WEST );
        
        JLabel l = new JLabel();
        l.setAlignmentX( Component.CENTER_ALIGNMENT );
        l.setAlignmentY( Component.CENTER_ALIGNMENT );
        l.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
        l.setIcon( Application.getIcon("org.jwall.security.cert.certificate") );
        info.add(  l, BorderLayout.EAST );
        info.add( msg, BorderLayout.CENTER );
        msg.setContentType( "text/html" );
        msg.setText( "<html><body style=\"font-family: Arial,Verdana,Sansserif; font-size:12pt;\">"
                + "The certificate could not be validated using the known CA certs. Please have a look at "
                + "the certificate chain and validate this chain manually."
                + "</body></html>" );
        msg.setEditable( false );
        msg.setBorder( new EmptyBorder( 25, 15, 15, 15 ) );
        
        JPanel p = new JPanel( new BorderLayout() );
        p.add( info, BorderLayout.NORTH );
        
        CertificateChainPanel chainPanel = new CertificateChainPanel( certs );
        chainPanel.addTreeSelectionListener( this );
        
        JSplitPane c = new JSplitPane( JSplitPane.VERTICAL_SPLIT, chainPanel, certPanel );
        p.add( c, BorderLayout.CENTER );
        
        this.getContentPane().add( p, BorderLayout.CENTER );
        
        if( certs.length > 0 )
            certPanel.setCertificate( certs[ certs.length - 1 ] );
        
        
        JPanel buttons = new JPanel( new FlowLayout( FlowLayout.RIGHT ) );
        
        JButton ok = new JButton("Trust");
        ok.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                chainApproved = true;
                setVisible( false );
            }
        });
        
        JButton permanent = new JButton("Trust permanently");
        permanent.addActionListener( new ActionListener(){
            public void actionPerformed( ActionEvent e ) {
                chainApproved = true;
                permanentApproved = true;
                setVisible( false );
            }
        });
        
        JButton cancel = new JButton("Cancel");
        cancel.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                chainApproved = false;
                setVisible( false );
            }
        });
        
        buttons.add( ok );
        buttons.add( permanent );
        buttons.add( cancel );
        this.getContentPane().add( buttons, BorderLayout.SOUTH );
        
        setSize( 600, 400 );
        this.center();
    }
    

    /**
     * This method simply returns whether the user approved the chain or not.

     * @return
     */
    public boolean isChainApproved(){
        return chainApproved;
    }

    
    public boolean isApprovedPermanently(){
        return permanentApproved;
    }
    

    /**
     * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
     */
    public void valueChanged(TreeSelectionEvent e)
    {  
        if( e.getPath().getLastPathComponent() != null ){
            X509Certificate cert = (X509Certificate) e.getPath().getLastPathComponent();
            certPanel.setCertificate( cert );
        }
    }
}