package org.jwall.security.cert.ui;

import java.awt.BorderLayout;
import java.awt.Font;

import java.security.cert.X509Certificate;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * 
 * This panel display the contents of a X509Certificate.
 * 
 * TODO: Currently it simply display the raw text-form of the certificate.
 *       It shouldn't be too hard to convert this into nice HTML and display
 *       the HTML instead.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class CertificatePanel
    extends JPanel
{
    /* A unique class id */
    private static final long serialVersionUID = 7877050608942399071L;

    /** The certificate to display */
    X509Certificate cert;
    
    /** The text-area used for displaying */
    JTextArea textarea;
    
    
    /**
     * Create a empty panel.
     */
    public CertificatePanel(){

        this.setLayout( new BorderLayout() );
        
        textarea = new JTextArea();
        textarea.setEditable( false );
        textarea.setFont( Font.getFont("Monospaced") );
        
        JScrollPane scroller = new JScrollPane( textarea );
        this.add( scroller, BorderLayout.CENTER );
    }
    
    

    /**
     * This method sets the certificate to be displayed and updates all
     * UI elements.
     * 
     * @param certificate The certificate to be displayed.
     */
    public void setCertificate( X509Certificate certificate ){
        this.cert = certificate;
        textarea.setText( cert.toString() );
    }
 
    
    /**
     * Returns the certificate currently displayed within this panel or
     * <code>null</code> if no certificate is being displayed.
     * 
     * @return The certificate currently displayed.
     */
    public X509Certificate getCertificate(){
        return cert;
    }
}