package org.jwall.security.cert.ui;

import java.security.cert.X509Certificate;
import java.util.LinkedList;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;


/**
 * 
 * This class implements a simple list (array) of certificates organized as a
 * linked-list tree. This is solely used to display the chain within a JTree instance.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class CertificateChainTreeModel
    implements TreeModel
{
    LinkedList<TreeModelListener> listener = new LinkedList<TreeModelListener>();
    X509Certificate[] chain;

    
    public CertificateChainTreeModel( X509Certificate[] chain ){
        this.chain = chain;
    }
    

    public void addTreeModelListener(TreeModelListener l)
    {
        listener.add( l );
    }

    public Object getChild(Object parent, int index)
    {
        for( int i = chain.length - 1; i > 0; i-- ){
            if( chain[i] == parent )
                return chain[ i - 1 ];
        }
        
        return null;
    }

    public int getChildCount(Object parent)
    {
        if( chain[ 0 ] == parent )
            return 0;
        
        return 1;
    }

    public int getIndexOfChild(Object parent, Object child)
    {
        return 0;
    }

    public Object getRoot()
    {
        return chain[ chain.length - 1 ];
    }

    public boolean isLeaf(Object node)
    {
        X509Certificate last = chain[ 0 ];
        if( last == node )
            return true;
        
        return false;
    }

    public void removeTreeModelListener(TreeModelListener l)
    {
        listener.remove( l );
    }

    public void valueForPathChanged(TreePath path, Object newValue)
    {
    }
}