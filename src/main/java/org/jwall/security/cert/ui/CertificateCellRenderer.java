package org.jwall.security.cert.ui;

import java.awt.Component;
import java.security.cert.X509Certificate;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jwall.app.Application;
import org.jwall.security.cert.CustomTrustManager;

public class CertificateCellRenderer
    extends DefaultTreeCellRenderer
{
    /** The unique class id */
    private static final long serialVersionUID = -4464537602798808152L;
    
    private static CustomTrustManager tm = CustomTrustManager.getInstance();
    
    
    /* (non-Javadoc)
     * @see javax.swing.tree.DefaultTreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean, boolean, boolean, int, boolean)
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row,
            boolean hasFocus)
    {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
                row, hasFocus);
        
        if( value instanceof X509Certificate ){
            
            X509Certificate cert = (X509Certificate) value;
            
            try {
                tm.isValid( cert );
                setIcon( Application.getIcon( "org.jwall.security.cert.certificate.validated" ) );
            } catch (Exception e) {
                setIcon( Application.getIcon( "org.jwall.security.cert.certificate.unvalidated" ) );
            }
            
            setText( cert.getSubjectX500Principal().toString() );
        }
        
        
        return this;
    }
}
