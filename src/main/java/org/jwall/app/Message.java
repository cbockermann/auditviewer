package org.jwall.app;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


@XStreamAlias("Message")
public class Message
{
    @XStreamAlias("body")
    String message;

    @XStreamAsAttribute
    Date date;
    
    //LinkedList<String> attachments = new LinkedList<String>();
    @XStreamAlias("attachments")
    HashMap<String,String> attachments = new HashMap<String,String>();
    
    public Message(){
        date = new Date( System.currentTimeMillis() );
        attachments = new HashMap<String,String>();
    }
    
    
    public Message( String msg ){
        message = msg;
    }

    
    public void setMessage( String msg ){
        message = msg;
    }
    
    public String getMessage(){
        return message;
    }
    
    public void addAttachment( String name, String data ){
        attachments.put( name, data );
    }
    
    public Map<String,String> getAttachments(){
        return attachments;
    }
    
    public String toXML(){
        XStream xs = new XStream();
        xs.processAnnotations( Message.class );
        return xs.toXML( this );
    }
}
