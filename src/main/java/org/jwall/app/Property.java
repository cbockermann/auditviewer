package org.jwall.app;

import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("Property")
public class Property<E> {
	private String name;
	private E value;
	
	public Property( String name, E val ){
		this.name = name;
		this.value = val;
	}
	
	public String getName(){
		return name;
	}
	
	public E getValue(){
		return value;
	}

	public void setValue( E val ){
		value = val;
	}
	
	public Class<?> getPropertyClass(){
	    return value.getClass();
	}
}