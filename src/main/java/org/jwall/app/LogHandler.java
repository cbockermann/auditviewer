/**
 * 
 */
package org.jwall.app;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * @author chris
 *
 */
public class LogHandler extends Handler {

    public LogHandler() {
        this.setLevel(Level.ALL);
        // System.out.println("Creating LogHandler, level = ALL");
    }

    /**
     * @see java.util.logging.Handler#publish(java.util.logging.LogRecord)
     */
    @Override
    public void publish(LogRecord record) {
        String msg = this.getFormatter().format(record);
        BugReportLogger.log(msg);
    }

    /**
     * @see java.util.logging.Handler#flush()
     */
    @Override
    public void flush() {
    }

    /**
     * @see java.util.logging.Handler#close()
     */
    @Override
    public void close() throws SecurityException {
    }
}
