package org.jwall.app;

import java.awt.event.ActionEvent;
import java.util.List;

import org.jwall.app.ui.ApplicationWindow;
import org.jwall.app.ui.View;


/**
 * 
 * This interface defines a module as part of an application. Modules provide
 * a set of actions and a view.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public interface Module
{

    /**
     * Returns the descriptive name of this module. 
     * 
     * @return Name of the module.
     */
    public abstract String getName();

    
    /**
     * This method is called when the module is unregistered from the
     * application.
     *  
     */
    public abstract void cleanUp();

    
    /**
     * This method returns the Action with the given name <code>cmd</code>. The
     * name corresponds to the Action's command string. If the module does
     * not provide an action with that name, <code>null</code> is returned.
     * 
     * @param cmd The command string of the action object.
     * @return The action object, or <code>null</code> if no object with the
     *         given command string exists.
     */
    public abstract Action getAction(String cmd);

    
    /**
     * This method returns the list of Actions provided by this module.
     * 
     * @return The list of actions of this module.
     */
    public abstract List<Action> getActions();

    
    /**
     * This is inherited by the ActionListener interface. The method is
     * called whenever an action fires.
     *  
     * @param evt The event fired by the action.
     */
    public abstract void actionPerformed(ActionEvent evt);

    
    /**
     * This method returns the Application to which this module has been registered.
     * 
     * @return The corresponding application (parent) of this module.
     */
    public abstract Application getApplication();

    
    /**
     * This is a convenient method which simply returns the application's window. In
     * case the module has not yet registered at an application, <code>null</code> is
     * returned. 
     * 
     * @return The application's window or <code>null</code> if the module is not
     *         registered to an application.
     */
    public abstract ApplicationWindow getWindow();

    
    /**
     * The view of the module. If the module does not provide a view, <code>null</code>
     * is returned.
     * 
     * @return The view (visual component) of the module.
     */
    public abstract View getView();
}