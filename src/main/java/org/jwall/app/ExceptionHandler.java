package org.jwall.app;

/**
 * 
 * This interface defines an abstract handler for catching exceptions. Classes implementing
 * this interface can be registered to an application instance and will be called if an
 * exception occurs somewhere in the code.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface ExceptionHandler
{

    /**
     * Handle the given exception.
     * 
     * @param e
     */
    public void handleException( Exception e );
}
