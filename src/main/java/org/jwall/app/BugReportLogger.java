/**
 * 
 */
package org.jwall.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * @author chris
 *
 */
public class BugReportLogger extends ConsoleAppender {

    final static List<String> lines = new ArrayList<String>();
    static int limit = 10000;

    public BugReportLogger() {
        // System.out.println("Creating new bug-report-logger..");
    }

    /**
     * @see org.apache.log4j.WriterAppender#append(org.apache.log4j.spi.LoggingEvent)
     */
    @Override
    public void append(LoggingEvent event) {
        // System.out.print("Logging event: " + event + " => ");
        String line = getLayout().format(event);
        // System.out.println(line);
        log(line);
    }

    public static String backlog() {
        StringBuffer s = new StringBuffer();
        for (String line : lines) {
            s.append(line + "\n");
        }
        return s.toString();
    }

    protected static void log(String msg) {
        lines.add(msg);
        while (lines.size() > limit) {
            lines.remove(0);
        }
    }
}