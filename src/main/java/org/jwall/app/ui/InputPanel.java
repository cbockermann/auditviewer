package org.jwall.app.ui;

import javax.swing.JPanel;


/**
 * 
 * 
 * 
 * @author chris@jwall.org
 */
public abstract class InputPanel extends JPanel {
	
	/**
     * 
     */
    private static final long serialVersionUID = -606554138335394144L;

    public abstract boolean checkInput() throws InputError;
	
	public void saveProperties(){
	    
	}
}
