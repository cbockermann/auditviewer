package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;


public class SelectionDialog
    extends JDialog
    implements KeyListener
{
    private static final long serialVersionUID = -1825593684630412137L;
    static Logger log = Logger.getLogger( SelectionDialog.class.getName() );
    
    private JList list;
    int keyPressed;
    Vector<Object> selections = new Vector<Object>();
    
    private JScrollPane scrollPane;
    private List<SelectionListener> listener = new LinkedList<SelectionListener>();
    
    private boolean finished = false;
    
    public SelectionDialog( Collection<?> vals ){
        this.setUndecorated(true);
        this.setAlwaysOnTop( true );
        this.setFocusable( false );
        this.setFocusableWindowState( false );
        
        for( Object o: vals )
            selections.add( o );
        
        list = new JList( selections );
        getContentPane().setLayout( new BorderLayout() );
        
        //list.addKeyListener( this );
        
        scrollPane = new JScrollPane( list );
        getContentPane().add( scrollPane, BorderLayout.CENTER );
    }
    
    public void show( int x, int y ){
        this.pack();
        this.setLocation( x, y );
        this.setVisible( true );
    }
    
    public void setSelectedIndex( int i ){
        ListModel lm = list.getModel();
        if( i < lm.getSize() ){
            list.setSelectedIndex( i );
            list.scrollRectToVisible( list.getCellBounds( i, i ) );
        }
    }
    
    public void clearSelection(){
        list.clearSelection();
        scrollPane.scrollRectToVisible( list.getCellBounds( 0, 0 ) );
    }
    
    public void selectNext(){
        int idx = list.getSelectedIndex();
        if( idx >= 0 && idx + 1 < list.getModel().getSize() )
            setSelectedIndex( idx + 1 );
        else
            setSelectedIndex( 0 );
    }

    public void selectPrevious(){
        int idx = list.getSelectedIndex();
        if( idx >= 1 )
            setSelectedIndex( idx - 1 );
        else
            setSelectedIndex( list.getModel().getSize() );
    }
    
    public void keyPressed( KeyEvent e ){
        log.fine( "KeyPressed: " + e );
        keyPressed = e.getKeyCode();
        
        if( e.getKeyCode() == KeyEvent.VK_KP_DOWN || e.getKeyCode() == KeyEvent.VK_DOWN ){
            this.selectNext();
            for( SelectionListener l : this.listener )
                l.selectionChanged( list.getSelectedValue(), this );
        }
        
        if( e.getKeyCode() == KeyEvent.VK_KP_UP || e.getKeyCode() == KeyEvent.VK_UP ){
            this.selectPrevious();
            for( SelectionListener l : this.listener )
                l.selectionChanged( list.getSelectedValue(), this );
        }
    }
    
    public void keyReleased( KeyEvent e){
        if( keyPressed == e.getKeyCode() ){
            if( keyPressed == KeyEvent.VK_ESCAPE ){
                cancel();
            }
            
            if( keyPressed == KeyEvent.VK_ENTER ){
                finish();
            }
        }
    }
    
    public void keyTyped( KeyEvent e ){
        if( e.getKeyCode() == KeyEvent.VK_ESCAPE ){
            list.clearSelection();
            finish();
            this.setVisible( false );
        }
    }
    
    public void addSelectionListener( SelectionListener l ){
        if( !listener.contains( l ) )
            listener.add( l );
    }
    
    public void removeSelectionListener( SelectionListener l ){
        listener.remove( l );
    }
    
    public void cancel(){
        
        if( !this.isVisible() )
            return;
        
        setVisible( false );
        for( SelectionListener l : listener )
            l.selectionFinished( null, this );
    }
    
    public void finish(){
        
        if( finished || !isVisible() )
            return;
        
        finished = true;
        setVisible( false );
        for( SelectionListener l : listener )
            l.selectionFinished( list.getSelectedValue(), this );
    }
    
    public boolean isFinished(){
        return finished;
    }
    
    public void setFinished( boolean b ){
        finished = b;
    }
    
    public void setCellRenderer( ListCellRenderer lcr ){
        list.setCellRenderer( lcr );
    }
    
    
    public void selectByPrefix( String prefix ){
        Iterator<?> it = selections.iterator();
        int idx = 0;
        
        boolean matches = false;
        while( it.hasNext() && !matches ){
            
            Object o = it.next();
            
            if( o.toString().startsWith( prefix ) ) {
                matches = true;
            }
            if( ! matches )
                idx++;
        }
        
        if( idx >= 0 && matches )
            setSelectedIndex( idx );
        else
            list.clearSelection();
    }
    
    public Object getSelectedValue(){
        return list.getSelectedValue();
    }
}