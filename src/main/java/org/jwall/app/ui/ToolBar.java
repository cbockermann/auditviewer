package org.jwall.app.ui;

import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JToolBar;

import org.jwall.app.Action;
import org.jwall.app.Application;


/**
 * 
 * This class implements a tool bar. It basically is a simple extension of the
 * JToolBar, but tries to preserve an order on the registered actions. The order
 * is determined by the actions properties.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ToolBar
    extends JToolBar
{

    private static final long serialVersionUID = -9017428846453367058L;
    
    /** The ordered set of actions, registered to this toolbar */
	TreeSet<Action> actions = new TreeSet<Action>();
	
	
	/**
	 * This method adds the action to the toolbar and tries to insert it
	 * at the position corresponding to the actions index.
	 * 
	 * @param action The action to be added to the toolbar.
	 */
	public void add( Action action ){
		
		if( actions.contains( action ) )
			return;
		
		int i = actions.size();

		for( Action a : actions )
			if( action.compareTo( a ) < 0 )
				i++;
		
		actions.add( action );
		
		JButton button = new JButton( action );
		//button.setBorderPainted( false );
		//button.setRolloverEnabled( false );
		button.setIcon( Application.getLargeIcon( action.getCommand() ) );
		if( 0 < i && i < super.getComponentCount() )
		    super.add( button, i );
		else
		    add( button );
		revalidate();
	}
}