package org.jwall.app.ui;

public interface SelectionListener
{
    public void selectionChanged( Object selectedValue, SelectionDialog source );
    
    public void selectionFinished( Object selectedValue, SelectionDialog source );
}
