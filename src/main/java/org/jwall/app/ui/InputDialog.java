/*
 * Created on 07.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;


/**
 * This class serves as the basis for all input dialog.
 *
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public abstract class InputDialog extends Dialog 
    implements KeyListener, ActionListener
{

    /**
     * 
     */
    private static final long serialVersionUID = -1843662183749022313L;
    public final static String LOGO_TALL = "org.jwall.app.logo.tall";
	protected JButton okButton;
    protected JPanel content;
    
	public InputDialog()
	{
        setTitle("Authenticate");
		setModal(true);
    
        content = new JPanel(new BorderLayout());
        super.getContentPane().setLayout(new BorderLayout());
        
        ImagePanel i = new ImagePanel(InputDialog.class.getResource("/icons/logo_tall_60x513.jpg"), 0, 0, 60, 513);
        i.setHeight( 513 );
        i.setWidth( 60 );
        i.setBackground( Color.BLACK );
        super.getContentPane().add( i, BorderLayout.WEST );
        super.getContentPane().add( content, BorderLayout.CENTER );
        
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		okButton = new JButton("Ok");
		URL url = InputDialog.class.getResource( "/icons/16x16/yes.png" );
        if(url != null)
            okButton.setIcon( new ImageIcon(url) );
        try {
        	okButton.setEnabled( checkInput() );
        } catch (Exception e){
        	okButton.setEnabled( false );
        }
		okButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				doOk();
			}
		});
		buttonPanel.add(okButton);
		
		JButton cancelButton = new JButton("Cancel");
        url = InputDialog.class.getResource( "/icons/16x16/no.png" );
        if(url != null)
            cancelButton.setIcon( new ImageIcon(url) );
		cancelButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				doCancel();
			}
		});
		buttonPanel.add(cancelButton);

		super.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		setSize(new Dimension(500,357));
        
		center();
	}
    
    public void addPanel(JPanel p){
        getContentPane().add(p, BorderLayout.CENTER);
    }
    
    public Container getContentPane(){
        return content;
    }
    
    public abstract void doOk();
    public abstract void doCancel();
    public abstract boolean checkInput() throws InputError;
    
    public void keyPressed(KeyEvent arg0){ }
    public void keyReleased(KeyEvent arg0){ }
    public void keyTyped(KeyEvent arg0){
    	try {
    		okButton.setEnabled( checkInput() );
    	} catch (InputError ie ){
    		okButton.setEnabled( false );
    	}
    }
}