package org.jwall.app.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jwall.app.Application;


/**
 * 
 * This class implements a context menu for a collection of objects of a specific type.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E> The type of objects for which this menu contains actions.
 */
public class ContextMenu<E>
    extends JPopupMenu
    implements ActionListener
{
    /** A unique class id. */
    private static final long serialVersionUID = -577277440064592607L;

    /** The set of objects to perform actions on */
    Collection<E> objects;

    /** The handlers, providing action within the context */
    LinkedList<ActionHandler<E>> actionHandler = new LinkedList<ActionHandler<E>>();

    
    /**
     * Create a new context menu for the given collection of objects (usually objects
     * selected within a jtree, a list or a table).
     * 
     * @param targets The objects which are to be processed by an action.
     */
    public ContextMenu( Collection<E> targets ){
        objects = new Vector<E>();
        objects.addAll( targets );
    }
    

    /**
     * Create a new context menu for the collection of target-objects and register all
     * actions provided by the given handler instance. 
     * 
     * @param handler The handler providing a set of actions.
     * @param targets The targets which shall be handled.
     */
    public ContextMenu( ActionHandler<E> handler, Collection<E> targets ){
        this( targets );
        
        for( String cmd: handler.getActionCommands() )
            addActionHandler( cmd, handler );
    }
    
    
    
    public void addActionHandler( String actionCommand, ActionHandler<E> l ){
        JMenuItem mi = new JMenuItem( actionCommand );
        
        
        Icon icon = Application.getIcon( actionCommand );
        if( icon != null )
            mi.setIcon( icon );
        
        String label = Application.getMessage( actionCommand );
        if( label != null )
            mi.setText( label );
        
        mi.addActionListener( this );
        add( mi );
    }
    

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed( ActionEvent e ){

        for( E object  : objects ){
            for( ActionHandler<E> handler : actionHandler )
                if( handler.handlesEvent( e ) )
                    handler.dispatchEvent( e, object );
        }
    }
}