package org.jwall.app.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jwall.app.Application;


/**
 * 
 * This panel display an editable combobox and a file chooser button. It is intended
 * to provide a convenient way to allow for the user to choose a file either using the
 * file chooser or selecting a previously selected file from the combo box.  
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class FileSelectionPanel
    extends InputPanel 
    implements ActionListener, ItemListener
{

    private static final long serialVersionUID = -251710008820286733L;

    /** The name of the property which holds the file name */
    public final static String PROPERTY_FILE_NAME = "load.file";
    
    /** The name of the property which holds the history of selected files */
	public final static String PROPERTY_LAST_FILE_NAMES = "org.jwall.web.audit.file.open.history";
	
	/** The command for the file selection button */
	public final static String BUTTON_OPEN = "org.jwall.web.audit.file.open";

	/** The combobox which display the previously selected files */
	JComboBox fileNameBox;
	
	/** The model which contains the filename history */
	StringSetModel fileList;
	
	/** A list of change listeners */
	Vector<ChangeListener> listener = new Vector<ChangeListener>();

	
	/**
	 * This creates new instance of this panel.
	 * 
	 */
	public FileSelectionPanel(){
		this(null);
		setBorder( null );
	}

	
	/**
	 * This creates a new instance of this panel. The given string <code>t</code> is
	 * used to create a label for describing the file to be selected. If <code>null</code>
	 * is specified, the label will not be created.
	 * 
	 * @param t The label for the file selection field.
	 */
	public FileSelectionPanel( String t, String prop ){
		super();
		
		GridBagLayout g = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		this.setBorder(null);
		setLayout( g );
		int i = 0;
		//setBackground( Color.RED );
		if( t != null ){
		    JLabel l = new JLabel( t );
		    //l.setBounds( 20, 30, 120, 22 );
		    l.setMinimumSize( new Dimension( 90, 20 ) );
		    l.setPreferredSize( new Dimension( 90, 20 ) );
		    
		    c.gridx = i++;
		    g.setConstraints( l, c);
		    add(l);
		}
		Font f = new Font("Monospaced", Font.PLAIN, 13);
		
        fileList = new StringSetModel( prop );
        fileNameBox = new JComboBox( fileList );
        fileNameBox.setEditable( true );
        fileNameBox.setFont( f );
        fileNameBox.addItemListener( this );
        //fileNameBox.setPreferredSize( new Dimension( fileNameBox.getPreferredSize().width, 21 ) );
        
        c.gridx = i++;
        c.fill = GridBagConstraints.REMAINDER;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        g.setConstraints( fileNameBox, c);
        add( fileNameBox );
        

		//fileNameField = new JTextField(26);
		//fileNameField.setFont(f);
		//fileNameField.setBounds( 90, 20, 230, 22 );

		//if( System.getProperty( PROPERTY_FILE_NAME ) != null )
		//	fileNameField.setText( System.getProperty(PROPERTY_FILE_NAME) );

		JButton fButton = new JButton( Application.getIcon("org.jwall.web.audit.file.open"));
		fButton.setBorderPainted( false );
		fButton.setContentAreaFilled( false );
		
		fButton.setPreferredSize( new Dimension( 20, 20 ) );
		fButton.setSize( 20, 20 );
		fButton.setMaximumSize( new Dimension( 20, 20 ) );
		fButton.setActionCommand("set.file");
		fButton.addActionListener( this );
		
		c.gridx = i++;
		c.weightx = 0.0;
		c.insets = new Insets(1,1,1,1);
		c.fill = GridBagConstraints.BOTH;
		g.setConstraints( fButton, c );
		add( fButton );
	}
	
	
	public FileSelectionPanel( String t ){
	    this( t, FileSelectionPanel.PROPERTY_LAST_FILE_NAMES );
	}

	
	/**
	 * This method returns the selected file name.
	 * 
	 * @return The selected file name.
	 */
	public String getFilename(){
	    if( fileNameBox.getSelectedItem() != null )
	        return fileNameBox.getSelectedItem().toString(); //.getText();
	    
	    return null;
	}

	
	/**
	 * This method sets the selected file name.
	 * 
	 * @param s The preselected file name.
	 */
	public void setFilename( String s ){
	    fileList.add( s );
	    fileNameBox.setSelectedItem( s );
	}

	
	/**
	 * This method returns the file that is selected.
	 * 
	 * @return The selected file.
	 */
	public File getFile(){
	    return new File( fileNameBox.getSelectedItem() + "" );
	}

	
	/**
	 * @see org.jwall.app.ui.InputPanel#checkInput()
	 */
	public boolean checkInput() throws InputError {
		String t = fileNameBox.getSelectedItem() + "";
		if( t == null || t.equals("") )
			throw new InputError("No file selected!");

		File f = getFile();
		return f != null && f.canRead();
	}

	
	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent arg0)
	{
		if("set.file".equals(arg0.getActionCommand())){

			JFileChooser f = new JFileChooser();
			int ret = f.showOpenDialog( this );

			if( ret == JFileChooser.APPROVE_OPTION ){
			    fileList.add( f.getSelectedFile().getAbsolutePath() );
				notifyListeners();
			}
		}
	}

	public void addChangeListener( ChangeListener l ){
		listener.add( l );
	}

	public void removeChangeListener( ChangeListener l ){
		listener.remove( l );
	}

	private void notifyListeners(){
		for( ChangeListener l : listener )
			l.stateChanged( new ChangeEvent( this ) );
	}

    /* (non-Javadoc)
     * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
     */
    public void itemStateChanged(ItemEvent e)
    {
        if( e.getSource() == fileNameBox ){
            notifyListeners();
        }
    }
    
    /**
     * This method save the file history to the system properties.
     * 
     * @see org.jwall.app.ui.InputPanel#saveProperties()
     */
    public void saveProperties(){
        fileList.saveListToSystemProperties( FileSelectionPanel.PROPERTY_LAST_FILE_NAMES );
    }
}