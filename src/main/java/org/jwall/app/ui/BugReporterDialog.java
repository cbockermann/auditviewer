package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import java.util.TreeSet;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import org.jwall.app.table.MapTable;
import org.jwall.app.table.MapTableCellRenderer;



/**
 * 
 * This dialog resents some details of a bug-report, previously created by the BugReporter class.
 * It is used to let the user confirm to file in the bug-report to the jwall.org site.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class BugReporterDialog
extends Dialog
{
    /** A unique class id  */
    private static final long serialVersionUID = -446291957255666728L;

    /** A constant indicating the user pressed the SEND button */
    public final static int SEND_REPORT = 0;

    /** A constant indicating the cancel button, pressed by the user */
    public final static int CANCEL_REPORT = 1;

    /** This attribute finally contains the user's choice, i.e. SEND or CANCEL */
    int returnStatus = 1;

    /* The buttons */
    JButton ok, cancel;

    /* A text-field for additional comments */
    JEditorPane commentField;

    /** An editor-pane displaying each of the attachments, i.e. the parts of the report */
    JEditorPane[] reportFields;

    Map<String,String> report; 

    /**
     * 
     * Create the UI elements and set up the dialog.
     * 
     */
    public BugReporterDialog( Map<String,String> reportData ){
        super();
        setModal( true );
        setResizable( false );
        setTitle( "Report a Bug" );

        this.report = reportData;
        reportFields = new JEditorPane[ reportData.keySet().size() ];

        this.getContentPane().setLayout( new BorderLayout() );

        ImagePanel i = new ImagePanel(InputDialog.class.getResource("/icons/logo_tall_60x513.jpg"), 0, 0, 60, 513);
        i.setHeight( 513 );
        i.setWidth( 60 );
        i.setBackground( Color.BLACK );
        this.getContentPane().add( i, BorderLayout.WEST );


        JEditorPane msg = new JEditorPane();
        msg.setBackground( Color.WHITE );
        msg.setFont( msg.getFont().deriveFont( Font.PLAIN ) );
        JPanel info = new JPanel( new BorderLayout() );
        info.setBackground( Color.WHITE );
        //info.add( new ImagePanel( CertificateValidationDialog.class.getResource("/icons/24x24/stock_certificate.png"), 0, 0, 24, 24 ), BorderLayout.WEST );

        JLabel l = new JLabel();
        l.setAlignmentX( Component.CENTER_ALIGNMENT );
        l.setAlignmentY( Component.CENTER_ALIGNMENT );
        l.setBorder( new EmptyBorder( 15, 15, 15, 15 ) );
        //l.setIcon( Application.getIcon("org.jwall.app.bug-report") );

        ImageIcon icon = new ImageIcon( BugReporterDialog.class.getResource( "/icons/48x48/bugzilla.png" ) );
        l.setIcon( icon );

        info.add(  l, BorderLayout.EAST );
        info.add( msg, BorderLayout.CENTER );
        msg.setContentType( "text/html" );
        msg.setText( "<html><body style=\"font-family: Verdana,Arial,Sansserif; font-size: 12pt;\">"
                + "<p>An exception has been thrown by the application. Below you find a stack-trace of the exception which can be very "
                + "helpful for debugging purposes. In order to help improving the application you can "
                + "send this report to the jwall.org site.</p><p>No private data will be sent.</p>"
                + "</body></html>" );
        msg.setEditable( false );
        msg.setBorder( new EmptyBorder( 5, 15, 15, 15 ) );
        info.setBorder( BorderFactory.createEtchedBorder() );

        JPanel p = new JPanel( new BorderLayout() );
        p.add( info, BorderLayout.NORTH );

        JPanel buttons = new JPanel( new FlowLayout( FlowLayout.RIGHT ) );
        commentField = new JEditorPane();

        JTabbedPane tabs = new JTabbedPane();

        int j = 0;
        int sel = 0;

        TreeSet<String> keys = new TreeSet<String>();
        keys.addAll( reportData.keySet() );

        for( String title : keys ){

            // Usually we simply create a tab with the text description of the report-part
            //
            if( !title.equalsIgnoreCase( "Version Information" ) ){
                
                reportFields[j] = new JEditorPane();
                reportFields[j].setEditable( false );
                reportFields[j].setText( reportData.get( title ) );
                JScrollPane rs = new JScrollPane( reportFields[j] );
                rs.setBorder( null );
                if( title.equalsIgnoreCase("Stack Trace" ) )
                    sel = j;
                tabs.add( title , rs );
                
            } else {
                //
                // we treat the "Version Information" part different
                //
                MapTable<String,String> table = new MapTable<String,String>( "Component", "Version" );
                try {
                    
                    BufferedReader r = new BufferedReader( new StringReader( reportData.get( title ) ) );
                    String line = r.readLine();
                    while( line != null ){
                        String[] tok = line.split( "=", 2 );
                        table.put( tok[0], tok[1] );
                        line = r.readLine();
                    }
                    
                } catch (IOException e) {
                    e.printStackTrace();
                }
                JTable jtable = new JTable( table );
                jtable.setRowHeight( 25 );
                jtable.setFont( new Font("Monospaced", Font.PLAIN, 13 ) );
                jtable.setDefaultRenderer( Object.class, new MapTableCellRenderer() );
                JScrollPane sp = new JScrollPane( jtable );
                sp.setBorder( null );
                tabs.add( title, sp );
            }
        }

        tabs.setBorder( null );
        tabs.setSelectedIndex( sel );
        JPanel cp = new JPanel( new BorderLayout() );
        JLabel clabel = new JLabel();

        JEditorPane cm = new JEditorPane();
        cm.setEditable( false );
        cm.setOpaque( false );
        cm.setBackground( this.getBackground() );
        cm.setContentType( "text/html" );
        cm.setText( "<html><body style=\"font-family: Verdana,Arial,Sansserif; font-size: 12pt;\">Please enter an additional comment describing the situation in which the exception occurred:</body></html>"  );
        cm.setBorder( new EmptyBorder( 5, 0, 5, 5 ) );
        clabel.setAlignmentX( Component.LEFT_ALIGNMENT );

        cp.add( cm, BorderLayout.NORTH );
        JScrollPane commentScroller = new JScrollPane( commentField );
        cp.add( commentScroller, BorderLayout.CENTER );

        JSplitPane sp = new JSplitPane( JSplitPane.VERTICAL_SPLIT, tabs, cp );
        sp.setBorder( new EmptyBorder( 10, 10, 10, 10 ) );
        p.add( sp, BorderLayout.CENTER );

        ok = new JButton("Send report");
        ok.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                returnStatus = SEND_REPORT;
                setVisible( false );
            }
        });

        cancel = new JButton("Cancel");
        cancel.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e){
                returnStatus = CANCEL_REPORT;
                setVisible( false );
            }
        });

        buttons.add( ok );
        buttons.add( cancel );
        this.getContentPane().add( p, BorderLayout.CENTER );

        this.getContentPane().add( buttons, BorderLayout.SOUTH );

        setSize( 680, 513 );
        this.center();
    }


    /**
     * Returns the user comment, entered in the bug-report dialog.
     * 
     * @return The user comment.
     */
    public String getComment(){
        return commentField.getText();
    }


    /**
     * 
     * 
     * @return
     */
    public Map<String,String> getReport(){
        return report;
    }

    public int getUserChoice(){
        return this.returnStatus;
    }
}