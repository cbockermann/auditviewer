package org.jwall.app.ui;

public class UserError
    extends Exception
{
    private static final long serialVersionUID = -8362621226312046348L;

    
    public UserError( String msg ){
        super( msg );
    }
}
