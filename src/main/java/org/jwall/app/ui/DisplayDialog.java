package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.jwall.app.Application;


/**
 * 
 * This class implements a small informational browser.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public class DisplayDialog
extends Dialog
implements ActionListener, HyperlinkListener
{

    private static final long serialVersionUID = 1486048724860104718L;

    /** The editor used for displaying the resource */
    JEditorPane editor;

    /** This list stores the history for "back" and "forward" navigation */
    LinkedList<URL> history = new LinkedList<URL>();

    /** The current point in the history */
    int historyIndex = 0;

    /** The Back and Forward buttons */
    JButton back, forw;


    public DisplayDialog( URL url ){
        this( url, true );
    }

    /**
     * This constructor creates a new dialog that displays the given URL. 
     * If <code>allowNavigation</code> is true, then it allows for the user to
     * follow hyper links, which are again displayed within the dialog. 
     * 
     * @param url The URL of the resource to be displayed.
     * @param allowNavigation If hyper link navigation is allowed.
     */
    public DisplayDialog(URL url, boolean allowNavigation ){
        setModal(true);
        setLayout(new BorderLayout());

        ImagePanel i = new ImagePanel(InputDialog.class.getResource("/icons/logo_tall_60x513.jpg"), 0, 0, 60, 513);
        i.setHeight( 513 );
        i.setWidth( 60 );
        i.setBackground( Color.BLACK );
        super.getContentPane().add( i, BorderLayout.WEST );

        JPanel buttons = new JPanel( new FlowLayout( FlowLayout.LEFT ) );

        back = new JButton( Application.getIcon( DisplayAction.BACKWARD ) );
        back.setActionCommand("history.back");
        back.addActionListener( this );
        back.setEnabled( false );
        buttons.add( back );

        forw = new JButton( Application.getIcon( DisplayAction.FORWARD ) );
        forw.setActionCommand("history.forw");
        forw.addActionListener( this );
        forw.setEnabled( false );
        buttons.add( forw );

        editor = new JEditorPane();
        editor.setEditable(false);
        editor.setBorder( new EmptyBorder(10,10,10,10) );
        editor.setBackground( Color.WHITE );

        try {
            if(url != null){
                editor.setPage(url);
                history.add( url );
            }

            if( allowNavigation)
                editor.addHyperlinkListener( this );

        } catch (IOException e) {
            e.printStackTrace();
        }
        JScrollPane editScroller = new JScrollPane(editor);

        super.getContentPane().add( editScroller, BorderLayout.CENTER );

        if( allowNavigation )
            super.getContentPane().add( buttons, BorderLayout.NORTH );

        setSize( new Dimension(700,450) );
        setResizable( false );
        center();
    }

    public DisplayDialog(URL url, boolean allowNav, int w, int h ){
        this( url, allowNav );
        setSize(new Dimension(w,h));
        center();
    }


    public void actionPerformed(ActionEvent e){
        //System.out.println("index: "+historyIndex+", "+e);

        if( history.size() > 0 && "history.back".equals( e.getActionCommand() ) ){
            try {
                editor.setPage( history.get( --historyIndex ) );
            } catch (Exception ex){
                ex.printStackTrace();
            }
            back.setEnabled( history.size() > 0 && historyIndex > 0 );
            forw.setEnabled( historyIndex + 1 < history.size() );
        }

        if( historyIndex + 1 < history.size() && "history.forw".equals( e.getActionCommand() ) ){
            try {
                editor.setPage( history.get( ++historyIndex ) );
            } catch (Exception ex){
                ex.printStackTrace();
            }
            forw.setEnabled( historyIndex > history.size() );
            back.setEnabled( historyIndex > 0 );
        }
    }

    public void hyperlinkUpdate(HyperlinkEvent e) {
        
        if( !"http".equals( e.getURL().getProtocol() ) && e.getEventType() == HyperlinkEvent.EventType.ACTIVATED ){
            try {
                URL u = e.getURL();
                history.add( u );
                historyIndex = history.size() - 1;
                editor.setPage( u );

                back.setEnabled( historyIndex > 0 );
            } catch ( Exception ex ){
                ex.printStackTrace();
            }
        }
    }


    private static final String errMsg = "Error attempting to launch web browser";

    public static void openURL( String url ) {
        String osName = System.getProperty("os.name");
        try {
            if (osName.startsWith("Mac OS")) {
                Class<?> fileMgr = Class.forName("com.apple.eio.FileManager");
                Method openURL = fileMgr.getDeclaredMethod("openURL",
                        new Class[] {String.class});
                openURL.invoke(null, new Object[] {url});
            }
            else if (osName.startsWith("Windows"))
                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
            else { //assume Unix or Linux
                String[] browsers = {
                        "firefox", "opera", "konqueror", "epiphany", "mozilla", "netscape" };
                String browser = null;
                for (int count = 0; count < browsers.length && browser == null; count++)
                    if (Runtime.getRuntime().exec(
                            new String[] {"which", browsers[count]}).waitFor() == 0)
                        browser = browsers[count];
                if (browser == null)
                    throw new Exception("Could not find web browser");
                else
                    Runtime.getRuntime().exec(new String[] {browser, url});
            }
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, errMsg + ":\n" + e.getLocalizedMessage());
        }
    }
}