package org.jwall.app.ui;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.jwall.app.Action;
import org.jwall.app.Application;


/**
 * 
 * This class implements a simple MenuBar. It basically adds some convenience to the
 * native swing JMenuBar in the way that it tries to create menu-items in sub-menues
 * according to an action's path.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * TODO: This class does not work properly, yet. There is still a lot to do to get
 * the basic order-preserving action management right.
 * 
 */
public class MenuBar
    extends JMenuBar 
{
    private static final long serialVersionUID = -1571547655333111280L;
    
    /** This vector takes care of the menues that have been created so far */
    Vector<String> menues = new Vector<String>();
	
    HashMap<JMenu,TreeSet<Action>> menuActions = new HashMap<JMenu,TreeSet<Action>>();
    
    /**
     * 
     * This creates an empty menu bar.
     * 
     */
	public MenuBar(){
		super();
		
		add( new JMenu("File") );
		add( new JMenu("Help") );
		this.setVisible( true );
	}

	
	public JMenu add( JMenu m ){
	    super.add( m );
	    
	    TreeSet<Action> actions = menuActions.get( m );
	    if( actions == null )
	        menuActions.put( m, new TreeSet<Action>() );
	    
	    return m;
	}
	
	
	/**
	 * This method adds a menu item for the given action to the menu bar. It determines
	 * the action's menu path and inserts the action into the menu according to this
	 * path.
	 * 
	 * @param action The action to be added.
	 * @return The JMenuItem created for this action.
	 */
	public JMenuItem add( Action action ){
		if( action.getMenuLabel() == null )
			return null;
		
		String[] mpath = action.getMenuLabel().split("/");
		JMenu menu = null;
		
		for( int i = 0; menu == null && i < this.getMenuCount(); i++ ){
			
			if( mpath[0].equals( getMenu(i).getText() ) ){
				menu = getMenu(i);
			}
		}
		
		if( menu == null ){
			menu = new JMenu( mpath[0] );
			add(menu, getMenuCount() - 1 ); // Insert new menus BEFORE the last one (help-menu)
		}
		
		JMenuItem mi = new JMenuItem( action );
		mi.setActionCommand( action.getCommand() );
		mi.setText( Application.getMessage( action.getCommand() ) );
		mi.setIcon( Application.getIcon( action ) );
		
		TreeSet<Action> actions = menuActions.get( menu );
		if( actions == null ){
		    actions = new TreeSet<Action>();
		    menuActions.put( menu, actions );
		}
		actions.add( action );
		
		
		int idx = 0;
		Iterator<Action> it = actions.iterator();
		while( it.hasNext() ){
		    
		    if( it.next().compareTo( action ) < 0 )
		        idx++;
		    else
		        break;
		}

		menu.insert( mi, idx );
		return mi;
	}
}