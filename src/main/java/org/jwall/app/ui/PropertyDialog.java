package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jwall.app.Application;
import org.jwall.app.Property;
import org.jwall.app.PropertySet;


public class PropertyDialog 
extends Dialog 
{
	private static final long serialVersionUID = 2939831916618134029L;

	JPanel content;
	JLabel msg = new JLabel();
	JButton okButton, cancelButton;
	PropertySet properties;

	public PropertyDialog( PropertySet p ){
		setModal( true );
		setTitle( "Configuration: " + p.getCategory() );
		content = new JPanel(new BorderLayout());
		super.getContentPane().setLayout(new BorderLayout());

		properties = p;
		
		ImagePanel i = new ImagePanel( InputDialog.class.getResource("/icons/logo_tall_60x513.jpg"), 0, 0, 60, 513);
		i.setHeight( 513 );
		i.setWidth( 60 );
		i.setBackground( Color.BLACK );

		JPanel info = new JPanel( new BorderLayout() );
		info.setBackground( Color.WHITE );
		info.setBorder( new EmptyBorder( 20, 10, 10, 10 ) );

		msg.setFont( msg.getFont().deriveFont( Font.PLAIN ) );
		info.add( msg, BorderLayout.CENTER );
		this.setMessage( "<html>Configuration of properties <b>\""+p.getCategory()+"\"</b>.</html>" );

		content.add( info, BorderLayout.NORTH );

		super.getContentPane().add( i, BorderLayout.WEST );
		super.getContentPane().add( content, BorderLayout.CENTER );

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		okButton = new JButton("Ok");
		URL url = InputDialog.class.getResource( "/icons/16x16/yes.png" );
		if(url != null)
			okButton.setIcon( new ImageIcon(url) );
		try {
			okButton.setEnabled( checkInput() );
		} catch (Exception e){
			okButton.setEnabled( false );
		}
		okButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				doOk();
			}
		});
		buttonPanel.add(okButton);

		JButton cancelButton = new JButton("Cancel");
		url = InputDialog.class.getResource( "/icons/16x16/no.png" );
		if(url != null)
			cancelButton.setIcon( new ImageIcon(url) );
			cancelButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				doCancel();
			}
		});
		buttonPanel.add(cancelButton);

		super.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		setSize(new Dimension(500,357));

		center();
	}

	public void setMessage( String msg ){
		this.msg.setText( msg );
	}

	public void doOk(){
		setVisible( false );
	}

	public void doCancel(){
		properties = null;
		setVisible( false );
	}

	public PropertySet getProperties(){
		return properties;
	}

	public boolean checkInput(){
		return true;
	}

	public static void main( String[] args ){

	    Application.setLookAndFeel();
	    
		PropertySet p = new PropertySet("Test");
		p.setProperty( "login" , "admin" );
		p.setProperty( "password", "secret" );

		PropertySet tp = new PropertySet("org.jwall.web.audit.event.table");
		tp.addProperty( new Property<String>( "Session-ID", "Session-ID" ) );
		tp.addProperty( new Property<String>( "", "" ) );
		
		PropertyDialog d = new PropertyDialog( p );

		d.setVisible( true );

		if( d.getProperties() != null )
			System.out.println( PropertySet.getXStream().toXML( d.getProperties() ) );
		
		System.exit(0);
	}
}
