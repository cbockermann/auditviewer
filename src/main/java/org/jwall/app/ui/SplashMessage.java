package org.jwall.app.ui;

import java.awt.Color;
import java.awt.Font;


class SplashMessage {
    public int x, y;
    public String msg;
    public Font font = null;
    public Color color = Color.WHITE;
    
    public SplashMessage( int x, int y, String m ){
        this.x = x;
        this.y = y;
        this.msg = m;
    }
    
    public SplashMessage( int x, int y, String m, Font f ){
        this( x, y, m );
        font = f;
    }
    
    public SplashMessage( int x, int y, String m, Font f, Color c ){
        this( x,y,m,f );
        color = c;
    }
}
