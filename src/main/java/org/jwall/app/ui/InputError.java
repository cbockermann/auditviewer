package org.jwall.app.ui;

public class InputError extends Exception {
	public final static long serialVersionUID = 453534L;
	
	
	public InputError( String msg ){
		super( msg );
	}
}
