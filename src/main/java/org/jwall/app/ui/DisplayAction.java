package org.jwall.app.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import org.jwall.app.Action;


/**
 * 
 * This action simply pops up a dialog for displaying a HTML resource to the user. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class DisplayAction 
extends Action
implements ActionListener
{

    private static final long serialVersionUID = -5898969936295399289L;
    
    public final static String FORWARD = "org.jwall.app.displaydialog.forw";
    public final static String BACKWARD = "org.jwall.app.displaydialog.back";
    
    /** The URL of the resource that is to be displayed */
    private URL url;
	
    /** Flag for allowing hyperlink usage within the dialog */
    boolean allowNav = false;
    

	/**
	 * This creates an action that displays the given url.
	 * 
	 * @param cmd
	 * @param menu
	 * @param url The url of the resource that is to be displayed.
	 * @param navAllow Determines if the user is allowed to follow hyperlinks within the dialog.
	 */
	public DisplayAction( String cmd, String menu, double idx, URL url, boolean navAllow ){
		this( cmd, menu, idx, url );
		allowNav = navAllow;
	}
	
	public DisplayAction( String cmd, String menu, double idx, URL url ){
		super( cmd, menu, idx );
		this.url = url;
	}

	
	/**
	 * @see org.jwall.app.Action#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed( ActionEvent e ){
		DisplayDialog d = new DisplayDialog( url, allowNav );
		d.setModal( true );
		d.setVisible( true );
	}
}