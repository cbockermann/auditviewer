package org.jwall.app.ui;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusBar
    extends JPanel
{
    private static final long serialVersionUID = 3524490141847236480L;
    JLabel msg = new JLabel();
    
    public StatusBar(){
        setLayout( new FlowLayout( FlowLayout.LEFT ) );
        
        add( msg );
    }
    
    
    public void setMessage( String m ){
        msg.setText( m );
    }
}
