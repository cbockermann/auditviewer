package org.jwall.app.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.net.URL;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

import org.jwall.app.Application;

/**
 * A simple splash-screen which allows to display customized messages upon startup. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class SplashScreen
    extends Dialog
    implements Runnable
{

    private static final long serialVersionUID = -8769175627193266653L;
    
    /** The progress bar displayed under the logo */
    JProgressBar progress = new JProgressBar(0, 100);
    
    /** The label which displays the status message */
    JLabel msg = new JLabel();
    
    /** This label allows the display of a subtitle for a message */
    JLabel sub = new JLabel();
    
    
    /** The image-panel displaying the image/logo */
    SplashPanel img;
    
    
    /**
     * 
     * This method creates a new undecorated dialog which display
     * the splash image at first and allows for a progress and status
     * messages to be displayed to the user.
     * 
     */
    public SplashScreen(){
        this.setUndecorated( true );
        this.setAlwaysOnTop( true );
        
        progress.setBorder( null );
        progress.setForeground( Color.WHITE );
        URL url = Application.class.getResource("/icons/logo.jpg");
        img = new SplashPanel(url, 0, 0, 300, 200);
        
        getContentPane().setLayout( new BorderLayout() );
        getContentPane().add( img, BorderLayout.CENTER );
        msg.setBounds( 5, 200, 300, 17);
        msg.setOpaque( false );
        msg.setForeground( Color.WHITE );
        progress.setBounds( 0, 217, 300, 3);
        //getContentPane().add( msg, BorderLayout.SOUTH );
        getContentPane().add( progress, BorderLayout.SOUTH );
        setSize(300, 203);
        progress.setBorder(null);
        progress.setForeground( Color.ORANGE );
        progress.setPreferredSize( new Dimension( 300, 3 ) );

        center();
    }
    
    
    /**
     * This method sets the status message to be displayed within the dialog.
     * 
     * @param msg The message to display.
     */
    public void setMessage( String msg ){
        clearMessages();
        addMessage( new SplashMessage( 5, 180, msg ) );
        img.repaint();
    }
    
    public void setSubtitle( String s ){
        Font f = img.getFont().deriveFont( 9.0f );
        addMessage( new SplashMessage( 5, 193, s, f ) );
    }
    
    public void clearMessages(){
        img.msgs.clear();
        img.repaint();
    }
    
    public void addMessage( SplashMessage m ){
        img.addMessage( m );
    }
    
    /**
     * This method sets the level of the progress bar.
     * 
     * @param val The new value of the progress bar.
     */
    public void setProgress( int val ){
    	progress.setValue( val );
    }

    
    /**
     * This method implements the Runnable interface. It is used to assure that the
     * image is properly painted.  
     * 
     * @see java.lang.Runnable#run()
     */
    public void run(){
        while( progress.getValue() < 110){
        	try {
        		progress.repaint();
        		Thread.sleep(10);
        	} catch (Exception e){
        		
        	}
        }
        
        setVisible(false);
    }
}