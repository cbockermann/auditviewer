/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.app.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * <p>
 * This is a simple implementation of the Swing-ListModel for presenting lists of columns.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class TableColumnListModel
implements ListModel, TreeModel
{
    /* The list of columns contained in this model */
    ArrayList<TableColumn> columns = new ArrayList<TableColumn>();

    /* The standard list of data listeners... */
    protected LinkedList<ListDataListener> listener = new LinkedList<ListDataListener>();

    protected LinkedList<TreeModelListener> treeListener = new LinkedList<TreeModelListener>();

    Object root = new Object();


    /**
     * 
     * @param columnList
     */
    public TableColumnListModel( Collection<TableColumn> columnList ){

        for( TableColumn col : columnList )
            columns.add( col );
    }


    /**
     * @see javax.swing.ListModel#getElementAt(int)
     */
    public Object getElementAt(int index)
    {
        return getColumnAt( index );
    }

    public TableColumn getColumnAt( int index ){
        return columns.get( index );
    }


    public void add( int index, TableColumn col ){
        columns.add( index, col );
        notifyListeners();
    }

    public void remove( int index ){
        columns.remove( index );
        notifyListeners();
    }

    public void swap( int index0, int index1 ){

        TableColumn col0 = columns.get( index0 );
        TableColumn col1 = columns.get( index1 );
        columns.set( index0, col1 );
        columns.set( index1, col0 );

        notifyListeners();
    }


    /**
     * @see javax.swing.ListModel#getSize()
     */
    public int getSize()
    {
        return columns.size();
    }


    public void notifyListeners(){
        for( ListDataListener l : listener )
            l.contentsChanged( new ListDataEvent( this, ListDataEvent.CONTENTS_CHANGED, 0, getSize() ) );

        for( TreeModelListener l : treeListener )
            l.treeStructureChanged( new TreeModelEvent( this, new Object[]{ root } ) );
    }

    /**
     * @see javax.swing.ListModel#addListDataListener(javax.swing.event.ListDataListener)
     */
    public void addListDataListener(ListDataListener l)
    {
        listener.add( l );
    }

    /**
     * @see javax.swing.ListModel#removeListDataListener(javax.swing.event.ListDataListener)
     */
    public void removeListDataListener(ListDataListener l)
    {
        listener.remove( l );
    }


    public Object getChild(Object parent, int index)
    {
        return columns.get( index );
    }


    public int getChildCount(Object parent)
    {
        return columns.size();
    }


    public int getIndexOfChild(Object parent, Object child)
    {
        return columns.indexOf( child );
    }


    public Object getRoot()
    {
        return root;
    }


    public boolean isLeaf(Object node)
    {
        return node != root;
    }


    public void addTreeModelListener(TreeModelListener l)
    {
        treeListener.add( l );
    }


    public void removeTreeModelListener(TreeModelListener l)
    {
        treeListener.remove( l );
    }


    public void valueForPathChanged(TreePath path, Object newValue)
    {
        if( newValue != null && path != null && path.getLastPathComponent() != null ){
            int idx = columns.indexOf( path.getLastPathComponent() );

            TableColumn newCol = (TableColumn) newValue;
            TableColumn col = columns.get( idx );
            col.setColumnName( newCol.getColumnName() );
            col.setVariable( newCol.getVariable() );
            notifyListeners();
        }
    }
}
