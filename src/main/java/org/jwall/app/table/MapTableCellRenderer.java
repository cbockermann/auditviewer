package org.jwall.app.table;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MapTableCellRenderer
    extends DefaultTableCellRenderer
{
    /** The unique class id */
    private static final long serialVersionUID = 8210108652641359175L;

    protected static Color oddBackgroundColor = Color.white;
    
    protected static Color evenBackgroundColor = new Color( 238, 248, 255 ); 
    
    /* (non-Javadoc)
     * @see javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column)
    {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                row, column);
        
        if( !isSelected ){
            if( row % 2 != 0 )
                this.setBackground( oddBackgroundColor );
            else
                this.setBackground( evenBackgroundColor );
        }
            
        return this;
    }
}