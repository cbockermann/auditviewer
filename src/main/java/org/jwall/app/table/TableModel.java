package org.jwall.app.table;

import javax.swing.table.AbstractTableModel;


/**
 * <p>
 * This class implements a generic table model. The model contains a list of objects, each of
 * which represents a single row in the table. Additionally, sub-classes need to provide a
 * column-mapper, which returns the appropriate column for a specific row by extracting the value
 * for that column from the row object. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E>
 */
public abstract class TableModel<E>
    extends AbstractTableModel
{
    
    /** The unique class ID */
    private static final long serialVersionUID = -5671017992322581586L;


    /**
     * 
     * @return
     */
    public abstract TableColumnMapper<E> getTableColumnMapper();
    

    /**
     * 
     * @param row
     * @return
     */
    public abstract E getObjectAtRow( int row );
    

    /**
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    public int getColumnCount()
    {
        return this.getTableColumnMapper().getColumns().size();
    }

    
    /**
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    public String getColumnName( int i ) {
        return this.getTableColumnMapper().getColumnName( i );
    }

    
    /**
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        E row = this.getObjectAtRow( rowIndex );

        if( row == null )
            return null;

        return getTableColumnMapper().extract( row, columnIndex );
    }
}