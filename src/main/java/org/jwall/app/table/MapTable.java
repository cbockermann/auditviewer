package org.jwall.app.table;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import javax.swing.table.AbstractTableModel;

/**
 * 
 * This class implements a table model which is backed by a map. Each (key,value) pair in the
 * map will be regarded as a row in a two-column table.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <K> The type of data in the first column of the table.
 * @param <V> The data type in the second column.
 */
public class MapTable<K,V>
    extends AbstractTableModel
{
    /** The unique class id */
    private static final long serialVersionUID = -4219504418173468221L;

    /** */
    private TreeSet<K> keys = new TreeSet<K>();
    
    /** The data */
    private Map<K,V> data = new HashMap<K,V>();
    
    /* */
    private String firstColumnName = "Key";
    
    /* */
    private String secondColumnName = "Value";
    
    
    public MapTable(){
        firstColumnName = "Key";
        secondColumnName = "Value";
    }
    
    public MapTable( String col1Name, String col2Name ){
        firstColumnName = col1Name;
        secondColumnName = col2Name;
    }
    
    
    /**
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    public int getColumnCount()
    {
        return 2;
    }

    
    /**
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(int column)
    {
        if( column == 0 )
            return firstColumnName;
        else
            return secondColumnName;
    }


    /**
     * @see javax.swing.table.TableModel#getRowCount()
     */
    public int getRowCount()
    {
        return data.size();
    }

    
    /**
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    public Object getValueAt(int rowIndex, int columnIndex)
    {
        int idx = 0;
        Iterator<K> it = keys.iterator();
        
        while( it.hasNext() ){
            
            K key = it.next();
            
            if( idx == rowIndex ){
                
                if( columnIndex == 0 )
                    return key;
                else
                    return data.get( key );
            } else
                idx++;
        }
        
        return null;
    }
    
    public void put( K key, V value ){
        data.put( key, value );
        if( keys.contains( key ) )
            keys.remove( key );

        keys.add( key );
        this.fireTableDataChanged();
    }
    
    public void remove( K key ){
        data.remove( key );
        keys.remove( key );
        this.fireTableDataChanged();
    }
    
    public V get( K key ){
        if( keys.contains( key ) )
            return data.get( key );
        return null;
    }
    
    public Map<K,V> getData(){
        return data;
    }
}