/*
 *  Copyright (C) 2007-2009 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  AuditViewer. You can get more information
 *  about  AuditViewer  on its web page at
 *
 *               http://www.jwall.org/web/audit/viewer.jsp
 *
 *  AuditViewer  is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AuditViewer  is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.app.table;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * <p>
 * This class represents a table column for a table consisting of rows of a certain
 * type of objects, <code>T</code>. It is serializable to XML using xstreams, which
 * allows for a customization using plain XML configuration files.
 * </p>
 * <p>
 * The concept of this approach is to have columns which provide a certain displayable
 * name and a specific ID. The ID is used by the column-mapper to extract the value
 * for the column given a specific row. The name is simply used to render the table
 * header names. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <T>
 */
@XStreamAlias("Column")
public class TableColumn
{
	@XStreamAlias("variable")
    String variable;
    
	@XStreamAlias("name")
    String name;

    public TableColumn( String id, String name ){
        this.variable = id;
        this.name = name;
    }
    
    public String getVariable(){
        return variable;
    }
    
    public void setVariable( String var ){
        this.variable = var;
    }
    
    public String getColumnName(){
        return name;
    }
    
    public void setColumnName( String name ){
        this.name = name;
    }
}
