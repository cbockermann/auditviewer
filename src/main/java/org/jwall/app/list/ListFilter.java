package org.jwall.app.list;


/**
 * 
 * This interface defines the methods for a filter that is used to
 * display a filtered list of objects using the <code>AbstractListView</code>.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <T> The type of objects that are displayed within the AbstractListView and
 *            which are filtered using an implementing class.
 */
public interface ListFilter<T>
{
    /**
     * This is a user-given parameter for filtering. Usually the given string
     * is a search/filter expression typed by the user.
     * 
     * @param exp The expression used for filtering.
     */
    public void setFilterExpression( String exp );
    
    
    /**
     * This is the basic filter method. It simply checks whether the
     * given object matches this filter's expression.
     * 
     * @param object The object to filter.
     * @return <code>true</code>, iff the filter matches the object.
     */
    public boolean matches( T object );
}
