package org.jwall.app.list;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public abstract class AbstractListModel<T>
    implements ListModel, TreeModel
{

    List<ListDataListener> listener = new LinkedList<ListDataListener>();
    List<TreeModelListener> tml = new LinkedList<TreeModelListener>();
    LinkedList<T> selected = new LinkedList<T>();
    LinkedList<T> contents = new LinkedList<T>();
    private ListFilter<T> filter = null;
    static Object root = new Object();
    
    public AbstractListModel()
    {
    }
    
    
    public abstract LinkedList<T> getAllChildren( T node );
    

    public void setContents(Collection<T> p)
    {
        contents.clear();
        selected.clear();
        contents.addAll( p );
        selected.addAll( p );
        notifyListener();
    }

    public Object getElementAt(int index)
    {
        return selected.get( index );
    }

    public int getSize()
    {
        return selected.size();
    }

    public void removeListDataListener(ListDataListener l)
    {
        listener.remove( l );
    }

    public void addListDataListener(ListDataListener l)
    {
        if( ! listener.contains( l ) )
            listener.add( l );
    }
    
    
    public void notifyListener(){
        for( ListDataListener l : listener )
            l.contentsChanged( new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, contents.size() ) );
        
        for( TreeModelListener l : tml ){
            l.treeNodesChanged( new TreeModelEvent( this, new Object[]{ root } ) );
            l.treeStructureChanged( new TreeModelEvent( this, new Object[]{ root } ) );
        }
    }
    
    

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    public void addTreeModelListener(TreeModelListener l)
    {
        tml.add( l );
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getChild(java.lang.Object, int)
     */
    public Object getChild(Object parent, int index)
    {
        //System.out.println( "parent = " + parent + ", index = " + index + ", element = " + getElementAt( index ) );
        return this.getElementAt( index );
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    public int getChildCount(Object parent)
    {
        //System.out.println("parent " + parent + " has " + getSize() + " children.");
        return getSize();
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    public int getIndexOfChild(Object parent, Object child)
    {
        return contents.indexOf( child );
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    public Object getRoot()
    {
        
        return root;
        
        /*
        if( contents.isEmpty() )
            return root;
        
        return contents.getFirst();
        */
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    public boolean isLeaf(Object node)
    {
        return node != root;
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    public void removeTreeModelListener(TreeModelListener l)
    {
        tml.remove(l);
    }

    /* (non-Javadoc)
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    public void valueForPathChanged(TreePath path, Object newValue)
    {
        for( TreeModelListener l : tml ){
            l.treeStructureChanged( new TreeModelEvent( newValue, path.getPath() ) );
        }
    }

    public ListFilter<T> getFilter()
    {
        return filter;
    }

    public void setFilter( ListFilter<T> f )
    {
        filter = f;
        doFilter();
    }
    
    public void doFilter(){
        selected.clear();
        
        if( filter == null ){
            selected.addAll( contents );

        } else {
            
            for( T r : contents )
                if( filter.matches( r ) )
                    selected.add( r );
        }
        
        dataChanged();
    }
    
    private void dataChanged(){
        for( ListDataListener l : listener )
            l.contentsChanged( new ListDataEvent( this, ListDataEvent.CONTENTS_CHANGED, 0, contents.size() ) );
        
        for( TreeModelListener l : tml )
            l.treeStructureChanged( new TreeModelEvent( this, new Object[]{ root } ) );
    }
    
    
    public void add( T element ){
        contents.add( element );
        selected.add( element );
        dataChanged();
    }
    

    public void removeElementAt( int i ){
        T element = selected.remove( i );
        contents.remove( element );
        dataChanged();
    }
    
    public void removeElementsAt( int[] idx ){
        List<T> elements = new LinkedList<T>();
        for( int index : idx ){
            T el = selected.get( index );
            elements.add( el );
        }
        
        for( T element : elements ){
            selected.remove( element );
            contents.remove( element );
        }
        dataChanged();
    }
}