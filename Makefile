# $Id$
#
# This Makefile contains all instructions to create RPM and DEB packages
# for the AuditConsole
#
#
#
VERSION=0.3.5
REVISION=1
NAME=AuditViewer
BUILD=.build_tmp
DIST=jwall
DEB_FILE=auditviewer-${VERSION}-${REVISION}.deb
RPM_FILE=auditviewer-${VERSION}-${REVISION}.noarch.rpm
RELEASE_DIR=releases
RPMBUILD=$(PWD)/.rpmbuild
ARCH=noarch

all:  assembly rpm deb zip sign-releases


pre-package:
	mvn assembly:assembly
	echo "Preparing package build in ${BUILD}"
	mkdir -p ${BUILD}/usr/share/AuditViewer/lib
	cp target/AuditViewer-${VERSION}-SNAPSHOT-jar-with-dependencies.jar ${BUILD}/usr/share/AuditViewer/lib/AuditViewer.jar


rpm:
	mkdir -p ${RELEASE_DIR}
	rm -rf ${RPMBUILD}
	mkdir -p ${RPMBUILD}
	mkdir -p ${RPMBUILD}/tmp
	mkdir -p ${RPMBUILD}/RPMS
	mkdir -p ${RPMBUILD}/RPMS/${ARCH}
	mkdir -p ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/SRPMS
	rm -rf ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/BUILD
	mkdir -p ${RPMBUILD}/SPECS
	cp -a dist/auditviewer.spec ${RPMBUILD}/SPECS
	mkdir -p ${RPMBUILD}/BUILD/usr/share/AuditViewer/lib
	cp target/AuditViewer-${VERSION}-SNAPSHOT-jar-with-dependencies.jar ${RPMBUILD}/BUILD/usr/share/AuditViewer/lib/AuditViewer.jar
	find .rpmbuild/BUILD -type f | sed -e s/^\.rpmbuild\\/BUILD// | grep -v DEBIAN > ${RPMBUILD}/tmp/rpmfiles.list
	rpmbuild --target noarch --sign --define '_topdir ${RPMBUILD}' --define '_version ${VERSION}' --define '_revision ${REVISION}' -bb ${RPMBUILD}/SPECS/auditviewer.spec --buildroot ${RPMBUILD}/BUILD
	cp ${RPMBUILD}/RPMS/${ARCH}/${RPM_FILE} ${RELEASE_DIR}

release-rpm:
	mkdir -p /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch
	cp ${RELEASE_DIR}/${RPM_FILE} /var/www/download.jwall.org/htdocs/yum/${DIST}/noarch/
	createrepo /var/www/download.jwall.org/htdocs/yum/${DIST}/


deb:	pre-package
	mkdir -p ${RELEASE_DIR}
	mkdir -p ${BUILD}/DEBIAN
	cp dist/DEBIAN/* ${BUILD}/DEBIAN/
	chmod 755 ${BUILD}/DEBIAN/p*
	cd ${BUILD} && find . -type f -exec md5sum {} \; > DEBIAN/md5sums && cd ..
	dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
#	rm -rf ${BUILD}
#	debsigs --sign=origin --default-key=C5C3953C ${RELEASE_DIR}/${DEB_FILE}

release-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian includedeb ${DIST} ${RELEASE_DIR}/${DEB_FILE}


unrelease-deb:
	reprepro --ask-passphrase -b /var/www/download.jwall.org/htdocs/debian remove ${DIST} auditconsole


pkgwar:	war
	mkdir -p ${BUILD}/DEBIAN
	cp dist/DEBIAN/* ${BUILD}/DEBIAN/
	cp -a dist/etc ${BUILD}
	chmod 755 ${BUILD}/DEBIAN/p*
	cd ${BUILD} && find opt -type f -exec md5sum {} \; > DEBIAN/md5sums && cd ..
	cd ${BUILD} && find etc -type f -exec md5sum {} \; >> DEBIAN/md5sums && cd ..
	dpkg -b ${BUILD} ${RELEASE_DIR}/${DEB_FILE}
#	rm -rf ${BUILD}
	debsigs --sign=origin --default-key=C5C3953C ${RELEASE_DIR}/${DEB_FILE}


zip:	war
	ant dist



clean:	clean-rpm clean-deb clean-zip
	mvn clean
	cd console-web && mvn clean && cd ..
	rm -rf ${RELEASE_DIR}

clean-rpm:
	rm -rf ${RPMBUILD}
	rm -f ${RPM_FILE}

clean-deb:
	rm -rf ${BUILD}
	rm -f ${DEB_FILE}

clean-zip:
	rm -rf ${BUILD}/zip
	rm -f ${NAME}-${VERSION}.zip

sign-releases:
	sh src/main/assembly/sign-releases.sh ./${RELEASE_DIR}

upload:
	cd ${RELEASE_DIR} && scp * chris@jwall.org:/var/www/download.jwall.org/upload/


release: upload
